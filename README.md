# PurpleAperture

PurpleAperture is Purple's data integration platform.

## Documentation sections

- [Development environment setup](./docs/development-environment-setup.md)
- [Manual deployment steps](./docs/manual-deployment-steps.md)
- [Testing with Postman](./docs/testing-with-postman.md)
- [Client certificates (mTLS)](./docs/client-certificates-setup.md)
- [SQL dynamic data masking](./docs/sql-dynamic-data-masking.md)
