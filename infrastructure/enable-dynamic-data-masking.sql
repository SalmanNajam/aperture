--
-- [PhoenixLeads]
--
ALTER TABLE [dbo].[PhoenixLeads]
ALTER COLUMN [FirstName] ADD MASKED WITH (FUNCTION = 'default()')

ALTER TABLE [dbo].[PhoenixLeads]
ALTER COLUMN [LastName] ADD MASKED WITH (FUNCTION = 'default()')

ALTER TABLE [dbo].[PhoenixLeads]
ALTER COLUMN [LeadOwner] ADD MASKED WITH (FUNCTION = 'email()')

--
-- [PhoenixOpportunities]
--
ALTER TABLE [dbo].[PhoenixOpportunities]
ALTER COLUMN [OppOwnerEmail] ADD MASKED WITH (FUNCTION = 'email()')

--
-- [PhoenixUsers]
--
ALTER TABLE [dbo].[PhoenixUsers]
ALTER COLUMN [FirstName] ADD MASKED WITH (FUNCTION = 'default()')

ALTER TABLE [dbo].[PhoenixUsers]
ALTER COLUMN [LastName] ADD MASKED WITH (FUNCTION = 'default()')

ALTER TABLE [dbo].[PhoenixUsers]
ALTER COLUMN [Email] ADD MASKED WITH (FUNCTION = 'email()')

--
-- [PhoenixAccounts]
--
ALTER TABLE [dbo].[PhoenixAccounts]
ALTER COLUMN [Address_AdborId] ADD MASKED WITH (FUNCTION = 'default()')

ALTER TABLE [dbo].[PhoenixAccounts]
ALTER COLUMN [Address_Address1] ADD MASKED WITH (FUNCTION = 'default()')

ALTER TABLE [dbo].[PhoenixAccounts]
ALTER COLUMN [Address_Address2] ADD MASKED WITH (FUNCTION = 'default()')

ALTER TABLE [dbo].[PhoenixAccounts]
ALTER COLUMN [Address_Address3] ADD MASKED WITH (FUNCTION = 'default()')

ALTER TABLE [dbo].[PhoenixAccounts]
ALTER COLUMN [Address_Locality] ADD MASKED WITH (FUNCTION = 'default()')

ALTER TABLE [dbo].[PhoenixAccounts]
ALTER COLUMN [Address_State] ADD MASKED WITH (FUNCTION = 'default()')

ALTER TABLE [dbo].[PhoenixAccounts]
ALTER COLUMN [Address_ZipCode] ADD MASKED WITH (FUNCTION = 'default()')

ALTER TABLE [dbo].[PhoenixAccounts]
ALTER COLUMN [Address_Country] ADD MASKED WITH (FUNCTION = 'default()')

ALTER TABLE [dbo].[PhoenixAccounts]
ALTER COLUMN [Address_AddressType] ADD MASKED WITH (FUNCTION = 'default()')

ALTER TABLE [dbo].[PhoenixAccounts]
ALTER COLUMN [Address_SourceSystem] ADD MASKED WITH (FUNCTION = 'default()')

ALTER TABLE [dbo].[PhoenixAccounts]
ALTER COLUMN [Address_AddressStatus] ADD MASKED WITH (FUNCTION = 'default()')

ALTER TABLE [dbo].[PhoenixAccounts]
ALTER COLUMN [Address_Primary] ADD MASKED WITH (FUNCTION = 'default()')
