param name string
param planResourceId string
param appInsightsInstrumentationKey string
param audience string
param storageAccountBlobUrl string
param storageAccountQueueUrl string
param subnetResourceId string
param sqlServerName string
param sqlDatabaseName string

var databaseConnectionString = 'Data Source=tcp:${sqlServerName}.database.windows.net,1433;Initial Catalog=${sqlDatabaseName};Encrypt=True;TrustServerCertificate=False;'

resource appService_name_resource 'Microsoft.Web/sites@2021-01-01' = {
  name: name
  location: resourceGroup().location
  identity: {
    type: 'SystemAssigned'
  }
  properties: {
    clientAffinityEnabled: false
    httpsOnly: true
    serverFarmId: planResourceId
    siteConfig: {
      alwaysOn: true
      appSettings: [
        {
          name: 'APPINSIGHTS_INSTRUMENTATIONKEY'
          value: appInsightsInstrumentationKey
        }
        {
          name: 'AzureAdOptions:Audience'
          value: audience
        }
        {
          name: 'AzureBlobStorageOptions:ServiceUri'
          value: storageAccountBlobUrl
        }
        {
          name: 'AzureQueueStorageOptions:ServiceUri'
          value: storageAccountQueueUrl
        }
        {
          name: 'ConnectionStrings:Default'
          value: databaseConnectionString
        }
        {
          name: 'WEBSITE_RUN_FROM_PACKAGE'
          value: '1'
        }
        {
          name: 'WEBSITE_DNS_SERVER'
          value: '168.63.129.16'
        }
        {
          name: 'WEBSITE_VNET_ROUTE_ALL'
          value: '1'
        }
      ]
      ftpsState: 'Disabled'
      javaVersion: 'off'
      nodeVersion: 'off'
      minTlsVersion: '1.2'
      netFrameworkVersion: 'v5.0'
      phpVersion: 'off'
      pythonVersion: 'off'
      remoteDebuggingEnabled: false
      webSocketsEnabled: false
    }
  }
}

resource appService_name_virtualNetwork 'Microsoft.Web/sites/networkConfig@2021-01-01' = {
  parent: appService_name_resource
  name: 'virtualNetwork'
  properties: {
    subnetResourceId: subnetResourceId
    swiftSupported: true
  }
}
