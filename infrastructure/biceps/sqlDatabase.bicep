param sqlServerName string
param sqlDatabaseName string
param autoPauseDelayInMinutes int
param maxCores int
param maxSizeInGigaBytes int

resource sqlServerResource 'Microsoft.Sql/servers@2021-02-01-preview' existing = {
  name: sqlServerName
}

var maxSizeInBytes = maxSizeInGigaBytes * 1024 * 1024 * 1024
resource sqlDatabaseResource 'Microsoft.Sql/servers/databases@2021-02-01-preview' = {
  name: sqlDatabaseName
  parent: sqlServerResource
  location: resourceGroup().location
  sku: {
    name: 'GP_S_Gen5'
    tier: 'GeneralPurpose'
    family: 'Gen5'
    capacity: maxCores
  }
  properties: {
    collation: 'SQL_Latin1_General_CP1_CI_AS'
    zoneRedundant: false
    requestedBackupStorageRedundancy: 'Zone'
    autoPauseDelay: autoPauseDelayInMinutes
    maxSizeBytes: maxSizeInBytes
  }
}
