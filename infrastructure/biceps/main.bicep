targetScope = 'subscription'

param azureAdOptionsAudience string
param appInsightsName string
param appServiceName string
param appServicePlanName string
param appServicePlanSize string
param privateBlobDnsZoneResourceId string
param privateQueueDnsZoneResourceId string
param resourceGroupName string
param sharedResourceGroupName string
param sqlServerName string
param sqlDatabaseName string
param sqlDatabaseAutoPuaseDelayInMinutes int
param sqlDatabaseMaxCore int
param sqlDatabaseMaxSizeInGigaBytes int
param storageAccountName string
param vnetWebSubnetResourceId string
param vnetDbSubnetResourceId string

resource rg 'Microsoft.Resources/resourceGroups@2021-04-01' = {
  name: resourceGroupName
  location: deployment().location
}

resource sharedResourceGroup 'Microsoft.Resources/resourceGroups@2021-04-01' existing = {
  name: sharedResourceGroupName
}

module storageModule 'storage.bicep' = {
  name: 'storageAccount'
  scope: rg
  params: {
    name: storageAccountName
  }
}

module privateEndPointModule 'privateEndPoint.bicep' = {
  name: 'privateEndPoint'
  scope: rg
  params: {
    privateEndpointNamePrefix: storageAccountName
    storageAccountId: storageModule.outputs.id
    subnetResourceId: vnetDbSubnetResourceId
    privateBlobDnsZoneResourceId: privateBlobDnsZoneResourceId
    privateQueueDnsZoneResourceId: privateQueueDnsZoneResourceId
  }
}

module appInsightsModule 'appInsights.bicep' = {
  name: 'appInsights'
  scope: rg
  params: {
    name: appInsightsName
  }
}

module sqlDatbaseModule 'sqlDatabase.bicep' = {
  name: 'sqlDatabase'
  scope: sharedResourceGroup
  params: {
    sqlServerName: sqlServerName
    sqlDatabaseName: sqlDatabaseName
    autoPauseDelayInMinutes: sqlDatabaseAutoPuaseDelayInMinutes
    maxCores: sqlDatabaseMaxCore
    maxSizeInGigaBytes: sqlDatabaseMaxSizeInGigaBytes
  }
}

module appServicePlanModule 'appServicePlan.bicep' = {
  name: 'appServicePlan'
  scope: sharedResourceGroup
  params: {
    name: appServicePlanName
    size: appServicePlanSize
  }
}

module webAppModule 'webapp.bicep' = {
  name: 'webApp'
  scope: rg
  params: {
    name: appServiceName
    appInsightsInstrumentationKey: appInsightsModule.outputs.instrumentationKey
    storageAccountBlobUrl: storageModule.outputs.blobUrl
    storageAccountQueueUrl: storageModule.outputs.queueUrl
    audience: azureAdOptionsAudience
    planResourceId: appServicePlanModule.outputs.id
    subnetResourceId: vnetWebSubnetResourceId
    sqlServerName: sqlServerName
    sqlDatabaseName: sqlDatabaseName
  }
}
