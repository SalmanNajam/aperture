param name string

resource appInsightsResource 'Microsoft.Insights/components@2018-05-01-preview' = {
  name: name
  location: resourceGroup().location
  kind: 'web'
  properties: {
    Application_Type: 'web'
    Request_Source: 'rest'
    RetentionInDays: 180
  }
}

output instrumentationKey string = appInsightsResource.properties.InstrumentationKey
