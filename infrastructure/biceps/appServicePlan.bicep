param name string
param size string

resource appServicePlanResource 'Microsoft.Web/serverfarms@2021-01-01' = {
  name: name
  location: resourceGroup().location
  kind: 'app'
  sku: {
    name: size
    size: size
  }
}

output id string = appServicePlanResource.id
