param name string

resource storageAccountResource 'Microsoft.Storage/storageAccounts@2021-04-01' = {
  name: name
  kind: 'StorageV2'
  location: resourceGroup().location
  sku: {
    name: 'Standard_LRS'
  }
  properties: {
    allowBlobPublicAccess: false
    allowSharedKeyAccess: false
    minimumTlsVersion: 'TLS1_2'
    supportsHttpsTrafficOnly: true
    networkAcls: {
      defaultAction: 'Deny'
      bypass: 'None'
    }
  }
}

resource blobServiceResource 'Microsoft.Storage/storageAccounts/blobServices@2021-04-01' = {
  parent: storageAccountResource
  name: 'default'
  properties: {
    deleteRetentionPolicy: {
      enabled: true
      days: 7
    }
    isVersioningEnabled: true
  }
}

output blobUrl string = storageAccountResource.properties.primaryEndpoints.blob
output queueUrl string = storageAccountResource.properties.primaryEndpoints.queue
output id string = storageAccountResource.id
