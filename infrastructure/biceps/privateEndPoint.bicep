param storageAccountId string
param subnetResourceId string
param privateEndpointNamePrefix string
param privateBlobDnsZoneResourceId string
param privateQueueDnsZoneResourceId string

var privateEndpointTypes = [
  {
    type: 'blob'
    dnsZoneResourceId: privateBlobDnsZoneResourceId
  }
  {
    type: 'queue'
    dnsZoneResourceId: privateQueueDnsZoneResourceId
  }
]

resource privateEndpoint 'Microsoft.Network/privateEndpoints@2021-02-01' = [for config in privateEndpointTypes: {
  name: '${privateEndpointNamePrefix}-${config.type}-pe'
  location: resourceGroup().location
  properties: {
    privateLinkServiceConnections: [
      {
        name: '${privateEndpointNamePrefix}-${config.type}-pe'
        properties: {
          privateLinkServiceId: storageAccountId
          groupIds: [
            config.type
          ]
        }
      }
    ]
    subnet: {
      id: subnetResourceId
    }
  }
}]

resource privateEndpointDnsZone 'Microsoft.Network/privateEndpoints/privateDnsZoneGroups@2021-02-01' = [for (config, index) in privateEndpointTypes: {
  name: 'default'
  parent: privateEndpoint[index]
  properties: {
    privateDnsZoneConfigs: [
      {
        name: 'privatelink-${config.type}-core-windows-net'
        properties: {
          privateDnsZoneId: config.dnsZoneResourceId
        }
      }
    ]
  }
  dependsOn: [
    privateEndpoint[index]
  ]
}]
