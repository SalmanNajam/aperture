﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using PurpleAperture.Common.Infrastructure;

namespace PurpleAperture.Common
{
    public class Program
    {
        public static void Main(string[] args)
            => CreateHostBuilder(args).Build().Run();

        // EF Core uses this method at design time to access the DbContext
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            new HostBuilder()
                .ConfigureHostConfiguration(configHost => configHost.AddEnvironmentVariables(prefix: "ASPNETCORE_"))
                .ConfigureAppConfiguration(ConfigureAppConfiguration)
                .ConfigureLogging(ConfigureLogging)
                .ConfigureServices(ConfigureDependencyInjection);

        private static void ConfigureAppConfiguration(HostBuilderContext builderContext, IConfigurationBuilder config)
        {
            var environment = builderContext.HostingEnvironment;

            config.AddJsonFile($"appsettings.{environment.EnvironmentName}.json", optional: true, reloadOnChange: true);

            if (environment.IsDevelopment())
            {
                config.AddUserSecrets<Program>();
            }
        }

        private static void ConfigureDependencyInjection(HostBuilderContext builderContext, IServiceCollection services)
        {
            // register services
            services.AddMemoryCache();
            services.RegisterPhoenixDbContext(builderContext.Configuration);
            services.RegisterWorkDataDbContext(builderContext.Configuration);
        }

        private static void ConfigureLogging(HostBuilderContext builderContext, ILoggingBuilder loggingBuilder)
        {
            loggingBuilder.AddConfiguration(builderContext.Configuration.GetSection("Logging"));
            loggingBuilder.AddDebug();
            loggingBuilder.AddConsole();
        }
    }
}
