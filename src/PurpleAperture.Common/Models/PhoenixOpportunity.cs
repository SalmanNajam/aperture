﻿using System;
using System.Collections.Generic;
using System.Linq;
using CSharpFunctionalExtensions;
using PurpleAperture.Common.Infrastructure;
using PurpleAperture.Common.Models.Json;

namespace PurpleAperture.Common.Models
{
    public class PhoenixOpportunity
    {
        public int Id { get; set; }

        public string SalesforceId { get; set; } = string.Empty;
        public string? OpportunityId { get; set; }
        public string? OpportunityName { get; set; }

        public List<PhoenixAlliance> Alliances { get; set; } = new();

        // ReSharper disable once IdentifierTypo
        public string? Cidn { get; set; }
        public DateTime? CloseDate { get; set; }
        public string? Competitor { get; set; }
        public string? ConfLvl { get; set; }
        public string? ContractType { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? LastModDate { get; set; }
        public string? LeadOrgSource { get; set; }
        public string? OppOwnerEmail { get; set; }
        public string? PriCampSource { get; set; }
        public string? PriDomain { get; set; }

        // ReSharper disable once IdentifierTypo
        public decimal? TotalContracVal { get; set; }
        public DateTime? RevImpactDate { get; set; }
        public string? Stage { get; set; }
        public List<PhoenixProduct> Products { get; set; } = new();
        public string? Description { get; set; }
        public decimal? CurrentRevenue { get; set; }
        public decimal? Probability { get; set; }
        public string? Partner { get; set; }
        public YesNo? IsTelstraPurpleOpp { get; set; }
        public List<PhoenixOpportunityTeamMembers> OppTeamMembers { get; set; } = new();

        public DateTimeOffset LastUpdatedDateTime { get; set; }

        public static Result<PhoenixOpportunity, EntityMappingError> From(
            JsonOpportunity json,
            DateTimeOffset currentDateTimeOffset)
        {
            var extensionData = json.ExtensionData.ToDictionary(
                k => k.Key,
                v => v.Value,
                StringComparer.InvariantCultureIgnoreCase);

            var alliances = PhoenixAlliance.From(json.Alliances);
            var products = PhoenixProduct.From(json.Products);
            var oppTeamMembers = PhoenixOpportunityTeamMembers.From(json.OppTeamMembers);
            var closeDate = extensionData.ParseShortDateOrDefault(nameof(CloseDate));
            var createDate = extensionData.ParseShortDateOrDefault(nameof(CreateDate));
            var lastModDate = extensionData.ParseShortDateOrDefault(nameof(LastModDate));
            var revImpactDate = extensionData.ParseShortDateOrDefault(nameof(RevImpactDate));
            var isTelstraPurpleOpp = extensionData.ParseEnum<YesNo>(nameof(IsTelstraPurpleOpp));
            var totalContractVal = extensionData.ParseDecimalOrDefault(nameof(TotalContracVal));

            var combinedResult = Result.Combine(
                closeDate,
                createDate,
                lastModDate,
                revImpactDate,
                isTelstraPurpleOpp,
                totalContractVal,
                alliances,
                products,
                oppTeamMembers);

            if (combinedResult.IsFailure)
            {
                return combinedResult
                    .ConvertFailure<PhoenixOpportunity>()
                    .MapError(fieldErrors =>
                        new EntityMappingError(
                            nameof(PhoenixOpportunity),
                            json.SalesforceId,
                            fieldErrors
                        ));
            }

            PhoenixOpportunity convertedOpportunity = new()
            {
                OpportunityId = json.OpportunityId,
                SalesforceId = json.SalesforceId ?? throw new InvalidOperationException(),
                OpportunityName = extensionData.GetStringOrDefault(nameof(OpportunityName)),

                Alliances = alliances.Value.ToList(),
                Products = products.Value.ToList(),
                OppTeamMembers = oppTeamMembers.Value.ToList(),

                Cidn = extensionData.GetStringOrDefault(nameof(Cidn)),
                CloseDate = closeDate.Value,
                Competitor = extensionData.GetStringOrDefault(nameof(Competitor)),
                ConfLvl = extensionData.GetStringOrDefault(nameof(ConfLvl)),
                ContractType = extensionData.GetStringOrDefault(nameof(ContractType)),
                CreateDate = createDate.Value,
                LastModDate = lastModDate.Value,
                LeadOrgSource = extensionData.GetStringOrDefault(nameof(LeadOrgSource)),
                OppOwnerEmail = extensionData.GetStringOrDefault(nameof(OppOwnerEmail)),
                PriCampSource = extensionData.GetStringOrDefault(nameof(PriCampSource)),
                PriDomain = extensionData.GetStringOrDefault(nameof(PriDomain)),
                TotalContracVal = totalContractVal.Value,
                RevImpactDate = revImpactDate.Value,
                Stage = extensionData.GetStringOrDefault(nameof(Stage)),
                Description = extensionData.GetStringOrDefault(nameof(Description)),
                Partner = extensionData.GetStringOrDefault(nameof(Partner)),
                IsTelstraPurpleOpp = isTelstraPurpleOpp.Value,

                LastUpdatedDateTime = currentDateTimeOffset
            };

            return convertedOpportunity;
        }
    }
}
