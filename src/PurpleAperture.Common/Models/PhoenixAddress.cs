﻿using System;
using CSharpFunctionalExtensions;
using PurpleAperture.Common.Infrastructure;
using PurpleAperture.Common.Models.Json;

namespace PurpleAperture.Common.Models
{
    public class PhoenixAddress
    {
        // ReSharper disable once IdentifierTypo
        public string? AdborId { get; set; }
        public string? Address1 { get; set; }
        public string? Address2 { get; set; }
        public string? Address3 { get; set; }
        public string? Locality { get; set; }
        public string? State { get; set; }
        public string? ZipCode { get; set; }
        public string? Country { get; set; }
        public DateTimeOffset? LastModifiedDate { get; set; }
        public string? AddressType { get; set; }
        public string? SourceSystem { get; set; }
        public string? AddressStatus { get; set; }
        public string? Primary { get; set; }

        public static Result<PhoenixAddress> From(JsonAddress? jsonAddress)
        {
            if (jsonAddress == null)
            {
                return Result.Failure<PhoenixAddress>("JSON Address was null");
            }

            var lastModifiedDate = jsonAddress.LastModifiedDate
                .ParseTimestampOrDefault(nameof(JsonAddress.LastModifiedDate));

            if (lastModifiedDate.IsFailure)
            {
                return lastModifiedDate
                    .ConvertFailure<PhoenixAddress>();
            }

            PhoenixAddress convertedAddress = new()
            {
                AdborId = jsonAddress.AdborId,
                Address1 = jsonAddress.Address1,
                Address2 = jsonAddress.Address2,
                Address3 = jsonAddress.Address3,
                Locality = jsonAddress.Locality,
                State = jsonAddress.State,
                ZipCode = jsonAddress.ZipCode,
                Country = jsonAddress.Country,
                LastModifiedDate = lastModifiedDate.Value,
                AddressType = jsonAddress.AddressType,
                SourceSystem = jsonAddress.SourceSystem,
                AddressStatus = jsonAddress.AddressStatus,
                Primary = jsonAddress.Primary,
            };

            return convertedAddress;
        }
    }
}
