﻿using System.Collections.Generic;
using System.Linq;
using CSharpFunctionalExtensions;
using PurpleAperture.Common.Models.Json;

namespace PurpleAperture.Common.Models
{
    public class PhoenixAlliance
    {
        public int Id { get; set; }

        public int PhoenixOpportunityId { get; set; }

        public string? AllianceName { get; set; }

        public static Result<PhoenixAlliance> From(JsonAlliance json)
        {
            PhoenixAlliance convertedAlliance = new() { AllianceName = json.AllianceName };
            return convertedAlliance;
        }

        public static Result<IEnumerable<PhoenixAlliance>> From(IEnumerable<JsonAlliance>? json)
        {
            if (json == null)
            {
                return Result.Success(Enumerable.Empty<PhoenixAlliance>());
            }

            var alliances = json.Select(From);
            var combinedResult = alliances.Combine(Result.ErrorMessagesSeparator);
            return combinedResult;
        }
    }
}
