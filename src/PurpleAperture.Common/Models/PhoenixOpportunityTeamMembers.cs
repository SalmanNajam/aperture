﻿using System.Collections.Generic;
using System.Linq;
using CSharpFunctionalExtensions;
using PurpleAperture.Common.Models.Json;

namespace PurpleAperture.Common.Models
{
    public class PhoenixOpportunityTeamMembers
    {
        public int Id { get; set; }

        public int PhoenixOpportunityId { get; set; }

        public string? FullName { get; set; }

        public string? Role { get; set; }

        // ReSharper disable once InconsistentNaming
        public string? SAMLFederationID { get; set; }

        public static Result<PhoenixOpportunityTeamMembers> From(JsonOpportunityTeamMembers json)
        {
            PhoenixOpportunityTeamMembers convertedTeamMembers = new()
            {
                FullName = json.FullName,
                Role = json.Role,
                SAMLFederationID = json.SAMLFederationID
            };

            return convertedTeamMembers;
        }

        public static Result<IEnumerable<PhoenixOpportunityTeamMembers>> From(IEnumerable<JsonOpportunityTeamMembers>? json)
        {
            if (json == null)
            {
                return Result.Success(Enumerable.Empty<PhoenixOpportunityTeamMembers>());
            }

            var teamMembers = json.Select(From);
            var combinedResult = teamMembers.Combine(Result.ErrorMessagesSeparator);
            return combinedResult;
        }
    }
}
