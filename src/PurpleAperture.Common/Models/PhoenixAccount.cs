using System;
using CSharpFunctionalExtensions;
using PurpleAperture.Common.Infrastructure;
using PurpleAperture.Common.Models.Json;

namespace PurpleAperture.Common.Models
{
    public class PhoenixAccount
    {
        public int Id { get; set; }

        public string? Name { get; set; }

        // ReSharper disable once IdentifierTypo
        public string Cidn { get; set; }
        public PhoenixAddress? Address { get; set; }
        public string? Abn { get; set; }
        public string? PortfolioCode { get; set; }
        public string? Industry { get; set; }
        public string? SubIndustry { get; set; }
        public string? Segment { get; set; }
        public string? MarketSegment { get; set; }

        public DateTimeOffset LastUpdatedDateTime { get; set; }

        public static Result<PhoenixAccount, EntityMappingError> From(
            JsonAccount jsonAccount,
            DateTimeOffset currentDateTimeOffset)
        {
            var addressResult = PhoenixAddress.From(jsonAccount.Address);

            if (addressResult.IsFailure)
            {
                return addressResult
                    .ConvertFailure<PhoenixAccount>()
                    .MapError(errors => new EntityMappingError(
                        nameof(PhoenixAccount),
                        jsonAccount.Cidn,
                        errors));
            }

            PhoenixAccount convertedAccount = new()
            {
                Name = jsonAccount.Name,
                Cidn = jsonAccount.Cidn,
                Address = addressResult.Value,
                Abn = jsonAccount.Abn,
                PortfolioCode = jsonAccount.PortfolioCode,
                Industry = jsonAccount.Industry,
                SubIndustry = jsonAccount.SubIndustry,
                Segment = jsonAccount.Segment,
                MarketSegment = jsonAccount.MarketSegment,

                LastUpdatedDateTime = currentDateTimeOffset
            };

            return convertedAccount;
        }
    }
}
