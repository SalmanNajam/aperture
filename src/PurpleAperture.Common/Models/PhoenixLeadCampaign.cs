﻿using System.Collections.Generic;
using System.Linq;
using CSharpFunctionalExtensions;
using PurpleAperture.Common.Models.Json;

namespace PurpleAperture.Common.Models
{
    public class PhoenixLeadCampaign
    {
        public int Id { get; set; }
        public int PhoenixLeadId { get; set; }

        public string CampaignId { get; set; }

        public static Result<PhoenixLeadCampaign> From(JsonLeadCampaign? jsonCampaign)
        {
            if (jsonCampaign == null)
            {
                return Result.Failure<PhoenixLeadCampaign>("Deserialised Campaign is null");
            }

            PhoenixLeadCampaign leadCampaign = new() { CampaignId = jsonCampaign.CampaignId };
            return leadCampaign;
        }

        public static Result<IEnumerable<PhoenixLeadCampaign>> From(IEnumerable<JsonLeadCampaign>? jsonCampaigns)
        {
            if (jsonCampaigns == null)
            {
                return Result.Success(Enumerable.Empty<PhoenixLeadCampaign>());
            }

            var campaigns = jsonCampaigns.Select(From);
            var combinedResult = campaigns.Combine(Result.ErrorMessagesSeparator);
            return combinedResult;
        }
    }
}
