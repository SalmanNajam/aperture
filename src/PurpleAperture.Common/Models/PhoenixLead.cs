﻿using System;
using System.Collections.Generic;
using System.Linq;
using CSharpFunctionalExtensions;
using PurpleAperture.Common.Infrastructure;
using PurpleAperture.Common.Models.Json;

namespace PurpleAperture.Common.Models
{
    public class PhoenixLead
    {
        public int Id { get; set; }

        public string LeadId { get; set; }

        public string? OpportunityId { get; set; }

        public List<PhoenixLeadCampaign> Campaigns { get; set; }

        public DateTime? AcceptedDate { get; set; }

        public string? Company { get; set; }

        public DateTime? ConvertedDate { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string CustomLeadId { get; set; }

        public string? CustomerCIDN { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string LeadOwner { get; set; }

        public string? LeadSourceRecent { get; set; }

        public string? LeadSourceOri { get; set; }

        public string LeadStatus { get; set; }

        public DateTime? MQLDate { get; set; }

        public string? OtherReason { get; set; }

        public DateTime? QualifyDate { get; set; }

        public string? RejectReason { get; set; }

        public string? Title { get; set; }

        public DateTimeOffset LastUpdatedDateTime { get; set; }

        public static Result<PhoenixLead, EntityMappingError> From(
            JsonLead json,
            DateTimeOffset currentDateTimeOffset)
        {
            var acceptedDate = json.AcceptedDate.ParseShortDateOrDefault(nameof(AcceptedDate));
            var convertedDate = json.ConvertedDate.ParseShortDateOrDefault(nameof(ConvertedDate));
            var createdDate = json.CreatedDate.ParseShortDateOrDefault(nameof(CreatedDate));
            var mqlDate = json.MQLDate.ParseShortDateOrDefault(nameof(MQLDate));
            var qualifyDate = json.QualifyDate.ParseShortDateOrDefault(nameof(QualifyDate));
            var campaigns = PhoenixLeadCampaign.From(json.Campaigns);

            var combinedResult = Result.Combine(
                Environment.NewLine,
                acceptedDate,
                convertedDate,
                createdDate,
                mqlDate,
                qualifyDate,
                campaigns);

            if (combinedResult.IsFailure)
            {
                return combinedResult
                    .ConvertFailure<PhoenixLead>()
                    .MapError(combinedError =>
                        new EntityMappingError(
                            nameof(PhoenixLead),
                            json.LeadId,
                            combinedError
                        ));
            }

            var convertedLead = new PhoenixLead
            {
                LeadId = json.LeadId,
                OpportunityId = json.OpportunityId,
                Campaigns = campaigns.Value.ToList(),
                AcceptedDate = acceptedDate.Value,
                Company = json.Company,
                ConvertedDate = convertedDate.Value,
                CreatedDate = createdDate.Value,
                CustomLeadId = json.CustomLeadId,
                CustomerCIDN = json.CustomerCIDN,
                FirstName = json.FirstName,
                LastName = json.LastName,
                LeadOwner = json.LeadOwner,
                LeadSourceRecent = json.LeadSourceRecent,
                LeadSourceOri = json.LeadSourceOri,
                LeadStatus = json.LeadStatus,
                MQLDate = mqlDate.Value,
                OtherReason = json.OtherReason,
                QualifyDate = qualifyDate.Value,
                RejectReason = json.RejectReason,
                Title = json.Title,

                LastUpdatedDateTime = currentDateTimeOffset
            };
            return convertedLead;
        }
    }
}
