﻿using System;
using CSharpFunctionalExtensions;
using PurpleAperture.Common.Infrastructure;
using PurpleAperture.Common.Models.Json;

namespace PurpleAperture.Common.Models
{
    public class PhoenixUser
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public string Email { get; set; }

        public string? EmpNo { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string? MiddleName { get; set; }

        public string? RoleName { get; set; }

        public string? ParentRoleName { get; set; }

        public string? ParentParentRoleName { get; set; }

        public string? SAMLFederationID { get; set; }

        public bool? Active { get; set; }

        public DateTimeOffset LastUpdatedDateTime { get; set; }

        public static Result<PhoenixUser, EntityMappingError> From(
            JsonUser json,
            DateTimeOffset currentDateTimeOffset)
        {
            var email = json.Email.EnsureRequiredString(nameof(Email));
            var firstName = json.FirstName.EnsureRequiredString(nameof(FirstName));
            var lastName = json.LastName.EnsureRequiredString(nameof(LastName));

            var combinedResult = Result.Combine(
                Environment.NewLine,
                email,
                firstName,
                lastName);

            if (combinedResult.IsFailure)
            {
                return combinedResult
                    .ConvertFailure<PhoenixUser>()
                    .MapError(combinedError =>
                        new EntityMappingError(
                            nameof(PhoenixUser),
                            json.UserId,
                            combinedError
                        ));
            }

            PhoenixUser convertedUser = new()
            {
                UserId = json.UserId,
                Email = email.Value,
                EmpNo = json.EmpNo,
                FirstName = firstName.Value,
                LastName = lastName.Value,
                MiddleName = json.MiddleName,
                RoleName = json.RoleName,
                ParentRoleName = json.ParentRoleName,
                ParentParentRoleName = json.ParentParentRoleName,
                SAMLFederationID = json.SAMLFederationID,
                Active = json.Active,

                LastUpdatedDateTime = currentDateTimeOffset
            };

            return convertedUser;
        }
    }
}
