﻿namespace PurpleAperture.Common.Models.Json
{
    public class JsonAlliance
    {
        public string? AllianceName { get; set; }
    }
}
