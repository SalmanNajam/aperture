﻿namespace PurpleAperture.Common.Models.Json
{
    public class JsonAddress
    {
        // ReSharper disable once IdentifierTypo
        public string? AdborId { get; set; }
        public string? Address1 { get; set; }
        public string? Address2 { get; set; }
        public string? Address3 { get; set; }
        public string? Locality { get; set; }
        public string? State { get; set; }
        public string? ZipCode { get; set; }
        public string? Country { get; set; }
        public string? LastModifiedDate { get; set; }
        public string? AddressType { get; set; }
        public string? SourceSystem { get; set; }
        public string? AddressStatus { get; set; }
        public string? Primary { get; set; }
    }
}
