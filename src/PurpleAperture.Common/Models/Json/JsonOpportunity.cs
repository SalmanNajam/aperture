﻿using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace PurpleAperture.Common.Models.Json
{
    public class JsonOpportunity
    {
        public string? SalesforceId { get; set; }
        public string? OpportunityId { get; set; }

        [JsonPropertyName("Alliance")]
        public List<JsonAlliance> Alliances { get; set; } = new();

        public List<JsonProduct>? Products { get; set; } = new();
        public List<JsonOpportunityTeamMembers> OppTeamMembers { get; set; } = new();

        [JsonExtensionData]
        public Dictionary<string, JsonElement> ExtensionData { get; set; }
    }
}
