﻿namespace PurpleAperture.Common.Models.Json
{
    public class JsonOpportunityTeamMembers
    {
        public string? FullName { get; set; }

        public string? Role { get; set; }

        // ReSharper disable once InconsistentNaming
        public string? SAMLFederationID { get; set; }
    }
}
