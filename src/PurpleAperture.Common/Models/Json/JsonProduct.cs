﻿using System.Text.Json.Serialization;

namespace PurpleAperture.Common.Models.Json
{
    public class JsonProduct
    {
        public string? ProductCode { get; set; }
        public string? ProductName { get; set; }
        public string? ProductQty { get; set; }

        [JsonPropertyName("Won/Lost")]
        public string? WonLost { get; set; }

        // ReSharper disable once IdentifierTypo
        public string? Chennel { get; set; }
        public string? TotalRevenue { get; set; }
        public string? TotalCost { get; set; }
        public string? GrossMargin { get; set; }
        public string? WonStatus { get; set; }
        public string? LostReason { get; set; }
        public string? AnnualSalesValue { get; set; }
    }
}
