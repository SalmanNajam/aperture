﻿namespace PurpleAperture.Common.Models.Json
{
    public class JsonLead
    {
        public string LeadId { get; set; }

        public string? OpportunityId { get; set; }

        public JsonLeadCampaign[] Campaigns { get; set; }

        public string AcceptedDate { get; set; }

        public string Company { get; set; }

        public string? ConvertedDate { get; set; }

        public string CreatedDate { get; set; }

        public string CustomLeadId { get; set; }

        public string CustomerCIDN { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string LeadOwner { get; set; }

        public string? LeadSourceRecent { get; set; }

        public string LeadSourceOri { get; set; }

        public string LeadStatus { get; set; }

        public string? MQLDate { get; set; }

        public string? OtherReason { get; set; }

        public string? QualifyDate { get; set; }

        public string? RejectReason { get; set; }

        public string? Title { get; set; }
    }
}
