﻿namespace PurpleAperture.Common.Models.Json
{
    public class JsonAccount
    {
        public string? Name { get; set; }

        // ReSharper disable once IdentifierTypo
        public string Cidn { get; set; } = string.Empty;
        public JsonAddress? Address { get; set; }
        public string? Abn { get; set; }
        public string? PortfolioCode { get; set; }
        public string? Industry { get; set; }
        public string? SubIndustry { get; set; }
        public string? Segment { get; set; }
        public string? MarketSegment { get; set; }
    }
}
