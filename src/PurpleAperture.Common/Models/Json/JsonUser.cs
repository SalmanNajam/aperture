﻿namespace PurpleAperture.Common.Models.Json
{
    public class JsonUser
    {
        public string UserId { get; set; }

        public string Email { get; set; }

        public string? EmpNo { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string? MiddleName { get; set; }

        public string? RoleName { get; set; }

        public string? ParentRoleName { get; set; }

        public string? ParentParentRoleName { get; set; }

        public string? SAMLFederationID { get; set; }

        public bool? Active { get; set; }
    }
}
