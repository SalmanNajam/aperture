﻿using System;
using System.Collections.Generic;
using System.Linq;
using CSharpFunctionalExtensions;
using PurpleAperture.Common.Infrastructure;
using PurpleAperture.Common.Models.Json;

namespace PurpleAperture.Common.Models
{
    public class PhoenixProduct
    {
        public int Id { get; set; }
        public int PhoenixOpportunityId { get; set; }

        public string? ProductCode { get; set; }
        public string? ProductName { get; set; }
        public decimal? ProductQty { get; set; }
        public string? WonLost { get; set; }

        // ReSharper disable once IdentifierTypo
        public string? Chennel { get; set; }
        public decimal? TotalRevenue { get; set; }
        public decimal? TotalCost { get; set; }
        public decimal? GrossMargin { get; set; }
        public string? WonStatus { get; set; }
        public string? LostReason { get; set; }
        public decimal? AnnualSalesValue { get; set; }

        public static Result<PhoenixProduct> From(JsonProduct json)
        {
            var productQty = json.ProductQty.ParseDecimalOrDefault(nameof(ProductQty));
            var totalRevenue = json.TotalRevenue.ParseDecimalOrDefault(nameof(TotalRevenue));
            var totalCost = json.TotalCost.ParseDecimalOrDefault(nameof(TotalCost));
            var grossMargin = json.GrossMargin.ParseDecimalOrDefault(nameof(GrossMargin));
            var annualSalesValue = json.AnnualSalesValue.ParseDecimalOrDefault(nameof(AnnualSalesValue));

            var combinedResult = Result.Combine(
                Environment.NewLine,
                productQty,
                totalRevenue,
                totalCost,
                grossMargin,
                annualSalesValue);

            if (combinedResult.IsFailure)
            {
                return combinedResult
                    .ConvertFailure<PhoenixProduct>();
            }

            PhoenixProduct convertedProduct = new()
            {
                ProductCode = json.ProductCode,
                ProductName = json.ProductName,
                ProductQty = productQty.Value,
                WonLost = json.WonLost,
                Chennel = json.Chennel,
                TotalRevenue = totalRevenue.Value,
                TotalCost = totalCost.Value,
                GrossMargin = grossMargin.Value,
                WonStatus = json.WonStatus,
                LostReason = json.LostReason,
                AnnualSalesValue = annualSalesValue.Value
            };

            return convertedProduct;
        }

        public static Result<IEnumerable<PhoenixProduct>> From(IEnumerable<JsonProduct>? jsonProducts)
        {
            if (jsonProducts == null)
            {
                return Result.Success(Enumerable.Empty<PhoenixProduct>());
            }

            var products = jsonProducts.Select(From);
            var combinedResult = products.Combine(Result.ErrorMessagesSeparator);
            return combinedResult;
        }
    }
}
