using System;

namespace PurpleAperture.Common.Models
{
    public class LastUpdate
    {
        public string TableName { get; set; }
        public DateTimeOffset UpdateTime { get; set; }
    }
}
