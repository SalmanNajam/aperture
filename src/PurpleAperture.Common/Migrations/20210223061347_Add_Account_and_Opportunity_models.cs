﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PurpleAperture.Common.Migrations
{
    public partial class Add_Account_and_Opportunity_models : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PhoenixAccounts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Cidn = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    Address_AdborId = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    Address_Address1 = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Address_Address2 = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Address_Address3 = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Address_Locality = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Address_State = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    Address_ZipCode = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    Address_Country = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Address_LastModifiedDate = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Address_AddressType = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Address_Active = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    Address_SourceSystem = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    Address_AddressStatus = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    Address_Primary = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    Abn = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    PortfolioCode = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    Industry = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    SubIndustry = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Segment = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    MarketSegment = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhoenixAccounts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PhoenixOpportunities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SalesforceId = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    OpportunityId = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    OpportunityName = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    Cidn = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    CloseDate = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Competitor = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    ConfLvl = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    ContractType = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    CreateDate = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    LastModDate = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    LeadOrgSource = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    OppOwnerEmail = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    PriCampSource = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    PriDomain = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    TotalContracVal = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    RevImpactDate = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Stage = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Description = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    CurrentRevenue = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    Probability = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    Partner = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    IsTelstraPurpleOpp = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhoenixOpportunities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PhoenixAlliances",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PhoenixOpportunityId = table.Column<int>(type: "int", nullable: false),
                    AllianceName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhoenixAlliances", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PhoenixAlliances_PhoenixOpportunities_PhoenixOpportunityId",
                        column: x => x.PhoenixOpportunityId,
                        principalTable: "PhoenixOpportunities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PhoenixOpportunityTeamMembers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PhoenixOpportunityId = table.Column<int>(type: "int", nullable: false),
                    FullName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Role = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    SAMLFederationID = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhoenixOpportunityTeamMembers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PhoenixOpportunityTeamMembers_PhoenixOpportunities_PhoenixOpportunityId",
                        column: x => x.PhoenixOpportunityId,
                        principalTable: "PhoenixOpportunities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PhoenixProducts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PhoenixOpportunityId = table.Column<int>(type: "int", nullable: false),
                    ProductCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    ProductName = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ProductQty = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    WonLost = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Chennel = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    TotalRevenue = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    TotalCost = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    GrossMargin = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    WonStatus = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    LostReason = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    AnnualSalesValue = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhoenixProducts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PhoenixProducts_PhoenixOpportunities_PhoenixOpportunityId",
                        column: x => x.PhoenixOpportunityId,
                        principalTable: "PhoenixOpportunities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PhoenixAccounts_Cidn",
                table: "PhoenixAccounts",
                column: "Cidn",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PhoenixAlliances_PhoenixOpportunityId",
                table: "PhoenixAlliances",
                column: "PhoenixOpportunityId");

            migrationBuilder.CreateIndex(
                name: "IX_PhoenixOpportunities_SalesforceId",
                table: "PhoenixOpportunities",
                column: "SalesforceId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PhoenixOpportunityTeamMembers_PhoenixOpportunityId",
                table: "PhoenixOpportunityTeamMembers",
                column: "PhoenixOpportunityId");

            migrationBuilder.CreateIndex(
                name: "IX_PhoenixProducts_PhoenixOpportunityId",
                table: "PhoenixProducts",
                column: "PhoenixOpportunityId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PhoenixAccounts");

            migrationBuilder.DropTable(
                name: "PhoenixAlliances");

            migrationBuilder.DropTable(
                name: "PhoenixOpportunityTeamMembers");

            migrationBuilder.DropTable(
                name: "PhoenixProducts");

            migrationBuilder.DropTable(
                name: "PhoenixOpportunities");
        }
    }
}
