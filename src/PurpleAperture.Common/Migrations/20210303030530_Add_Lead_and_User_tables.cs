﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PurpleAperture.Common.Migrations
{
    public partial class Add_Lead_and_User_tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PhoenixLeads",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LeadId = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    OpportunityId = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    CampaignId = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    AcceptedDate = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    Company = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    ConvertedDate = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    CreatedDate = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    CustomLeadId = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    CustomerCIDN = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    LeadOwner = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    LeadSourceRecent = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    LeadSourceOri = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    LeadStatus = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    MQLDate = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    OtherReason = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    QualifyDate = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    RejectReason = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Title = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhoenixLeads", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PhoenixUsers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    EmpNo = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    MiddleName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    RoleName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    ParentRoleName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    ParentParentRoleName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    SAMLFederationID = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    Active = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhoenixUsers", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PhoenixLeads_LeadId",
                table: "PhoenixLeads",
                column: "LeadId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PhoenixUsers_UserId",
                table: "PhoenixUsers",
                column: "UserId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PhoenixLeads");

            migrationBuilder.DropTable(
                name: "PhoenixUsers");
        }
    }
}
