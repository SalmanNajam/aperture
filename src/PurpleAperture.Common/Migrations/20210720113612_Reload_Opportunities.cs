﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PurpleAperture.Common.Migrations
{
    public partial class Reload_Opportunities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // To reprocess failed requests due to scienitific values
            migrationBuilder.Sql(@"DELETE FROM LastUpdates WHERE TableName = 'opportunity-requests'");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
