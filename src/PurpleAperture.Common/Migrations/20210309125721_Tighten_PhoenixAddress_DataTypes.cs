﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PurpleAperture.Common.Migrations
{
    public partial class Tighten_PhoenixAddress_DataTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address_Active",
                table: "PhoenixAccounts");

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "Address_LastModifiedDate",
                table: "PhoenixAccounts",
                type: "datetimeoffset",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(20)",
                oldMaxLength: 20,
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Address_LastModifiedDate",
                table: "PhoenixAccounts",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(DateTimeOffset),
                oldType: "datetimeoffset",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address_Active",
                table: "PhoenixAccounts",
                type: "nvarchar(5)",
                maxLength: 5,
                nullable: true);
        }
    }
}
