﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PurpleAperture.Common.Migrations
{
    public partial class Change_CampaignId_to_Campaigns_list : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PhoenixLeadCampaigns",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PhoenixLeadId = table.Column<int>(type: "int", nullable: false),
                    CampaignId = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhoenixLeadCampaigns", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PhoenixLeadCampaigns_PhoenixLeads_PhoenixLeadId",
                        column: x => x.PhoenixLeadId,
                        principalTable: "PhoenixLeads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.Sql(
                "EXECUTE('INSERT INTO dbo.PhoenixLeadCampaigns (CampaignId, PhoenixLeadId) SELECT L.CampaignId, L.Id FROM dbo.PhoenixLeads L')");

            migrationBuilder.DropColumn(
                name: "CampaignId",
                table: "PhoenixLeads");

            migrationBuilder.CreateIndex(
                name: "IX_PhoenixLeadCampaigns_PhoenixLeadId",
                table: "PhoenixLeadCampaigns",
                column: "PhoenixLeadId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PhoenixLeadCampaigns");

            migrationBuilder.AddColumn<string>(
                name: "CampaignId",
                table: "PhoenixLeads",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "");
        }
    }
}
