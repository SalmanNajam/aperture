﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PurpleAperture.Common.Migrations
{
    public partial class Reset_Users_Processing : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // To reprocess failed requests due to EF Core failure
            migrationBuilder.Sql(@"DELETE FROM LastUpdates WHERE TableName = 'user-requests'");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
