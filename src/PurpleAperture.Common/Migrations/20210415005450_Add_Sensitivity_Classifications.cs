﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PurpleAperture.Common.Migrations
{
    /// <summary>
    /// See https://docs.microsoft.com/en-us/azure/azure-sql/database/data-discovery-and-classification-overview#manage-classification
    /// </summary>
    public partial class Add_Sensitivity_Classifications : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- 
-- [PhoenixAccounts]
-- 
ADD SENSITIVITY CLASSIFICATION TO [dbo].[PhoenixAccounts].[Address_AdborId] WITH (
    LABEL = 'Confidential',
    LABEL_ID = 'cd18c45f-17bb-476b-b5f7-7216ad45ee24',
    INFORMATION_TYPE = 'Contact Info',
    INFORMATION_TYPE_ID = '5c503e21-22c6-81fa-620b-f369b8ec38d1',
    RANK = MEDIUM
)

ADD SENSITIVITY CLASSIFICATION TO [dbo].[PhoenixAccounts].[Address_Address1] WITH (
    LABEL = 'Confidential',
    LABEL_ID = 'cd18c45f-17bb-476b-b5f7-7216ad45ee24',
    INFORMATION_TYPE = 'Contact Info',
    INFORMATION_TYPE_ID = '5c503e21-22c6-81fa-620b-f369b8ec38d1',
    RANK = MEDIUM
)

ADD SENSITIVITY CLASSIFICATION TO [dbo].[PhoenixAccounts].[Address_Address2] WITH (
    LABEL = 'Confidential',
    LABEL_ID = 'cd18c45f-17bb-476b-b5f7-7216ad45ee24',
    INFORMATION_TYPE = 'Contact Info',
    INFORMATION_TYPE_ID = '5c503e21-22c6-81fa-620b-f369b8ec38d1',
    RANK = MEDIUM
)

ADD SENSITIVITY CLASSIFICATION TO [dbo].[PhoenixAccounts].[Address_Address3] WITH (
    LABEL = 'Confidential',
    LABEL_ID = 'cd18c45f-17bb-476b-b5f7-7216ad45ee24',
    INFORMATION_TYPE = 'Contact Info',
    INFORMATION_TYPE_ID = '5c503e21-22c6-81fa-620b-f369b8ec38d1',
    RANK = MEDIUM
)

ADD SENSITIVITY CLASSIFICATION TO [dbo].[PhoenixAccounts].[Address_AddressStatus] WITH (
    LABEL = 'Confidential',
    LABEL_ID = 'cd18c45f-17bb-476b-b5f7-7216ad45ee24',
    INFORMATION_TYPE = 'Contact Info',
    INFORMATION_TYPE_ID = '5c503e21-22c6-81fa-620b-f369b8ec38d1',
    RANK = MEDIUM
)

ADD SENSITIVITY CLASSIFICATION TO [dbo].[PhoenixAccounts].[Address_AddressType] WITH (
    LABEL = 'Confidential',
    LABEL_ID = 'cd18c45f-17bb-476b-b5f7-7216ad45ee24',
    INFORMATION_TYPE = 'Contact Info',
    INFORMATION_TYPE_ID = '5c503e21-22c6-81fa-620b-f369b8ec38d1',
    RANK = MEDIUM
)

ADD SENSITIVITY CLASSIFICATION TO [dbo].[PhoenixAccounts].[Address_Country] WITH (
    LABEL = 'Confidential',
    LABEL_ID = 'cd18c45f-17bb-476b-b5f7-7216ad45ee24',
    INFORMATION_TYPE = 'Contact Info',
    INFORMATION_TYPE_ID = '5c503e21-22c6-81fa-620b-f369b8ec38d1',
    RANK = MEDIUM
)

ADD SENSITIVITY CLASSIFICATION TO [dbo].[PhoenixAccounts].[Address_Locality] WITH (
    LABEL = 'Confidential',
    LABEL_ID = 'cd18c45f-17bb-476b-b5f7-7216ad45ee24',
    INFORMATION_TYPE = 'Contact Info',
    INFORMATION_TYPE_ID = '5c503e21-22c6-81fa-620b-f369b8ec38d1',
    RANK = MEDIUM
)

ADD SENSITIVITY CLASSIFICATION TO [dbo].[PhoenixAccounts].[Address_Primary] WITH (
    LABEL = 'Confidential',
    LABEL_ID = 'cd18c45f-17bb-476b-b5f7-7216ad45ee24',
    INFORMATION_TYPE = 'Contact Info',
    INFORMATION_TYPE_ID = '5c503e21-22c6-81fa-620b-f369b8ec38d1',
    RANK = MEDIUM
)

ADD SENSITIVITY CLASSIFICATION TO [dbo].[PhoenixAccounts].[Address_SourceSystem] WITH (
    LABEL = 'Confidential',
    LABEL_ID = 'cd18c45f-17bb-476b-b5f7-7216ad45ee24',
    INFORMATION_TYPE = 'Contact Info',
    INFORMATION_TYPE_ID = '5c503e21-22c6-81fa-620b-f369b8ec38d1',
    RANK = MEDIUM
)

ADD SENSITIVITY CLASSIFICATION TO [dbo].[PhoenixAccounts].[Address_State] WITH (
    LABEL = 'Confidential',
    LABEL_ID = 'cd18c45f-17bb-476b-b5f7-7216ad45ee24',
    INFORMATION_TYPE = 'Contact Info',
    INFORMATION_TYPE_ID = '5c503e21-22c6-81fa-620b-f369b8ec38d1',
    RANK = MEDIUM
)

ADD SENSITIVITY CLASSIFICATION TO [dbo].[PhoenixAccounts].[Address_ZipCode] WITH (
    LABEL = 'Confidential',
    LABEL_ID = 'cd18c45f-17bb-476b-b5f7-7216ad45ee24',
    INFORMATION_TYPE = 'Contact Info',
    INFORMATION_TYPE_ID = '5c503e21-22c6-81fa-620b-f369b8ec38d1',
    RANK = MEDIUM
)

--
-- [PhoenixOpportunities]
--
ADD SENSITIVITY CLASSIFICATION TO [dbo].[PhoenixOpportunities].[OppOwnerEmail] WITH (
    LABEL = 'Confidential',
    LABEL_ID = 'cd18c45f-17bb-476b-b5f7-7216ad45ee24',
    INFORMATION_TYPE = 'Contact Info',
    INFORMATION_TYPE_ID = '5c503e21-22c6-81fa-620b-f369b8ec38d1',
    RANK = MEDIUM
)

--
-- [PhoenixOpportunityTeamMembers]
--
ADD SENSITIVITY CLASSIFICATION TO [dbo].[PhoenixOpportunityTeamMembers].[FullName] WITH (
    LABEL = 'Confidential - GDPR',
    LABEL_ID = '689890a7-f321-4c40-86fa-f43c0ce141b2',
    INFORMATION_TYPE = 'Name',
    INFORMATION_TYPE_ID = '57845286-7598-22f5-9659-15b24aeb125e',
    RANK = MEDIUM
)

--
-- [PhoenixLeads]
--
ADD SENSITIVITY CLASSIFICATION TO [dbo].[PhoenixLeads].[FirstName] WITH (
    LABEL = 'Confidential - GDPR',
    LABEL_ID = '689890a7-f321-4c40-86fa-f43c0ce141b2',
    INFORMATION_TYPE = 'Name',
    INFORMATION_TYPE_ID = '57845286-7598-22f5-9659-15b24aeb125e',
    RANK = MEDIUM
)

ADD SENSITIVITY CLASSIFICATION TO [dbo].[PhoenixLeads].[LastName] WITH (
    LABEL = 'Confidential - GDPR',
    LABEL_ID = '689890a7-f321-4c40-86fa-f43c0ce141b2',
    INFORMATION_TYPE = 'Name',
    INFORMATION_TYPE_ID = '57845286-7598-22f5-9659-15b24aeb125e',
    RANK = MEDIUM
)

ADD SENSITIVITY CLASSIFICATION TO [dbo].[PhoenixLeads].[LeadOwner] WITH (
    LABEL = 'Confidential',
    LABEL_ID = 'cd18c45f-17bb-476b-b5f7-7216ad45ee24',
    INFORMATION_TYPE = 'Contact Info',
    INFORMATION_TYPE_ID = '5c503e21-22c6-81fa-620b-f369b8ec38d1',
    RANK = MEDIUM
)

--
-- [PhoenixUsers]
--
ADD SENSITIVITY CLASSIFICATION TO [dbo].[PhoenixUsers].[Email] WITH (
    LABEL = 'Confidential',
    LABEL_ID = 'cd18c45f-17bb-476b-b5f7-7216ad45ee24',
    INFORMATION_TYPE = 'Contact Info',
    INFORMATION_TYPE_ID = '5c503e21-22c6-81fa-620b-f369b8ec38d1',
    RANK = MEDIUM
)

ADD SENSITIVITY CLASSIFICATION TO [dbo].[PhoenixUsers].[FirstName] WITH (
    LABEL = 'Confidential - GDPR',
    LABEL_ID = '689890a7-f321-4c40-86fa-f43c0ce141b2',
    INFORMATION_TYPE = 'Name',
    INFORMATION_TYPE_ID = '57845286-7598-22f5-9659-15b24aeb125e',
    RANK = MEDIUM
)

ADD SENSITIVITY CLASSIFICATION TO [dbo].[PhoenixUsers].[LastName] WITH (
    LABEL = 'Confidential - GDPR',
    LABEL_ID = '689890a7-f321-4c40-86fa-f43c0ce141b2',
    INFORMATION_TYPE = 'Name',
    INFORMATION_TYPE_ID = '57845286-7598-22f5-9659-15b24aeb125e',
    RANK = MEDIUM
)
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- 
-- [PhoenixAccounts]
-- 
DROP SENSITIVITY CLASSIFICATION FROM [dbo].[PhoenixAccounts].[Address_AdborId]
DROP SENSITIVITY CLASSIFICATION FROM [dbo].[PhoenixAccounts].[Address_Address1]
DROP SENSITIVITY CLASSIFICATION FROM [dbo].[PhoenixAccounts].[Address_Address2]
DROP SENSITIVITY CLASSIFICATION FROM [dbo].[PhoenixAccounts].[Address_Address3]
DROP SENSITIVITY CLASSIFICATION FROM [dbo].[PhoenixAccounts].[Address_AddressStatus]
DROP SENSITIVITY CLASSIFICATION FROM [dbo].[PhoenixAccounts].[Address_AddressType]
DROP SENSITIVITY CLASSIFICATION FROM [dbo].[PhoenixAccounts].[Address_Country]
DROP SENSITIVITY CLASSIFICATION FROM [dbo].[PhoenixAccounts].[Address_Locality]
DROP SENSITIVITY CLASSIFICATION FROM [dbo].[PhoenixAccounts].[Address_Primary]
DROP SENSITIVITY CLASSIFICATION FROM [dbo].[PhoenixAccounts].[Address_SourceSystem]
DROP SENSITIVITY CLASSIFICATION FROM [dbo].[PhoenixAccounts].[Address_State]
DROP SENSITIVITY CLASSIFICATION FROM [dbo].[PhoenixAccounts].[Address_ZipCode]

--
-- [PhoenixOpportunities]
--
DROP SENSITIVITY CLASSIFICATION FROM [dbo].[PhoenixOpportunities].[OppOwnerEmail]

--
-- [PhoenixOpportunityTeamMembers]
--
DROP SENSITIVITY CLASSIFICATION FROM [dbo].[PhoenixOpportunityTeamMembers].[FullName]

--
-- [PhoenixLeads]
--
DROP SENSITIVITY CLASSIFICATION FROM [dbo].[PhoenixLeads].[FirstName]
DROP SENSITIVITY CLASSIFICATION FROM [dbo].[PhoenixLeads].[LastName]
DROP SENSITIVITY CLASSIFICATION FROM [dbo].[PhoenixLeads].[LeadOwner]

--
-- [PhoenixUsers]
--
DROP SENSITIVITY CLASSIFICATION FROM [dbo].[PhoenixUsers].[Email]
DROP SENSITIVITY CLASSIFICATION FROM [dbo].[PhoenixUsers].[FirstName]
DROP SENSITIVITY CLASSIFICATION FROM [dbo].[PhoenixUsers].[LastName]
");
        }
    }
}
