using System.Collections;
using System.Collections.Generic;

namespace PurpleAperture.Common.Configuration
{
    public class StorageContainers : IEnumerable<string>
    {
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        public string Accounts { get; set; }
        public string AuthTests { get; set; }
        public string Leads { get; set; }
        public string Opportunities { get; set; }
        public string Users { get; set; }
        public string Reports { get; set; }
        public string WorkdayUpdate { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

        public IEnumerator<string> GetEnumerator()
        {
            yield return Accounts;
            yield return AuthTests;
            yield return Leads;
            yield return Opportunities;
            yield return Users;
            yield return Reports;
            yield return WorkdayUpdate;
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
