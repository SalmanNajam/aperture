using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.Json;
using CSharpFunctionalExtensions;

namespace PurpleAperture.Common.Infrastructure
{
    public static class PhoenixDataExtensions
    {
        private static readonly string[] DateFormats = {"yyyy-MM-dd", "dd-MM-yyyy"};

        private static readonly string[] TimestampFormats =
        {
            "yyyy-MM-ddTHH:mm:ss.fff+0000",
            "yyyy-MM-ddTHH:mm:ss.fff\u002B0000"
        };

        public static string? GetStringOrDefault<TKey>(this IDictionary<TKey, JsonElement> dictionary, TKey key)
        {
            dictionary.TryGetValue(key, out var value);
            return value.GetString();
        }

        public static Result<TEnum?> ParseEnum<TEnum>(this IDictionary<string, JsonElement> dictionary,
            string fieldName)
            where TEnum : struct
        {
            if (!dictionary.TryGetValue(fieldName, out var value))
            {
                return Result.Success<TEnum?>(null);
            }

            string stringValue = value.GetString();

            if (Enum.TryParse(stringValue, true, out TEnum returnValue))
            {
                return returnValue;
            }

            return Result.Failure<TEnum?>(
                $"Error parsing field {fieldName}, value {stringValue} " +
                "was not a valid value in the specified enum type.");
        }

        public static Result<decimal?> ParseDecimalOrDefault(this string? stringValue, string fieldName)
        {
            if (string.IsNullOrEmpty(stringValue))
            {
                return Result.Success<decimal?>(null);
            }

            if (decimal.TryParse(stringValue,
                NumberStyles.AllowExponent | NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign,
                new NumberFormatInfo(), out var returnValue))
            {
                return returnValue;
            }

            return Result.Failure<decimal?>(
                $"Error parsing field {fieldName}, value {stringValue} was not a valid decimal.");
        }

        public static Result<decimal?> ParseDecimalOrDefault(this IDictionary<string, JsonElement> dictionary,
            string fieldName)
        {
            if (!dictionary.TryGetValue(fieldName, out var jsonElement))
            {
                return Result.Success<decimal?>(null);
            }

            string stringValue = jsonElement.GetString();

            return stringValue.ParseDecimalOrDefault(fieldName);
        }

        public static Result<DateTime?> ParseShortDateOrDefault(this string? stringValue, string fieldName)
        {
            if (string.IsNullOrEmpty(stringValue))
            {
                return Result.Success<DateTime?>(null);
            }

            if (DateTime.TryParseExact(
                stringValue,
                DateFormats,
                CultureInfo.InvariantCulture.DateTimeFormat,
                DateTimeStyles.AssumeLocal,
                out var returnValue))
            {
                return returnValue;
            }

            return Result.Failure<DateTime?>(
                $"Error parsing field {fieldName}, value {stringValue} did not fit expected formats.");
        }

        public static Result<DateTime?> ParseShortDateOrDefault(this IDictionary<string, JsonElement> dictionary,
            string fieldName)
        {
            if (!dictionary.TryGetValue(fieldName, out var jsonElement))
            {
                return Result.Success<DateTime?>(null);
            }

            var stringValue = jsonElement.GetString();

            return stringValue.ParseShortDateOrDefault(fieldName);
        }

        public static Result<DateTimeOffset?> ParseTimestampOrDefault(this string? stringValue, string fieldName)
        {
            if (string.IsNullOrEmpty(stringValue))
            {
                return Result.Success<DateTimeOffset?>(null);
            }

            // Example: 2020-09-14T17:27:22.000+0000
            if (DateTimeOffset.TryParseExact(
                stringValue,
                TimestampFormats,
                CultureInfo.InvariantCulture.DateTimeFormat,
                DateTimeStyles.AssumeUniversal,
                out var dateTimeValue))
            {
                return dateTimeValue;
            }

            return Result.Failure<DateTimeOffset?>(
                $"Error parsing field {fieldName}, value {stringValue} did not fit expected formats.");
        }

        public static Result<DateTimeOffset> ParseTimestamp(this string? stringValue, string fieldName)
        {
            if (string.IsNullOrEmpty(stringValue))
            {
                return Result.Failure<DateTimeOffset>($"Error parsing field {fieldName}, value was null.");
            }

            // Example: 2020-09-14T17:27:22.000+0000
            if (DateTimeOffset.TryParseExact(
                stringValue,
                TimestampFormats,
                CultureInfo.InvariantCulture.DateTimeFormat,
                DateTimeStyles.AssumeUniversal,
                out var dateTimeValue))
            {
                return dateTimeValue;
            }

            return Result.Failure<DateTimeOffset>(
                $"Error parsing field {fieldName}, value {stringValue} did not fit expected formats.");
        }

        public static Result<string> EnsureRequiredString(this string stringValue, string fieldName)
        {
            return string.IsNullOrWhiteSpace(stringValue) ? Result.Failure<string>($"The field {fieldName}, value was null.") : stringValue;
        }

        public static IEnumerable<Result> AsResults<TResult>(this IEnumerable<Result<TResult>> results)
            => results.Select(r => (Result) r);
    }
}
