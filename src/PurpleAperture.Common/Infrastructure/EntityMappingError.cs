﻿using System;

namespace PurpleAperture.Common.Infrastructure
{
    public record EntityMappingError(string EntityName, string EntityId, string FieldErrors)
    {
        public override string ToString()
        {
            return $"Mapping failed for {EntityName} with Id {EntityId}:" +
                   $"{Environment.NewLine}{FieldErrors}";
        }

        public string MessageTemplate => "Mapping failed for {EntityName} with Id {EntityId}:" +
                                         $"{Environment.NewLine}" +
                                         "{FieldErrors}";

        public object[] MessageValues => new object[] { EntityName, EntityId, FieldErrors };
    }
}
