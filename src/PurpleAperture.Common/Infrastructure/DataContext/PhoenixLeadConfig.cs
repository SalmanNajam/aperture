﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PurpleAperture.Common.Models;

namespace PurpleAperture.Common.Infrastructure.DataContext
{
    public class PhoenixLeadConfig : IEntityTypeConfiguration<PhoenixLead>
    {
        public void Configure(EntityTypeBuilder<PhoenixLead> builder)
        {
            builder.HasMany(l => l.Campaigns)
                .WithOne()
                .HasForeignKey(a => a.PhoenixLeadId);

            builder.HasIndex(u => u.LeadId).IsUnique();

            builder.Property(l => l.LeadId).HasMaxLength(50);
            builder.Property(l => l.OpportunityId).HasMaxLength(30);
            builder.Property(l => l.AcceptedDate);
            builder.Property(l => l.Company).HasMaxLength(150);
            builder.Property(l => l.ConvertedDate);
            builder.Property(l => l.CreatedDate);
            builder.Property(l => l.CustomLeadId).HasMaxLength(50);
            builder.Property(l => l.CustomerCIDN).HasMaxLength(15);
            builder.Property(l => l.FirstName).HasMaxLength(50);
            builder.Property(l => l.LastName).HasMaxLength(50);
            builder.Property(l => l.LeadOwner).HasMaxLength(100);
            builder.Property(l => l.LeadSourceRecent).HasMaxLength(100);
            builder.Property(l => l.LeadSourceOri).HasMaxLength(50);
            builder.Property(l => l.LeadStatus).HasMaxLength(40);
            builder.Property(l => l.MQLDate);
            builder.Property(l => l.OtherReason).HasMaxLength(200);
            builder.Property(l => l.QualifyDate);
            builder.Property(l => l.RejectReason).HasMaxLength(50);
            builder.Property(l => l.Title).HasMaxLength(50);
        }
    }
}
