﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PurpleAperture.Common.Models;

namespace PurpleAperture.Common.Infrastructure.DataContext
{
    public class PhoenixAccountConfig : IEntityTypeConfiguration<PhoenixAccount>
    {
        public void Configure(EntityTypeBuilder<PhoenixAccount> builder)
        {
            const int CidnMaxLength = 15;

            builder.OwnsOne<PhoenixAddress>(
                a => a.Address!,
                address =>
                {
                    address.Property(a => a.AdborId).HasMaxLength(50);
                    address.Property(a => a.Address1).HasMaxLength(150);
                    address.Property(a => a.Address2).HasMaxLength(150);
                    address.Property(a => a.Address3).HasMaxLength(150);
                    address.Property(a => a.Locality).HasMaxLength(50);
                    address.Property(a => a.State).HasMaxLength(15);
                    address.Property(a => a.ZipCode).HasMaxLength(10);
                    address.Property(a => a.Country).HasMaxLength(50);
                    address.Property(a => a.LastModifiedDate);
                    address.Property(a => a.AddressType).HasMaxLength(50);
                    address.Property(a => a.SourceSystem).HasMaxLength(15);
                    address.Property(a => a.AddressStatus).HasMaxLength(15);
                    address.Property(a => a.Primary).HasMaxLength(5);
                });

            builder.HasIndex(a => a.Cidn).IsUnique();

            builder.Property(a => a.Name).HasMaxLength(150);
            builder.Property(a => a.Cidn).HasMaxLength(CidnMaxLength);
            builder.Property(a => a.Abn).HasMaxLength(15);
            builder.Property(a => a.PortfolioCode).HasMaxLength(15);
            builder.Property(a => a.Industry).HasMaxLength(100);
            builder.Property(a => a.SubIndustry).HasMaxLength(100);
            builder.Property(a => a.Segment).HasMaxLength(100);
            builder.Property(a => a.MarketSegment).HasMaxLength(100);
        }
    }
}
