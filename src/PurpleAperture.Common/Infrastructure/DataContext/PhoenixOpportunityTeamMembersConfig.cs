﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PurpleAperture.Common.Models;

namespace PurpleAperture.Common.Infrastructure.DataContext
{
    public class PhoenixOpportunityTeamMembersConfig : IEntityTypeConfiguration<PhoenixOpportunityTeamMembers>
    {
        public void Configure(EntityTypeBuilder<PhoenixOpportunityTeamMembers> builder)
        {
            builder.Property(tm => tm.FullName).HasMaxLength(50);
            builder.Property(tm => tm.Role).HasMaxLength(50);
            builder.Property(tm => tm.SAMLFederationID).HasMaxLength(50);
        }
    }
}
