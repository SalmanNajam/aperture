﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PurpleAperture.Common.Models;

namespace PurpleAperture.Common.Infrastructure.DataContext
{
    public class PhoenixOpportunityConfig : IEntityTypeConfiguration<PhoenixOpportunity>
    {
        public void Configure(EntityTypeBuilder<PhoenixOpportunity> builder)
        {
            const int CidnMaxLength = 15;

            builder.HasMany(o => o.Alliances)
                .WithOne()
                .HasForeignKey(a => a.PhoenixOpportunityId);

            builder.HasMany(o => o.OppTeamMembers)
                .WithOne()
                .HasForeignKey(tm => tm.PhoenixOpportunityId);

            builder.HasMany(o => o.Products)
                .WithOne()
                .HasForeignKey(p => p.PhoenixOpportunityId);

            builder.HasIndex(o => o.SalesforceId).IsUnique();

            builder.Property(o => o.SalesforceId).HasMaxLength(50);
            builder.Property(o => o.OpportunityId).HasMaxLength(30);
            builder.Property(o => o.OpportunityName).HasMaxLength(150);
            builder.Property(o => o.Cidn).HasMaxLength(CidnMaxLength);
            builder.Property(o => o.CloseDate);
            builder.Property(o => o.Competitor).HasMaxLength(100);
            builder.Property(o => o.ConfLvl).HasMaxLength(50);
            builder.Property(o => o.ContractType).HasMaxLength(50);
            builder.Property(o => o.CreateDate);
            builder.Property(o => o.LastModDate);
            builder.Property(o => o.LeadOrgSource).HasMaxLength(50);
            builder.Property(o => o.OppOwnerEmail).HasMaxLength(100);
            builder.Property(o => o.PriCampSource).HasMaxLength(100);
            builder.Property(o => o.PriDomain).HasMaxLength(50);
            builder.Property(o => o.TotalContracVal).HasPrecision(28, 2);
            builder.Property(o => o.RevImpactDate);
            builder.Property(o => o.Stage).HasMaxLength(20);
            builder.Property(o => o.Description).HasMaxLength(4000);
            builder.Property(o => o.CurrentRevenue).HasPrecision(28, 2);
            builder.Property(o => o.Probability).HasPrecision(28, 2);
            builder.Property(o => o.Partner).HasMaxLength(100);
            builder.Property(o => o.IsTelstraPurpleOpp);
        }
    }
}
