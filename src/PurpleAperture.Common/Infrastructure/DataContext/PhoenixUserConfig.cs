﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PurpleAperture.Common.Models;

namespace PurpleAperture.Common.Infrastructure.DataContext
{
    public class PhoenixUserConfig : IEntityTypeConfiguration<PhoenixUser>
    {
        public void Configure(EntityTypeBuilder<PhoenixUser> builder)
        {
            builder.HasIndex(u => u.UserId).IsUnique();

            builder.Property(u => u.UserId).HasMaxLength(50);
            builder.Property(u => u.Email).HasMaxLength(100);
            builder.Property(u => u.EmpNo).HasMaxLength(15);
            builder.Property(u => u.FirstName).HasMaxLength(50);
            builder.Property(u => u.LastName).HasMaxLength(50);
            builder.Property(u => u.MiddleName).HasMaxLength(50);
            builder.Property(u => u.RoleName).HasMaxLength(100);
            builder.Property(u => u.ParentRoleName).HasMaxLength(100);
            builder.Property(u => u.ParentParentRoleName).HasMaxLength(100);
            builder.Property(u => u.SAMLFederationID).HasMaxLength(50);
            builder.Property(u => u.Active);
        }
    }
}
