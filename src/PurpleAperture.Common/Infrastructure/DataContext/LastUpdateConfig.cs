﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PurpleAperture.Common.Models;

namespace PurpleAperture.Common.Infrastructure.DataContext
{
    public class LastUpdateConfig : IEntityTypeConfiguration<LastUpdate>
    {
        public void Configure(EntityTypeBuilder<LastUpdate> builder)
        {
            builder.HasKey(i => i.TableName);
            builder.Property(l => l.TableName).HasMaxLength(50);
        }
    }
}
