﻿using Microsoft.EntityFrameworkCore;
using PurpleAperture.Common.Models;

namespace PurpleAperture.Common.Infrastructure.DataContext
{
    public class PhoenixContext : DbContext
    {
        public virtual DbSet<PhoenixOpportunity> PhoenixOpportunities => Set<PhoenixOpportunity>();

        public virtual DbSet<PhoenixAlliance> PhoenixAlliances => Set<PhoenixAlliance>();

        public virtual DbSet<PhoenixProduct> PhoenixProducts => Set<PhoenixProduct>();

        public virtual DbSet<PhoenixOpportunityTeamMembers> PhoenixOpportunityTeamMembers => Set<PhoenixOpportunityTeamMembers>();

        public virtual DbSet<PhoenixAccount> PhoenixAccounts => Set<PhoenixAccount>();

        public virtual DbSet<PhoenixLead> PhoenixLeads => Set<PhoenixLead>();

        public virtual DbSet<PhoenixLeadCampaign> PhoenixLeadCampaigns => Set<PhoenixLeadCampaign>();

        public virtual DbSet<PhoenixUser> PhoenixUsers => Set<PhoenixUser>();

        public virtual DbSet<LastUpdate> LastUpdates => Set<LastUpdate>();

        public PhoenixContext(DbContextOptions<PhoenixContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfigurationsFromAssembly(typeof(PhoenixContext).Assembly);
        }
    }
}
