﻿using Microsoft.EntityFrameworkCore;

namespace PurpleAperture.Common.Infrastructure.DataContext
{
    public class WorkdayContext : DbContext
    {
        public WorkdayContext(DbContextOptions<WorkdayContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}
