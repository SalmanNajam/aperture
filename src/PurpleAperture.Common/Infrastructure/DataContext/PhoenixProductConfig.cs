﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PurpleAperture.Common.Models;

namespace PurpleAperture.Common.Infrastructure.DataContext
{
    public class PhoenixProductConfig : IEntityTypeConfiguration<PhoenixProduct>
    {
        public void Configure(EntityTypeBuilder<PhoenixProduct> builder)
        {
            builder.Property(p => p.ProductCode).HasMaxLength(50);
            builder.Property(p => p.ProductName).HasMaxLength(150);
            builder.Property(p => p.ProductQty).HasPrecision(28, 2);
            builder.Property(p => p.WonLost).HasMaxLength(20);
            builder.Property(p => p.Chennel).HasMaxLength(10);
            builder.Property(p => p.TotalRevenue).HasPrecision(28, 2);
            builder.Property(p => p.TotalCost).HasPrecision(28, 2);
            builder.Property(p => p.GrossMargin).HasPrecision(28, 2);
            builder.Property(p => p.WonStatus).HasMaxLength(50);
            builder.Property(p => p.LostReason).HasMaxLength(50);
            builder.Property(p => p.AnnualSalesValue).HasPrecision(28, 2);
        }
    }
}
