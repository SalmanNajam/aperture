﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PurpleAperture.Common.Models;

namespace PurpleAperture.Common.Infrastructure.DataContext
{
    public class PhoenixAllianceConfig : IEntityTypeConfiguration<PhoenixAlliance>
    {
        public void Configure(EntityTypeBuilder<PhoenixAlliance> builder)
        {
            builder.Property(a => a.AllianceName).HasMaxLength(50);
        }
    }
}