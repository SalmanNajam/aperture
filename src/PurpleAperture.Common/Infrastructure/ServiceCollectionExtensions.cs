using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PurpleAperture.Common.Infrastructure.Data;
using PurpleAperture.Common.Infrastructure.DataContext;

namespace PurpleAperture.Common.Infrastructure
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection RegisterPhoenixDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IAzureSqlTokenProvider, AzureIdentityAzureSqlTokenProvider>();
            services.Decorate<IAzureSqlTokenProvider, CacheAzureSqlTokenProvider>();
            services.AddSingleton<AzureAdAuthenticationDbConnectionInterceptor>();

            return services.AddDbContext<PhoenixContext>((provider, options) =>
            {
                options.UseSqlServer(
                    configuration.GetConnectionString("Default"),
                    sqlOptions =>
                    {
                        sqlOptions.MigrationsAssembly(typeof(PhoenixContext).Assembly.GetName().Name);
                        sqlOptions.EnableRetryOnFailure();
                    });
                options.AddInterceptors(provider.GetRequiredService<AzureAdAuthenticationDbConnectionInterceptor>());
            });
        }

        public static IServiceCollection RegisterWorkDataDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddDbContext<WorkdayContext>((provider, options) =>
            {
                options.UseSqlServer(
                    configuration.GetConnectionString("Workday"),
                    sqlOptions =>
                    {
                        sqlOptions.MigrationsAssembly(typeof(WorkdayContext).Assembly.GetName().Name);
                        sqlOptions.EnableRetryOnFailure();
                    });
            });
        }
    }
}
