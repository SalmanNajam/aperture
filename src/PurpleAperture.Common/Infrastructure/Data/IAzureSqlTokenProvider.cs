﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace PurpleAperture.Common.Infrastructure.Data
{
    public interface IAzureSqlTokenProvider
    {
        (string AccessToken, DateTimeOffset ExpiresOn) GetAccessToken();
        Task<(string AccessToken, DateTimeOffset ExpiresOn)> GetAccessTokenAsync(CancellationToken cancellationToken = default);
    }
}
