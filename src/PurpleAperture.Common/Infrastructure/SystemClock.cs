using System;

namespace PurpleAperture.Common.Infrastructure
{
    public class SystemClock : ISystemClock
    {
        public DateTimeOffset GetUtcNow() => DateTimeOffset.UtcNow;
    }

    public interface ISystemClock
    {
        DateTimeOffset GetUtcNow();
    }
}
