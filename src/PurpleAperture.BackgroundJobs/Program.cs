using Azure.Identity;
using Microsoft.Azure.WebJobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using PurpleAperture.BackgroundJobs.Infrastructure;
using PurpleAperture.BackgroundJobs.Phoenix.Handlers;
using PurpleAperture.BackgroundJobs.Reports.Account;
using PurpleAperture.BackgroundJobs.Reports.Lead;
using PurpleAperture.Common.Configuration;
using PurpleAperture.Common.Infrastructure;
using PurpleAperture.Common.Infrastructure.DataContext;
using System.Threading.Tasks;

namespace PurpleAperture.BackgroundJobs
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = CreateHostBuilder(args);
            var host = builder.Build();

            // Run migrations when in Development mode, i.e. during local dev
            using (var scope = host.Services.CreateScope())
            {
                var scopedProvider = scope.ServiceProvider;
                var environment = scopedProvider.GetRequiredService<IHostEnvironment>();
                if (environment.IsDevelopment())
                {
                    var context = scopedProvider.GetRequiredService<PhoenixContext>();
                    await context.Database.MigrateAsync();
                }
            }

            using (host)
            {
                await host.RunAsync();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            new HostBuilder()
                .ConfigureHostConfiguration(configHost => configHost.AddEnvironmentVariables(prefix: "ASPNETCORE_"))
                .ConfigureWebJobs(b =>
                {
                    b.AddAzureStorageCoreServices();
                    b.AddTimers();
                    b.AddAzureStorageQueues();
                })
                .ConfigureAppConfiguration(ConfigureAppConfiguration)
                .ConfigureLogging(ConfigureLogging)
                .ConfigureServices(ConfigureDependencyInjection);

        private static void ConfigureAppConfiguration(HostBuilderContext builderContext, IConfigurationBuilder config)
        {
            var environment = builderContext.HostingEnvironment;

            config.AddJsonFile($"appsettings.{environment.EnvironmentName}.json", optional: true, reloadOnChange: true);

            if (environment.IsDevelopment())
            {
                config.AddUserSecrets<Program>();
            }
        }

        private static void ConfigureDependencyInjection(HostBuilderContext builderContext, IServiceCollection services)
        {
            services.Configure<StorageContainers>(builderContext.Configuration.GetSection(nameof(StorageContainers)));
            services.Configure<StorageQueues>(builderContext.Configuration.GetSection(nameof(StorageQueues)));
            services.AddAutoMapper(typeof(Program));

            services.AddAzureClients(builder =>
            {
                builder.UseCredential(new ManagedIdentityCredential());
                builder.AddBlobServiceClient(builderContext.Configuration.GetSection("AzureBlobStorageOptions"));
                builder.AddQueueServiceClient(builderContext.Configuration.GetSection("AzureQueueStorageOptions"));
            });

            // Work around for the WebJob host storage account config not supporting managed identity.
            if (!builderContext.HostingEnvironment.IsDevelopment())
            {
                services.AddSingleton<DistributedLockManagerContainerProvider>(
                    new CustomDistributedLockManagerContainerProvider(builderContext.Configuration.GetValue<string>("AzureBlobStorageOptions:ServiceUri")));
            }

            services.AddMemoryCache();
            services.RegisterPhoenixDbContext(builderContext.Configuration);

            services.AddTransient<AccountReport>();
            services.AddTransient<LeadReport>();

            services.AddTransient<ProcessPhoenixAccount>();
            services.AddTransient<ProcessPhoenixLead>();
            services.AddTransient<ProcessPhoenixOpportunity>();
            services.AddTransient<ProcessPhoenixUser>();
            services.AddSingleton<ISystemClock, SystemClock>();
        }

        private static void ConfigureLogging(HostBuilderContext builderContext, ILoggingBuilder loggingBuilder)
        {
            loggingBuilder.AddConfiguration(builderContext.Configuration.GetSection("Logging"));
            loggingBuilder.AddApplicationInsightsWebJobs();
            loggingBuilder.AddDebug();
            loggingBuilder.AddConsole();
        }
    }
}
