using PurpleAperture.Common.Infrastructure.DataContext;
using System.Linq;

namespace PurpleAperture.BackgroundJobs.Reports.Account
{
    public class AccountReportQuery : IReportQuery<AccountReportModel>
    {
        public IQueryable<AccountReportModel> GetQuery(PhoenixContext context, ReportInterval reportInterval)
        {
            var accounts = reportInterval switch
            {
                ReportInterval.StartEndRange range => context.PhoenixAccounts.Where(account => account.LastUpdatedDateTime > range.Start && account.LastUpdatedDateTime <= range.End),
                _ => context.PhoenixAccounts
            };

            return from account in accounts
                   select new AccountReportModel
                   {
                       CIDN = account.Cidn,
                       AccountName = account.Name,
                       PrimaryAddressLine1 = account.Address.Address1,
                       PrimaryAddressLine2 = account.Address.Address2,
                       PrimaryAddressLine3 = account.Address.Address3,
                       BillingCity = account.Address.Locality,
                       BillingState = account.Address.State,
                       BillingZip = account.Address.ZipCode,
                       BillingCountry = account.Address.Country,
                       LastModifiedDate = account.Address.LastModifiedDate,
                       ABN = account.Abn,
                       PortfolioCode = account.PortfolioCode,
                       Industry = account.Industry,
                       SubIndustry = account.SubIndustry,
                       Segment = account.Segment,
                       MarketSegment = account.MarketSegment
                   };
        }
    }
}
