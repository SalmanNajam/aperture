using System;

namespace PurpleAperture.BackgroundJobs.Reports.Account
{
    public class AccountReportModel
    {
        public string CIDN { get; set; }
        public string AccountName { get; set; }
        public string PrimaryAddressLine1 { get; set; }
        public string PrimaryAddressLine2 { get; set; }
        public string PrimaryAddressLine3 { get; set; }
        public string BillingCity { get; set; }
        public string BillingState { get; set; }
        public string BillingZip { get; set; }
        public string BillingCountry { get; set; }
        public DateTimeOffset? LastModifiedDate { get; set; }
        public string ABN { get; set; }
        public string PortfolioCode { get; set; }
        public string Industry { get; set; }
        public string SubIndustry { get; set; }
        public string Segment { get; set; }
        public string MarketSegment { get; set; }
    }
}
