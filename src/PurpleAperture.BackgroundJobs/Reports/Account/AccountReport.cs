using Azure.Storage.Blobs;
using Microsoft.Extensions.Options;
using PurpleAperture.Common.Configuration;
using PurpleAperture.Common.Infrastructure.DataContext;

namespace PurpleAperture.BackgroundJobs.Reports.Account
{
    public class AccountReport : CsvReport<AccountReportQuery, AccountReportModel, AccountReportMap>
    {
        public AccountReport(
            PhoenixContext dataContext,
            BlobServiceClient blobServiceClient,
            IOptions<StorageContainers> options) : base(dataContext, blobServiceClient, options)
        {
        }

        public override string NamePrefix => "Report results (Account)";
    }
}
