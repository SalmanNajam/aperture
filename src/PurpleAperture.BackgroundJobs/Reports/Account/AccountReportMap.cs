using CsvHelper.Configuration;
using PurpleAperture.BackgroundJobs.Infrastructure;

namespace PurpleAperture.BackgroundJobs.Reports.Account
{
    public class AccountReportMap : ClassMap<AccountReportModel>
    {
        public AccountReportMap()
        {
            Map(c => c.CIDN).Name("CIDN");
            Map(c => c.AccountName).Name("Account Name");
            Map(c => c.PrimaryAddressLine1).Name("Primary Address Line 1");
            Map(c => c.PrimaryAddressLine2).Name("Primary Address Line 2");
            Map(c => c.PrimaryAddressLine3).Name("Primary Address Line 3");
            Map(c => c.BillingCity).Name("Billing City");
            Map(c => c.BillingState).Name("Billing State/Province");
            Map(c => c.BillingZip).Name("Billing Zip/Postal Code");
            Map(c => c.BillingCountry).Name("Billing Country");
            Map(c => c.LastModifiedDate)
                .Name("Last Modified Date")
                .TypeConverter<ShortDateConverter>();
            Map(c => c.ABN).Name("ABN");
            Map(c => c.PortfolioCode).Name("Portfolio Code");
            Map(c => c.Industry).Name("Industry");
            Map(c => c.SubIndustry).Name("Sub-Industry");
            Map(c => c.Segment).Name("Segment");
            Map(c => c.MarketSegment).Name("Market Segment");
        }
    }
}
