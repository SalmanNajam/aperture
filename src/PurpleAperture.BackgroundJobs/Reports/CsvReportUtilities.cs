using CsvHelper;
using CsvHelper.Configuration;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace PurpleAperture.BackgroundJobs.Reports
{
    public static class CsvReportUtilities
    {
        public static async Task GenerateReport<TModel, TMap>(IEnumerable<TModel> results, Stream output) where TMap : ClassMap<TModel>
        {
            await using StreamWriter streamWriter = new(output, new UTF8Encoding(false));
            await using CsvWriter csvWriter = new(streamWriter, CultureInfo.InvariantCulture);
            csvWriter.Context.RegisterClassMap<TMap>();
            await csvWriter.WriteRecordsAsync(results);
        }
    }
}
