using System.Threading.Tasks;

namespace PurpleAperture.BackgroundJobs.Reports
{
    public interface ICsvReport
    {
        Task GenerateReport(ReportInterval reportInterval);
        string GetFilename(ReportInterval reportInterval);
    }
}
