using Azure.Storage.Blobs;
using CsvHelper.Configuration;
using Microsoft.Extensions.Options;
using PurpleAperture.Common.Configuration;
using PurpleAperture.Common.Infrastructure.DataContext;
using System.IO;
using System.Threading.Tasks;

namespace PurpleAperture.BackgroundJobs.Reports
{
    public abstract class CsvReport<TQuery, TModel, TMap> : ICsvReport
        where TQuery : IReportQuery<TModel>, new()
        where TMap : ClassMap<TModel>
    {
        protected readonly PhoenixContext _context;
        protected readonly BlobContainerClient _containerClient;

        public CsvReport(
            PhoenixContext context,
            BlobServiceClient blobServiceClient,
            IOptions<StorageContainers> options)
        {
            _context = context;
            _containerClient = blobServiceClient.GetBlobContainerClient(options.Value.Reports);
        }
        public abstract string NamePrefix { get; }

        public string GetFilename(ReportInterval reportInterval) => reportInterval switch
        {
            ReportInterval.All => $"{NamePrefix}_full.csv",
            ReportInterval.StartEndRange range => $"{NamePrefix}_{range.End:yyyy-MM-dd}.csv",
            _ => $"{NamePrefix}_{Path.GetRandomFileName()}.csv"
        };

        public async Task GenerateReport(ReportInterval reportInterval)
        {
            // Generate CSV to temp file to scale to reports containing all data rather than loading all data into memory.
            var tempCsv = Path.GetTempFileName();
            try
            {
                var query = new TQuery().GetQuery(_context, reportInterval);
                using (var tempCsvStream = File.OpenWrite(tempCsv))
                {
                    await CsvReportUtilities.GenerateReport<TModel, TMap>(query, tempCsvStream);
                }

                var blobClient = _containerClient.GetBlobClient(GetFilename(reportInterval));
                await blobClient.UploadAsync(tempCsv, true);
            }
            finally
            {
                File.Delete(tempCsv);
            }
        }
    }
}
