using PurpleAperture.Common.Infrastructure.DataContext;
using System.Linq;

namespace PurpleAperture.BackgroundJobs.Reports
{
    public interface IReportQuery<TModel>
    {
        IQueryable<TModel> GetQuery(PhoenixContext context, ReportInterval reportInterval);
    }
}
