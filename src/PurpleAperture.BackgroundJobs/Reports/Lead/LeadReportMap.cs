using CsvHelper.Configuration;
using PurpleAperture.BackgroundJobs.Infrastructure;

namespace PurpleAperture.BackgroundJobs.Reports.Lead
{
    public class LeadReportMap : ClassMap<LeadReportModel>
    {
        public LeadReportMap()
        {
            Map(c => c.CampaignName).Name("Campaign Name");
            Map(c => c.CustomLeadId).Name("Custom Lead Id");
            Map(c => c.LeadStatus).Name("Lead Status");
            Map(c => c.LeadSourceOriginal).Name("Lead Source - Original");
            Map(c => c.LeadSourceMostRecent).Name("Lead Source - Most Recent (Description)");
            Map(c => c.LeadOwner).Name("Lead Owner");
            Map(c => c.FirstName).Name("First Name");
            Map(c => c.LastName).Name("Last Name");
            Map(c => c.Title).Name("Title");
            Map(c => c.CompanyAccount).Name("Company / Account");
            Map(c => c.CreatedDate)
                .Name("Created Date")
                .TypeConverter<ShortDateConverter>();
            Map(c => c.QualifyDate)
                .Name("Qualify Date")
                .TypeConverter<ShortDateConverter>();
            Map(c => c.MQLDate)
                .Name("MQL Date")
                .TypeConverter<ShortDateConverter>();
            Map(c => c.AcceptedDate)
                .Name("Accepted Date")
                .TypeConverter<ShortDateConverter>();
            Map(c => c.ConvertedDate)
                .Name("Converted Date")
                .TypeConverter<ShortDateConverter>();
            Map(c => c.LastModifiedDate)
                .Name("Last Modified Date")
                .TypeConverter<ShortDateConverter>();
            Map(c => c.RejectReason).Name("Reject/Recycle/Reroute Reason");
            Map(c => c.OtherReason).Name("Other - Reason");
            Map(c => c.OpportunityId).Name("Opportunity ID");
            Map(c => c.OpportunityName).Name("Opportunity Name");
            Map(c => c.OpportunityAccount).Name("Opportunity: Account");
            Map(c => c.OpportunityAmount).Name("Opportunity Amount");
            Map(c => c.OpportunityCloseDate)
                .Name("Oppt Close Date")
                .TypeConverter<ShortDateConverter>();
            Map(c => c.RevenueImpactDate)
                .Name("Revenue Impact Date")
                .TypeConverter<ShortDateConverter>();
            Map(c => c.RevenueImpactDateCalc).Name("Revenue Impact Date Calc");
            Map(c => c.CustomerCIDN).Name("Customer CIDN");
            Map(c => c.CampaignId).Name("Campaign ID");
            Map(c => c.ParentCampaignId).Name("Parent Campaign ID");
            Map(c => c.ParentCampaignName).Name("Parent Campaign Name");
        }
    }
}
