using PurpleAperture.Common.Infrastructure.DataContext;
using System.Collections.Generic;
using System.Linq;

namespace PurpleAperture.BackgroundJobs.Reports.Lead
{
    public class LeadReportQuery : IReportQuery<LeadReportModel>
    {
        public IQueryable<LeadReportModel> GetQuery(PhoenixContext context, ReportInterval reportInterval)
        {
            var leads = reportInterval switch
            {
                ReportInterval.StartEndRange range => context.PhoenixLeads.Where(lead => lead.LastUpdatedDateTime > range.Start && lead.LastUpdatedDateTime <= range.End),
                _ => context.PhoenixLeads
            };

            return from lead in leads
                   join opportunity in context.PhoenixOpportunities on lead.OpportunityId equals opportunity.OpportunityId
                   select new LeadReportModel
                   {
                       CustomLeadId = lead.LeadId,
                       LeadStatus = lead.LeadStatus,
                       LeadSourceOriginal = lead.LeadSourceOri,
                       LeadSourceMostRecent = lead.LeadSourceRecent,
                       LeadOwner = lead.LeadOwner,
                       FirstName = lead.FirstName,
                       LastName = lead.LastName,
                       Title = lead.Title,
                       CompanyAccount = lead.Company,
                       CreatedDate = lead.CreatedDate,
                       QualifyDate = lead.QualifyDate,
                       MQLDate = lead.MQLDate,
                       AcceptedDate = lead.AcceptedDate,
                       ConvertedDate = lead.ConvertedDate,
                       RejectReason = lead.RejectReason,
                       OtherReason = lead.OtherReason,
                       OpportunityId = opportunity.OpportunityId,
                       OpportunityName = opportunity.OpportunityName,
                       OpportunityCloseDate = opportunity.CloseDate,
                       RevenueImpactDate = opportunity.RevImpactDate,
                       CustomerCIDN = opportunity.Cidn
                   };
        }
    }
}
