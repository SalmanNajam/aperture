using System;

namespace PurpleAperture.BackgroundJobs.Reports.Lead
{
    public class LeadReportModel
    {
        public string? CampaignName { get; set; }
        public string CustomLeadId { get; set; }
        public string LeadStatus { get; set; }
        public string LeadSourceOriginal { get; set; }
        public string LeadSourceMostRecent { get; set; }
        public string LeadOwner { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string CompanyAccount { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? QualifyDate { get; set; }
        public DateTime? MQLDate { get; set; }
        public DateTimeOffset? AcceptedDate { get; set; }
        public DateTime? ConvertedDate { get; set; }
        public string LastModifiedDate { get; set; }
        public string RejectReason { get; set; }
        public string OtherReason { get; set; }
        public string? OpportunityId { get; set; }
        public string? OpportunityName { get; set; }
        public string OpportunityAccount { get; set; }
        public string OpportunityAmount { get; set; }
        public DateTime? OpportunityCloseDate { get; set; }
        public DateTime? RevenueImpactDate { get; set; }
        public string RevenueImpactDateCalc { get; set; }
        public string CustomerCIDN { get; set; }
        public string CampaignId { get; set; }
        public string ParentCampaignId { get; set; }
        public string ParentCampaignName { get; set; }
    }
}
