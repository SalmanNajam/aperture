using Azure.Storage.Blobs;
using Microsoft.Extensions.Options;
using PurpleAperture.Common.Configuration;
using PurpleAperture.Common.Infrastructure.DataContext;

namespace PurpleAperture.BackgroundJobs.Reports.Lead
{
    public class LeadReport : CsvReport<LeadReportQuery, LeadReportModel, LeadReportMap>
    {
        public LeadReport(
            PhoenixContext dataContext,
            BlobServiceClient blobServiceClient,
            IOptions<StorageContainers> options) : base(dataContext, blobServiceClient, options)
        {
        }

        public override string NamePrefix => "Report results (Leads)";
    }
}
