using System;

namespace PurpleAperture.BackgroundJobs.Reports
{
    public abstract record ReportInterval
    {
        public record All : ReportInterval;
        public record StartEndRange(DateTime Start, DateTime End) : ReportInterval;

        /// <summary>
        /// Get an interval for the report data that corresponds to since yesterday around a fixed time.
        /// Using a fixed time ensures that each report of yesterday's data should not have any overlapping data
        /// between generated reports regardless of what time that day they were run.
        /// </summary>
        /// <param name="reportYesterdayTime">Time of UTC day where anything before this time is considered yesterday.</param>
        /// <param name="reportGenerationTime">The UTC time when the report generation is run.</param>
        public static ReportInterval GetSinceYesterdayReportInterval(TimeSpan? reportYesterdayTime, DateTime reportGenerationTime)
        {
            if (reportGenerationTime.Kind != DateTimeKind.Utc)
            {
                throw new ArgumentOutOfRangeException(nameof(reportGenerationTime), $"Value must be UTC");
            }

            // If no reference point for yesterday is provided then assume we want all data.
            if (reportYesterdayTime is null)
            {
                return new All();
            }
            else if (reportYesterdayTime >= new TimeSpan(24, 0, 0) || reportYesterdayTime < TimeSpan.Zero)
            {
                throw new ArgumentOutOfRangeException(nameof(reportYesterdayTime), $"Provide a time between 0:00 and 23:59");
            }
            // If the report is generated after the yesterday reference time
            // then get all data between the previous day at the reference time and on the current day at the reference time.
            else if (reportGenerationTime.TimeOfDay >= reportYesterdayTime)
            {
                var end = reportGenerationTime.Date + reportYesterdayTime.Value;
                return new StartEndRange(end.AddDays(-1), end);
            }
            // If the report is generated before the yesterday reference time
            // then get all data between two days ago at the reference time and the previous day at the reference time.
            else
            {
                var end = reportGenerationTime.Date.AddDays(-1) + reportYesterdayTime.Value;
                return new StartEndRange(end.AddDays(-1), end);
            }
        }
    }
}
