using Microsoft.Azure.WebJobs;
using PurpleAperture.BackgroundJobs.Phoenix.Handlers;
using System.Threading.Tasks;

namespace PurpleAperture.BackgroundJobs
{
    public class ConvertPhoenixDataFunctions
    {
        private const string QueueConnectionName = "AzureQueueStorageOptions";

        private readonly ProcessPhoenixAccount _accountProcessor;
        private readonly ProcessPhoenixLead _leadProcessor;
        private readonly ProcessPhoenixOpportunity _opportunityProcessor;
        private readonly ProcessPhoenixUser _userProcessor;

        public ConvertPhoenixDataFunctions(
            ProcessPhoenixAccount accountProcessor,
            ProcessPhoenixLead leadProcessor,
            ProcessPhoenixOpportunity opportunityProcessor,
            ProcessPhoenixUser userProcessor)
        {
            _accountProcessor = accountProcessor;
            _leadProcessor = leadProcessor;
            _opportunityProcessor = opportunityProcessor;
            _userProcessor = userProcessor;
        }


        public async Task ProcessPhoenixAccountPayload([QueueTrigger("%StorageQueues:Accounts%", Connection = QueueConnectionName)] string blobName)
        {
            await _accountProcessor.ProcessPayloadAsync(blobName);
        }

        public async Task ProcessPhoenixLeadPayload([QueueTrigger("%StorageQueues:Leads%", Connection = QueueConnectionName)] string blobName)
        {
            await _leadProcessor.ProcessPayloadAsync(blobName);
        }

        public async Task ProcessPhoenixOpportunityPayload([QueueTrigger("%StorageQueues:Opportunities%", Connection = QueueConnectionName)] string blobName)
        {
            await _opportunityProcessor.ProcessPayloadAsync(blobName);
        }

        public async Task ProcessPhoenixUserPayload([QueueTrigger("%StorageQueues:Users%", Connection = QueueConnectionName)] string blobName)
        {
            await _userProcessor.ProcessPayloadAsync(blobName);
        }
    }
}
