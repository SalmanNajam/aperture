using Azure.Core;
using Azure.Identity;
using Microsoft.Azure.WebJobs;
using System;
using System.Threading;

namespace PurpleAperture.BackgroundJobs.Infrastructure
{
    public class CustomDistributedLockManagerContainerProvider : DistributedLockManagerContainerProvider
    {
        public CustomDistributedLockManagerContainerProvider(string accountUrl)
        {
            // Get a managed identity credential using Azure.Identity
            var credential = new DefaultAzureCredential();
            var tokenRequestContext = new TokenRequestContext(new[] { "https://storage.azure.com/.default" });
            var initialTokenResponse = credential.GetToken(tokenRequestContext);

            // Create a V11 blob storage SDK token and token refresh function
            var storageTokenCredential = new Microsoft.Azure.Storage.Auth.TokenCredential(initialTokenResponse.Token, async (s, ct) =>
            {
                var newTokenResponse = await credential.GetTokenAsync(tokenRequestContext, CancellationToken.None);
                return new Microsoft.Azure.Storage.Auth.NewTokenAndFrequency(newTokenResponse.Token, GetRefreshFrequency(DateTimeOffset.UtcNow, newTokenResponse.ExpiresOn));
            }, this, GetRefreshFrequency(DateTimeOffset.UtcNow, initialTokenResponse.ExpiresOn));

            // The configuration for the storage account used by the WebJobs host only supports shared keys and not managed identity
            // This WebJob host storage account is used by the Timer trigger.
            // To enable this to work with managed identity we bypass the config and set a blob container directly.
            InternalContainer = new Microsoft.Azure.Storage.Blob.CloudBlobContainer(new Uri(accountUrl + "azure-webjobs-hosts"), new Microsoft.Azure.Storage.Auth.StorageCredentials(storageTokenCredential));
        }

        /// <summary>
        /// Get a refresh frequency 5 minutes before expiry time
        /// </summary>
        /// <returns></returns>
        private static TimeSpan GetRefreshFrequency(DateTimeOffset tokenRequestTime, DateTimeOffset tokenExpiryTime) => (tokenExpiryTime - tokenRequestTime).Subtract(new TimeSpan(0, 5, 0));
    }
}
