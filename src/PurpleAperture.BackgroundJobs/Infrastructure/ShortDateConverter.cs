﻿using System;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using PurpleAperture.Common.Infrastructure;

namespace PurpleAperture.BackgroundJobs.Infrastructure
{
    public class ShortDateConverter : DefaultTypeConverter
    {
        public override object ConvertFromString(string text, IReaderRow row, MemberMapData memberMapData) =>
            text.ParseShortDateOrDefault(memberMapData.Names.First());

        public override string ConvertToString(object? value, IWriterRow row, MemberMapData memberMapData) =>
            value is DateTime dateTime ? dateTime.ToString("dd/MM/yyyy") : "-";
    }
}
