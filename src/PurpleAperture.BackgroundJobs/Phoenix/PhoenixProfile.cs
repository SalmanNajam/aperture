using AutoMapper;
using PurpleAperture.Common.Models;

namespace PurpleAperture.BackgroundJobs.Phoenix
{
    public class PhoenixProfile : Profile
    {
        public PhoenixProfile()
        {
            CreateMap<PhoenixAccount, PhoenixAccount>()
                .ForMember(source => source.Id, opt => opt.Ignore());
            CreateMap<PhoenixAlliance, PhoenixAlliance>()
                .ForMember(source => source.Id, opt => opt.Ignore())
                .ForMember(source => source.PhoenixOpportunityId, opt => opt.Ignore());
            CreateMap<PhoenixOpportunity, PhoenixOpportunity>()
                .ForMember(source => source.Id, opt => opt.Ignore());
            CreateMap<PhoenixOpportunityTeamMembers, PhoenixOpportunityTeamMembers>()
                .ForMember(source => source.Id, opt => opt.Ignore())
                .ForMember(source => source.PhoenixOpportunityId, opt => opt.Ignore());
            CreateMap<PhoenixProduct, PhoenixProduct>()
                .ForMember(source => source.Id, opt => opt.Ignore())
                .ForMember(source => source.PhoenixOpportunityId, opt => opt.Ignore());
            CreateMap<PhoenixLead, PhoenixLead>()
                .ForMember(source => source.Id, opt => opt.Ignore());
            CreateMap<PhoenixLeadCampaign, PhoenixLeadCampaign>()
                .ForMember(source => source.Id, opt => opt.Ignore())
                .ForMember(source => source.PhoenixLeadId, opt => opt.Ignore());
            CreateMap<PhoenixUser, PhoenixUser>()
                .ForMember(source => source.Id, opt => opt.Ignore());
        }
    }
}
