using AutoMapper;
using Azure.Storage.Blobs;
using CSharpFunctionalExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PurpleAperture.Common.Configuration;
using PurpleAperture.Common.Infrastructure;
using PurpleAperture.Common.Infrastructure.DataContext;
using PurpleAperture.Common.Models;
using PurpleAperture.Common.Models.Json;
using System;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace PurpleAperture.BackgroundJobs.Phoenix.Handlers
{
    public class ProcessPhoenixUser : BasePhoenixProcessor
    {
        private static readonly JsonSerializerOptions SerializerOptions = new() { PropertyNameCaseInsensitive = true };
        private readonly PhoenixContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ILogger<ProcessPhoenixUser> _logger;
        private readonly ISystemClock _systemClock;
        private readonly StorageContainers _storageContainers;

        public ProcessPhoenixUser(
            BlobServiceClient blobServiceClient,
            PhoenixContext dbContext,
            IMapper mapper,
            ILogger<ProcessPhoenixUser> logger,
            ISystemClock systemClock,
            IOptions<StorageContainers> storageContainers) : base(blobServiceClient)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _logger = logger;
            _systemClock = systemClock;
            _storageContainers = storageContainers.Value;
        }

        protected override string ContainerName => _storageContainers.Users;

        protected override async Task StoreInDatabaseAsync(Stream payload)
        {
            var jsonUser = await JsonSerializer.DeserializeAsync<JsonUser>(payload, SerializerOptions);

            if (jsonUser == null || string.IsNullOrWhiteSpace(jsonUser.UserId))
            {
                throw new InvalidOperationException($"Missing data in property ({nameof(jsonUser.UserId)}) that needs unique key");
            }

            var (_, isFailure, userValue, errorValue) = PhoenixUser.From(jsonUser, _systemClock.GetUtcNow());

            if (isFailure)
            {
                _logger.LogError(errorValue.MessageTemplate, errorValue.MessageValues);
                return;
            }

            var existingUser = await _dbContext.PhoenixUsers
                .FirstOrDefaultAsync(o => o.UserId == userValue.UserId);

            if (existingUser == null)
            {
                _dbContext.PhoenixUsers.Add(userValue);
            }
            else
            {
                _mapper.Map(userValue, existingUser);
            }

            await _dbContext.SaveChangesAsync();
        }
    }
}
