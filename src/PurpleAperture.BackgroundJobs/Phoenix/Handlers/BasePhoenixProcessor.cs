using Azure.Storage.Blobs;
using System.IO;
using System.Threading.Tasks;

namespace PurpleAperture.BackgroundJobs.Phoenix.Handlers
{
    public abstract class BasePhoenixProcessor
    {
        private readonly BlobServiceClient _blobServiceClient;

        protected abstract string ContainerName { get; }

        protected BasePhoenixProcessor(BlobServiceClient blobServiceClient)
        {
            _blobServiceClient = blobServiceClient;
        }

        public async Task ProcessPayloadAsync(string blobName)
        {
            var containerClient = _blobServiceClient.GetBlobContainerClient(ContainerName);
            var blobClient = containerClient.GetBlobClient(blobName);

            var response = await blobClient.DownloadAsync();
            await StoreInDatabaseAsync(response.Value.Content);
        }

        protected abstract Task StoreInDatabaseAsync(Stream payload);
    }
}
