using AutoMapper;
using Azure.Storage.Blobs;
using CSharpFunctionalExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PurpleAperture.Common.Configuration;
using PurpleAperture.Common.Infrastructure;
using PurpleAperture.Common.Infrastructure.DataContext;
using PurpleAperture.Common.Models;
using PurpleAperture.Common.Models.Json;
using System;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace PurpleAperture.BackgroundJobs.Phoenix.Handlers
{
    public class ProcessPhoenixAccount : BasePhoenixProcessor
    {
        private static readonly JsonSerializerOptions SerializerOptions = new() { PropertyNameCaseInsensitive = true };
        private readonly PhoenixContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ILogger<ProcessPhoenixAccount> _logger;
        private readonly ISystemClock _systemClock;
        private readonly StorageContainers _storageContainers;

        public ProcessPhoenixAccount(
            BlobServiceClient blobServiceClient,
            PhoenixContext dbContext,
            IMapper mapper,
            ILogger<ProcessPhoenixAccount> logger,
            ISystemClock systemClock,
            IOptions<StorageContainers> storageContainers) : base(blobServiceClient)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _logger = logger;
            _systemClock = systemClock;
            _storageContainers = storageContainers.Value;
        }

        protected override string ContainerName => _storageContainers.Accounts;

        protected override async Task StoreInDatabaseAsync(Stream payload)
        {
            var jsonAccount = await JsonSerializer.DeserializeAsync<JsonAccount>(payload, SerializerOptions);

            if (jsonAccount == null || string.IsNullOrWhiteSpace(jsonAccount.Cidn))
            {
                throw new InvalidOperationException($"Missing data in property ({nameof(jsonAccount.Cidn)}) that needs unique key");
            }

            var (_, isFailure, accountValue, errorValue) = PhoenixAccount.From(jsonAccount, _systemClock.GetUtcNow());

            if (isFailure)
            {
                _logger.LogError(errorValue.MessageTemplate, errorValue.MessageValues);
                return;
            }

            var existingAccount = await _dbContext.PhoenixAccounts
                .FirstOrDefaultAsync(o => o.Cidn == accountValue.Cidn);

            if (existingAccount == null)
            {
                _dbContext.PhoenixAccounts.Add(accountValue);
            }
            else
            {
                _mapper.Map(accountValue, existingAccount);
                _mapper.Map(accountValue.Address, existingAccount.Address);
            }

            await _dbContext.SaveChangesAsync();
        }
    }
}
