using AutoMapper;
using Azure.Storage.Blobs;
using CSharpFunctionalExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PurpleAperture.Common.Configuration;
using PurpleAperture.Common.Infrastructure;
using PurpleAperture.Common.Infrastructure.DataContext;
using PurpleAperture.Common.Models;
using PurpleAperture.Common.Models.Json;
using System;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace PurpleAperture.BackgroundJobs.Phoenix.Handlers
{
    public class ProcessPhoenixLead : BasePhoenixProcessor
    {
        private static readonly JsonSerializerOptions SerializerOptions = new() { PropertyNameCaseInsensitive = true };
        private readonly PhoenixContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ILogger<ProcessPhoenixLead> _logger;
        private readonly ISystemClock _systemClock;
        private readonly StorageContainers _storageContainers;

        public ProcessPhoenixLead(
            BlobServiceClient blobServiceClient,
            PhoenixContext dbContext,
            IMapper mapper,
            ILogger<ProcessPhoenixLead> logger,
            ISystemClock systemClock,
            IOptions<StorageContainers> storageContainers) : base(blobServiceClient)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _logger = logger;
            _systemClock = systemClock;
            _storageContainers = storageContainers.Value;
        }

        protected override string ContainerName => _storageContainers.Leads;

        protected override async Task StoreInDatabaseAsync(Stream payload)
        {
            var json = await JsonSerializer.DeserializeAsync<JsonLead>(payload, SerializerOptions);

            if (json == null || string.IsNullOrWhiteSpace(json.LeadId))
            {
                throw new InvalidOperationException($"Missing data in property ({nameof(json.LeadId)}) that needs unique key");
            }

            var (_, isFailure, lead, error) = PhoenixLead.From(json, _systemClock.GetUtcNow());

            if (isFailure)
            {
                _logger.LogError(error.MessageTemplate, error.MessageValues);
                return;
            }

            var existingLead = await _dbContext.PhoenixLeads
                .FirstOrDefaultAsync(l => l.LeadId == lead.LeadId);

            if (existingLead == null)
            {
                _dbContext.PhoenixLeads.Add(lead);
            }
            else
            {
                _mapper.Map(lead, existingLead);
            }

            await _dbContext.SaveChangesAsync();
        }
    }

}
