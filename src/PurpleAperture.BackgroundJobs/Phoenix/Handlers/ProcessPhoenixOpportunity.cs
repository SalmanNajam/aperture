using AutoMapper;
using Azure.Storage.Blobs;
using CSharpFunctionalExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PurpleAperture.Common.Configuration;
using PurpleAperture.Common.Infrastructure;
using PurpleAperture.Common.Infrastructure.DataContext;
using PurpleAperture.Common.Models;
using PurpleAperture.Common.Models.Json;
using System;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace PurpleAperture.BackgroundJobs.Phoenix.Handlers
{
    public class ProcessPhoenixOpportunity : BasePhoenixProcessor
    {
        private static readonly JsonSerializerOptions SerializerOptions = new() { PropertyNameCaseInsensitive = true };
        private readonly PhoenixContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ILogger<ProcessPhoenixOpportunity> _logger;
        private readonly ISystemClock _systemClock;
        private readonly StorageContainers _storageContainers;

        public ProcessPhoenixOpportunity(
            BlobServiceClient blobServiceClient,
            PhoenixContext dbContext,
            IMapper mapper,
            ILogger<ProcessPhoenixOpportunity> logger,
            ISystemClock systemClock,
            IOptions<StorageContainers> storageContainers) : base(blobServiceClient)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _logger = logger;
            _systemClock = systemClock;
            _storageContainers = storageContainers.Value;
        }

        protected override string ContainerName => _storageContainers.Opportunities;

        protected override async Task StoreInDatabaseAsync(Stream payload)
        {
            var jsonOpportunity = await JsonSerializer.DeserializeAsync<JsonOpportunity>(payload, SerializerOptions);

            if (jsonOpportunity == null || string.IsNullOrWhiteSpace(jsonOpportunity.SalesforceId))
            {
                throw new InvalidOperationException($"Missing data in property ({nameof(jsonOpportunity.SalesforceId)}) that needs unique key");
            }

            var (_, isFailure, opportunityValue, errorValue) = PhoenixOpportunity.From(jsonOpportunity, _systemClock.GetUtcNow());

            if (isFailure)
            {
                _logger.LogError(errorValue.MessageTemplate, errorValue.MessageValues);
                return;
            }

            var existingOpportunity = await _dbContext.PhoenixOpportunities
                .Include(o => o.Alliances)
                .Include(o => o.Products)
                .Include(o => o.OppTeamMembers)
                .FirstOrDefaultAsync(o => o.SalesforceId == opportunityValue.SalesforceId);

            if (existingOpportunity == null)
            {
                _dbContext.PhoenixOpportunities.Add(opportunityValue);
            }
            else
            {
                _mapper.Map(opportunityValue, existingOpportunity);
            }

            await _dbContext.SaveChangesAsync();
        }
    }
}
