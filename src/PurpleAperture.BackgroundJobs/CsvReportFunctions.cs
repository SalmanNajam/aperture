using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using PurpleAperture.BackgroundJobs.Reports;
using PurpleAperture.BackgroundJobs.Reports.Account;
using PurpleAperture.BackgroundJobs.Reports.Lead;
using System;
using System.Threading.Tasks;

namespace PurpleAperture.BackgroundJobs
{
    public class CsvReportFunctions
    {
        private const string CsvReportQueueName = "%StorageQueues:Reports%";
        private const string QueueConnectionName = "AzureQueueStorageOptions";
        private readonly TimeSpan _reportYesterdayTime = new(20, 0, 0);

        private readonly ICsvReport[] _reports;

        public CsvReportFunctions(
            AccountReport accountReport,
            LeadReport leadReport)
        {
            _reports = new ICsvReport[] { accountReport, leadReport };
        }

        [return: Queue(CsvReportQueueName, Connection = QueueConnectionName)]
        public CsvReportParameters InitiateSinceYesterdayReportGeneration([TimerTrigger("0 0 20 * * *")] TimerInfo _)
        {
            return new CsvReportParameters(_reportYesterdayTime, DateTime.UtcNow);
        }

        public async Task GenerateCsvReports([QueueTrigger(CsvReportQueueName, Connection = QueueConnectionName)] CsvReportParameters reportParameters, ILogger logger)
        {
            var reportInterval = ReportInterval.GetSinceYesterdayReportInterval(reportParameters.YesterdayTime, reportParameters.GenerationTime);
            foreach (var report in _reports)
            {
                try
                {
                    await report.GenerateReport(reportInterval);
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "Failed to generate CSV report {reportName}", report.GetFilename(reportInterval));
                }
            }
        }
    }

    public record CsvReportParameters(TimeSpan? YesterdayTime, DateTime GenerationTime);
}
