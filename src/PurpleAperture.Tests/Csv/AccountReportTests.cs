using Microsoft.Extensions.DependencyInjection;
using PurpleAperture.Common.Infrastructure.DataContext;
using PurpleAperture.Common.Models;
using PurpleAperture.Common.Models.Json;
using PurpleAperture.BackgroundJobs.Reports;
using PurpleAperture.BackgroundJobs.Reports.Account;
using Shouldly;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PurpleAperture.Tests.Csv
{
    [Collection(nameof(WebJobFixtureCollection))]
    public class AccountReportTests
    {
        private readonly WebJobFixture _fixture;

        public AccountReportTests(WebJobFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public async Task RunAccountCsv_SampleAccountOutput()
        {
            // Arrange
            await _fixture.ResetDbCheckpoint();
            var dbContext = _fixture.Services.GetRequiredService<PhoenixContext>();
            dbContext.PhoenixAccounts.Add(await GetSampleAccount());
            await dbContext.SaveChangesAsync();

            var query = new AccountReportQuery().GetQuery(dbContext, new ReportInterval.All());
            using var stream = new MemoryStream();

            // Act
            await CsvReportUtilities.GenerateReport<AccountReportModel, AccountReportMap>(query, stream);

            // Assert
            Encoding.UTF8.GetString(stream.ToArray()).ShouldBe(
                @"CIDN,Account Name,Primary Address Line 1,Primary Address Line 2,Primary Address Line 3,Billing City,Billing State/Province,Billing Zip/Postal Code,Billing Country,Last Modified Date,ABN,Portfolio Code,Industry,Sub-Industry,Segment,Market Segment
4802291438,AACMNIMJXM IGWALWOVFS,""14 BLAYDON ST,"",,,KINGS MEADOWS,TAS,7249,AUSTRALIA,-,,,,,,Business Unsegmented
",
                StringCompareShould.IgnoreLineEndings);
        }

        private static async Task<PhoenixAccount> GetSampleAccount()
        {
            const string resourceName = "PurpleAperture.Tests.test_data.account-sample.json";
            var json = await Resources.GetResourceJson<JsonAccount>(resourceName);

            return PhoenixAccount.From(json!, DateTimeOffset.UtcNow).Value;
        }
    }
}
