using Microsoft.Extensions.DependencyInjection;
using PurpleAperture.Common.Infrastructure.DataContext;
using PurpleAperture.Common.Models;
using PurpleAperture.Common.Models.Json;
using PurpleAperture.BackgroundJobs.Reports;
using PurpleAperture.BackgroundJobs.Reports.Lead;
using Shouldly;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PurpleAperture.Tests.Csv
{
    [Collection(nameof(WebJobFixtureCollection))]
    public class LeadsReportTests
    {
        private readonly WebJobFixture _fixture;

        public LeadsReportTests(WebJobFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public async Task RunLeadCsv_LeadAndOpportunityJoined()
        {
            // Arrange
            await _fixture.ResetDbCheckpoint();
            var dbContext = _fixture.Services.GetRequiredService<PhoenixContext>();
            dbContext.PhoenixLeads.Add(await GetSampleLead());
            dbContext.PhoenixOpportunities.Add(await GetSampleOpportunity());
            await dbContext.SaveChangesAsync();

            var query = new LeadReportQuery().GetQuery(dbContext, new ReportInterval.All());
            using var stream = new MemoryStream();

            // Act
            await CsvReportUtilities.GenerateReport<LeadReportModel, LeadReportMap>(query, stream);

            // Assert
            Encoding.UTF8.GetString(stream.ToArray()).ShouldBe(
                @"Campaign Name,Custom Lead Id,Lead Status,Lead Source - Original,Lead Source - Most Recent (Description),Lead Owner,First Name,Last Name,Title,Company / Account,Created Date,Qualify Date,MQL Date,Accepted Date,Converted Date,Last Modified Date,Reject/Recycle/Reroute Reason,Other - Reason,Opportunity ID,Opportunity Name,Opportunity: Account,Opportunity Amount,Oppt Close Date,Revenue Impact Date,Revenue Impact Date Calc,Customer CIDN,Campaign ID,Parent Campaign ID,Parent Campaign Name
,0065P0000843b3a,Qalify,Customer Care,Customer Care,tom.lee@team.telstra.com,Tom,Lee,Lead Name,TestCompany,13/10/2022,18/02/2022,18/02/2022,-,13/10/2022,-,Funding not availabe,Test Reason,A-00504544,test-sfd-oa-int,,,15/09/2022,13/10/2022,,4867422038,,,
",
                StringCompareShould.IgnoreLineEndings);
        }

        private static async Task<PhoenixLead> GetSampleLead()
        {
            const string resourceName = "PurpleAperture.Tests.test_data.lead-sample-2.json";

            var json = await Resources.GetResourceJson<JsonLead>(resourceName);

            return PhoenixLead.From(json!, DateTimeOffset.UtcNow).Value;
        }

        private static async Task<PhoenixOpportunity> GetSampleOpportunity()
        {
            var json = await Resources.GetResourceJson<JsonOpportunity>(Resources.OpportunityVariablePropertyCasingSampleJson);

            return PhoenixOpportunity.From(json!, DateTimeOffset.UtcNow).Value;
        }
    }
}
