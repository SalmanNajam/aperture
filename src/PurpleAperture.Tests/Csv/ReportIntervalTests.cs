using PurpleAperture.BackgroundJobs.Reports;
using System;
using System.Collections.Generic;
using Xunit;

namespace PurpleAperture.Tests.Csv
{
    public class ReportIntervalTests
    {
        [Fact]
        public void WhenReportYesterdayTimeIsNull()
        {
            // Act
            var result = ReportInterval.GetSinceYesterdayReportInterval(null, DateTime.UtcNow);

            // Assert
            Assert.Equal(new ReportInterval.All(), result);
        }

        [Theory]
        [MemberData(nameof(InvalidYesterdayTimes))]
        public void WhenReportYesterdayTimeIsInvalid(TimeSpan yesterdayTimes)
        {
            // Act
            var ex = Assert.Throws<ArgumentOutOfRangeException>(() => ReportInterval.GetSinceYesterdayReportInterval(yesterdayTimes, DateTime.UtcNow));

            // Assert
            Assert.Equal("Provide a time between 0:00 and 23:59 (Parameter 'reportYesterdayTime')", ex.Message);
        }

        [Fact]
        public void WhenReportGenerationTimeIsNotUtc()
        {
            // Act
            var ex = Assert.Throws<ArgumentOutOfRangeException>(() => ReportInterval.GetSinceYesterdayReportInterval(new TimeSpan(10, 0, 0), DateTime.Now));

            // Assert
            Assert.Equal("Value must be UTC (Parameter 'reportGenerationTime')", ex.Message);
        }

        [Theory]
        [MemberData(nameof(ReportTimes))]
        public void WhenReportGenerationTimeIsAfterReportYesterdayTime(TimeSpan reportGenerationTime)
        {
            // Arrange
            var reportYesterdayTime = new TimeSpan(10, 0, 0);
            var reportGenerationDate = new DateTime(2021, 1, 20, 0, 0, 0, DateTimeKind.Utc);

            // Act
            var result = ReportInterval.GetSinceYesterdayReportInterval(reportYesterdayTime, reportGenerationDate + reportGenerationTime);

            // Assert
            Assert.Equal(new ReportInterval.StartEndRange(
                Start: new DateTime(2021, 1, 19, 10, 0, 0, DateTimeKind.Utc),
                End: new DateTime(2021, 1, 20, 10, 0, 0, DateTimeKind.Utc)),
                result);
        }

        [Theory]
        [MemberData(nameof(ReportTimes))]
        public void WhenReportGenerationTimeIsBeforeReportYesterdayTime(TimeSpan reportGenerationTime)
        {
            // Arrange
            var reportYesterdayTime = new TimeSpan(11, 0, 1);
            var reportGenerationDate = new DateTime(2021, 1, 20, 0, 0, 0, DateTimeKind.Utc);

            // Act
            var result = ReportInterval.GetSinceYesterdayReportInterval(reportYesterdayTime, reportGenerationDate + reportGenerationTime);

            // Assert
            Assert.Equal(new ReportInterval.StartEndRange(
                Start: new DateTime(2021, 1, 18, 11, 0, 1, DateTimeKind.Utc),
                End: new DateTime(2021, 1, 19, 11, 0, 1, DateTimeKind.Utc)),
                result);
        }

        public static IEnumerable<object[]> InvalidYesterdayTimes()
        {
            return new[]
            {
                new object[] { new TimeSpan(-10, 0, 0) },
                new object[] { new TimeSpan(24, 0, 0) }
            };
        }

        public static IEnumerable<object[]> ReportTimes()
        {
            return new[]
            {
                new object[] { new TimeSpan(10, 0, 0) },
                new object[] { new TimeSpan(11, 0, 0) }
            };
        }
    }
}
