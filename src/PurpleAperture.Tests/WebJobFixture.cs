using Microsoft.Azure.WebJobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using PurpleAperture.BackgroundJobs;
using PurpleAperture.Common.Infrastructure.DataContext;
using PurpleAperture.Tests.Services;
using PurpleAperture.Web.Services;
using Respawn;
using System;
using System.Threading.Tasks;
using Xunit;

namespace PurpleAperture.Tests
{
    [CollectionDefinition(nameof(WebJobFixtureCollection))]
    public class WebJobFixtureCollection : ICollectionFixture<WebJobFixture>
    {
    }

    public class WebJobFixture : IAsyncLifetime
    {
        private IHost _host = default!;
        private IServiceProvider? _services;
        private readonly Checkpoint _checkpoint = new();
        private string? _testDbConnectionString;

        public async Task InitializeAsync()
        {
            var hostBuilder = Program.CreateHostBuilder(Array.Empty<string>()).UseEnvironment("Development");
            hostBuilder.ConfigureAppConfiguration(builder =>
            {
                builder.AddJsonFile("appsettings.WebJobTests.json", optional: false);
                builder.AddEnvironmentVariables();
                builder.AddUserSecrets<WebJobFixture>();
            });
            hostBuilder.ConfigureServices((builderContext, services) =>
            {
                var dbContextType = typeof(DbContextOptions<PhoenixContext>);
                services.RemoveAll(dbContextType);

                services.AddDbContext<PhoenixContext>((_, options) =>
                {
                    _testDbConnectionString = builderContext.Configuration.GetConnectionString("Default");
                    options.UseSqlServer(_testDbConnectionString);
                });

                services.AddScoped<BlobContainerCreator>();
                services.AddScoped<QueueContainerCreator>();
                services.AddScoped<BlobContainerDeleter>();
            });

            _host = hostBuilder.Build();

            await _host.StartAsync();

            _services = _host.Services;

            using var scope = _host.Services.CreateScope();
            var blobContainerCreator = scope.ServiceProvider.GetRequiredService<BlobContainerCreator>();
            await blobContainerCreator.CreateContainers();

            var queueContainerCreator = scope.ServiceProvider.GetRequiredService<QueueContainerCreator>();
            await queueContainerCreator.CreateQueues();

            var context = scope.ServiceProvider.GetRequiredService<PhoenixContext>();
            await context.Database.MigrateAsync();
        }

        public async Task DisposeAsync()
        {
            using (var scope = _host.Services.CreateScope())
            {
                var blobContainerDeleter = scope.ServiceProvider.GetRequiredService<BlobContainerDeleter>();
                await blobContainerDeleter.DeleteContainers();

                var queueContainerCreator = scope.ServiceProvider.GetRequiredService<QueueContainerCreator>();
                await queueContainerCreator.DeleteQueues();

                var context = scope.ServiceProvider.GetRequiredService<PhoenixContext>();
                await context.Database.EnsureDeletedAsync();
            }

            await _host.StopAsync();
            _host.Dispose();
        }

        /// <summary>
        /// There is time sliding issue with docker Azure container which causes big difference in time in system time and container time
        /// And therefore many tests do not work as expected
        /// This is a workaround
        /// https://teams.microsoft.com/l/message/19:254426bd57034fb2b86ed6d7b3a58e26@thread.tacv2/1626958794057?tenantId=fc6a7adc-53de-44ad-88fc-af783bbb1d6e&groupId=fba8b4ad-9ef6-4aa5-9767-8d35dc96761f&parentMessageId=1626958794057&teamName=Telstra%20Purple&channelName=%E2%84%B9%EF%B8%8F%20Help%20Me&createdTime=1626958794057
        /// </summary>
        /// <param name="containerName"></param>
        internal async Task ClearLastUpdate(string containerName)
        {
            var dbContext = _host.Services.GetRequiredService<PhoenixContext>();
            var lastUpdate = await dbContext.LastUpdates.FirstOrDefaultAsync(x => x.TableName == containerName);
            if (lastUpdate != null)
            {
                dbContext.Remove(lastUpdate);
                await dbContext.SaveChangesAsync();
            }
        }

        private IJobHost GetJobHost() => _host.Services.GetRequiredService<IJobHost>();
        internal async Task ResetDbCheckpoint() => await _checkpoint.Reset(_testDbConnectionString);

        public IServiceProvider Services => _services ?? throw new InvalidOperationException();
    }
}
