﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Xunit;

namespace PurpleAperture.Tests
{
    [Collection(nameof(HttpFixtureCollection))]
    public class RootEndpointTests
    {
        private readonly HttpFixture _fixture;

        public RootEndpointTests(HttpFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public async Task WhenNoUserThenResponseIsNoContent()
        {
            // Arrange
            _fixture.WithNoMockUser();

            // Act
            var response = await _fixture.Client.GetAsync("/");

            // Assert
            Assert.Equal(StatusCodes.Status204NoContent, (int)response.StatusCode);
        }
    }
}
