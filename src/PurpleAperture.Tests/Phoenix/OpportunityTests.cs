﻿using System.Net.Http;
using System.Net.Http.Json;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using PurpleAperture.Common.Configuration;
using Xunit;

namespace PurpleAperture.Tests.Phoenix
{
    [Collection(nameof(HttpFixtureCollection))]
    public class OpportunityTests
    {
        private readonly HttpFixture _fixture;

        public OpportunityTests(HttpFixture fixture)
        {
            _fixture = fixture;

            var identity = TestIdentity.Create("Phoenix");
            _fixture.WithMockUser(identity);
        }

        [Fact]
        public async Task SendPhoenixOpportunity_StoredInBlobWithNameAsSalesforceId()
        {
            // Arrange
            const string expectedBlobName = "0062O000003FarUQAS.json";
            var testData = await Resources.GetResourceString(Resources.OpportunityIncorrectTypesSampleJson);

            // Act
            var response = await _fixture.Client.PostAsync(
                "phoenix/opportunity",
                new StringContent(testData, Encoding.Default, MediaTypeNames.Application.Json));

            // Assert
            Assert.Equal(StatusCodes.Status204NoContent, (int)response.StatusCode);

            var blobContainerClient = _fixture.Services.GetRequiredService<BlobServiceClient>();
            var opportunityRequestsContainerName = _fixture.Services.GetRequiredService<IOptions<StorageContainers>>().Value.Opportunities;
            var containerClient = blobContainerClient.GetBlobContainerClient(opportunityRequestsContainerName);
            var blobClient = containerClient.GetBlobClient(expectedBlobName);
            var blobExists = await blobClient.ExistsAsync();
            Assert.True(blobExists.Value);
        }

        [Fact]
        public async Task SendPhoenixOpportunityWithoutSalesforceId_ReturnsBadRequest()
        {
            // Arrange
            var jsonPayload = "{\"SomeRandomProperty\":true}";

            // Act
            var response = await _fixture.Client.PostAsync(
                "phoenix/opportunity",
                new StringContent(jsonPayload, Encoding.Default, MediaTypeNames.Application.Json));

            // Assert
            Assert.Equal(StatusCodes.Status400BadRequest, (int)response.StatusCode);

            var problemDetails = await response.Content.ReadFromJsonAsync<ProblemDetails>();
            Assert.NotNull(problemDetails);
        }

        [Fact]
        public async Task SendPhoenixOpportunityWithInvalidJson_ReturnsBadRequest()
        {
            // Arrange
            var invalidJsonPayload = "{\"SomeRandom}";

            // Act
            var response = await _fixture.Client.PostAsync(
                "phoenix/opportunity",
                new StringContent(invalidJsonPayload, Encoding.Default, MediaTypeNames.Application.Json));

            // Assert
            Assert.Equal(StatusCodes.Status400BadRequest, (int)response.StatusCode);

            var problemDetails = await response.Content.ReadFromJsonAsync<ProblemDetails>();
            Assert.NotNull(problemDetails);
        }

        [Fact]
        public async Task SendPhoenixOpportunityWithNonStringOpportunityId_ReturnsBadRequest()
        {
            // Arrange
            var jsonPayload = "{\"OpportunityId\":1898989}";

            // Act
            var response = await _fixture.Client.PostAsync(
                "phoenix/opportunity",
                new StringContent(jsonPayload, Encoding.Default, MediaTypeNames.Application.Json));

            // Assert
            Assert.Equal(StatusCodes.Status400BadRequest, (int)response.StatusCode);

            var problemDetails = await response.Content.ReadFromJsonAsync<ProblemDetails>();
            Assert.NotNull(problemDetails);
        }
    }
}
