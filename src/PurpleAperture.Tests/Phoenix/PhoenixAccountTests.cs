﻿using System;
using System.Threading.Tasks;
using PurpleAperture.Common.Models;
using PurpleAperture.Common.Models.Json;
using Shouldly;
using Xunit;

namespace PurpleAperture.Tests.Phoenix
{
    public class PhoenixAccountTests
    {
        [Fact]
        public async Task CreateAccount_ShouldHaveProvidedUpdatedDateTime()
        {
            const string resourceName = "PurpleAperture.Tests.test_data.account-sample.json";
            var json = await Resources.GetResourceJson<JsonAccount>(resourceName);

            DateTimeOffset dateTimeOffset = new(2021, 10, 01, 0, 0, 0, TimeSpan.Zero);
            var account = PhoenixAccount.From(json, dateTimeOffset);

            account.Value.LastUpdatedDateTime.ShouldBe(dateTimeOffset);
        }
    }
}
