﻿using System.Net.Http;
using System.Net.Http.Json;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using PurpleAperture.Common.Configuration;
using Xunit;

namespace PurpleAperture.Tests.Phoenix
{
    [Collection(nameof(HttpFixtureCollection))]
    public class UserTests
    {
        private readonly HttpFixture _fixture;

        public UserTests(HttpFixture fixture)
        {
            _fixture = fixture;

            var identity = TestIdentity.Create("Phoenix");
            _fixture.WithMockUser(identity);
        }

        [Fact]
        public async Task SendPhoenixUser_StoredInBlobWithNameAsUserId()
        {
            // Arrange
            const string expectedBlobName = "00E5P000000Ja57UAC.json";
            const string resourceName = "PurpleAperture.Tests.test_data.user-sample.json";
            var testData = await Resources.GetResourceString(resourceName);

            // Act
            var response = await _fixture.Client.PostAsync(
                "phoenix/user",
                new StringContent(testData, Encoding.Default, MediaTypeNames.Application.Json));

            // Assert
            Assert.Equal(StatusCodes.Status204NoContent, (int)response.StatusCode);

            var blobContainerClient = _fixture.Services.GetRequiredService<BlobServiceClient>();
            var userRequestsContainerName = _fixture.Services.GetRequiredService<IOptions<StorageContainers>>().Value.Users;
            var containerClient = blobContainerClient.GetBlobContainerClient(userRequestsContainerName);
            var blobClient = containerClient.GetBlobClient(expectedBlobName);
            var blobExists = await blobClient.ExistsAsync();
            Assert.True(blobExists.Value);
        }

        [Fact]
        public async Task SendPhoenixUserWithoutSAMLFederationID_ReturnsBadRequest()
        {
            // Arrange
            var jsonPayload = "{\"SomeRandomProperty\":true}";

            // Act
            var response = await _fixture.Client.PostAsync(
                "phoenix/user",
                new StringContent(jsonPayload, Encoding.Default, MediaTypeNames.Application.Json));

            // Assert
            Assert.Equal(StatusCodes.Status400BadRequest, (int)response.StatusCode);

            var problemDetails = await response.Content.ReadFromJsonAsync<ProblemDetails>();
            Assert.NotNull(problemDetails);
        }

        [Fact]
        public async Task SendPhoenixUserWithNullSAMLFederationID_ReturnsBadRequest()
        {
            // Arrange
            var jsonPayload = "{\"SAMLFederationID\":null}";

            // Act
            var response = await _fixture.Client.PostAsync(
                "phoenix/user",
                new StringContent(jsonPayload, Encoding.Default, MediaTypeNames.Application.Json));

            // Assert
            Assert.Equal(StatusCodes.Status400BadRequest, (int)response.StatusCode);

            var problemDetails = await response.Content.ReadFromJsonAsync<ProblemDetails>();
            Assert.NotNull(problemDetails);
        }

        [Fact]
        public async Task SendPhoenixUserWithInvalidJson_ReturnsBadRequest()
        {
            // Arrange
            var invalidJsonPayload = "{\"SomeRandom}";

            // Act
            var response = await _fixture.Client.PostAsync(
                "phoenix/user",
                new StringContent(invalidJsonPayload, Encoding.Default, MediaTypeNames.Application.Json));

            // Assert
            Assert.Equal(StatusCodes.Status400BadRequest, (int)response.StatusCode);

            var problemDetails = await response.Content.ReadFromJsonAsync<ProblemDetails>();
            Assert.NotNull(problemDetails);
        }

        [Fact]
        public async Task SendPhoenixUserWithNonStringUserId_ReturnsBadRequest()
        {
            // Arrange
            var jsonPayload = "{\"UserId\":1898989}";

            // Act
            var response = await _fixture.Client.PostAsync(
                "phoenix/user",
                new StringContent(jsonPayload, Encoding.Default, MediaTypeNames.Application.Json));

            // Assert
            Assert.Equal(StatusCodes.Status400BadRequest, (int)response.StatusCode);

            var problemDetails = await response.Content.ReadFromJsonAsync<ProblemDetails>();
            Assert.NotNull(problemDetails);
        }
    }
}
