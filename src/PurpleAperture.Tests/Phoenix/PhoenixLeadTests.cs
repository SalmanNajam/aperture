﻿using System;
using System.Threading.Tasks;
using PurpleAperture.Common.Models;
using PurpleAperture.Common.Models.Json;
using Shouldly;
using Xunit;

namespace PurpleAperture.Tests.Phoenix
{
    public class PhoenixLeadTests
    {
        [Fact]
        public async Task PhoenixLead_FromJsonLead_ShouldHaveCorrectValues()
        {
            const string resourceName = "PurpleAperture.Tests.test_data.lead-sample-2.json";

            var json = await Resources.GetResourceJson<JsonLead>(resourceName);

            DateTimeOffset dateTimeOffset = new(2021, 10, 01, 0, 0, 0, TimeSpan.Zero);
            var result = PhoenixLead.From(json, dateTimeOffset);
            result.IsSuccess.ShouldBeTrue();
            result.Value.LeadId.ShouldBe("0065P0000843b3a");
            result.Value.LeadOwner.ShouldBe("tom.lee@team.telstra.com");
            result.Value.MQLDate.ShouldBe(new DateTime(2022, 02, 18));
            result.Value.AcceptedDate.ShouldBe(new DateTime(2020, 09, 14));
            result.Value.LastUpdatedDateTime.ShouldBe(dateTimeOffset);
        }

        [Fact]
        public async Task PhoenixLead_FromJsonLeadWithIncorrectDateFormat_ShouldHaveError()
        {
            const string resourceName = "PurpleAperture.Tests.test_data.lead-sample-error.json";

            var json = await Resources.GetResourceJson<JsonLead>(resourceName);
            var result = PhoenixLead.From(json, DateTimeOffset.UtcNow);

            const string expectedFieldError =
                "Error parsing field MQLDate, value 2022-20-18 did not fit expected formats.";
            result.IsFailure.ShouldBeTrue();
            result.Error.EntityName.ShouldBe("PhoenixLead");
            result.Error.EntityId.ShouldBe("0065P0000843b3a");
            result.Error.FieldErrors.ShouldBe(expectedFieldError);
        }

        [Fact]
        public async Task PhoenixLead_CreatedFromJsonLead_ShouldHaveProvidedUpdateDateTime()
        {
            const string resourceName = "PurpleAperture.Tests.test_data.lead-sample-2.json";

            var json = await Resources.GetResourceJson<JsonLead>(resourceName);

            DateTimeOffset dateTimeOffset = new(2021, 10, 01, 0, 0, 0, TimeSpan.Zero);
            var result = PhoenixLead.From(json, dateTimeOffset);
            result.Value.LastUpdatedDateTime.ShouldBe(dateTimeOffset);
        }
    }
}
