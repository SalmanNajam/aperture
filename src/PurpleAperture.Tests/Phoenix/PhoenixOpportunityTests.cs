﻿using System;
using System.Threading.Tasks;
using PurpleAperture.Common.Models;
using PurpleAperture.Common.Models.Json;
using Shouldly;
using Xunit;

namespace PurpleAperture.Tests.Phoenix
{
    public class PhoenixOpportunityTests
    {
        [Fact]
        public async Task CreateOpportunity_FromJson()
        {
            var json = await Resources.GetResourceJson<JsonOpportunity>(Resources.OpportunityManyProductsSampleJson);
            var opportunity = PhoenixOpportunity.From(json, DateTimeOffset.UtcNow);

            opportunity.Value.SalesforceId.ShouldBe("0062N000002fzcdQAA");
            opportunity.Value.Products.Count.ShouldBe(299);
        }

        [Fact]
        public async Task CreateOpportunity_FromJsonWithVariablePropertyCasing()
        {
            var json = await Resources.GetResourceJson<JsonOpportunity>(Resources.OpportunityVariablePropertyCasingSampleJson);
            var opportunity = PhoenixOpportunity.From(json, DateTimeOffset.UtcNow);

            opportunity.Value.SalesforceId.ShouldBe("0062O000003FarUQAS");
            opportunity.Value.OpportunityId.ShouldBe("A-00504544");
            opportunity.Value.OpportunityName.ShouldBe("test-sfd-oa-int");
            opportunity.Value.Cidn.ShouldBe("4867422038");

            opportunity.Value.Alliances.Count.ShouldBe(2);
        }

        [Fact]
        public async Task CreateOpportunity_FromJsonWithNullProducts_Succeeds()
        {
            var json = await Resources.GetResourceJson<JsonOpportunity>(Resources.OpportunityNullProducts);
            var opportunity = PhoenixOpportunity.From(json, DateTimeOffset.UtcNow);

            opportunity.Value.Products.Count.ShouldBe(0);
        }

        [Fact]
        public async Task CreateOpportunity_ShouldHaveProvidedUpdatedDateTime()
        {
            var json = await Resources.GetResourceJson<JsonOpportunity>(Resources.OpportunityNullProducts);
            DateTimeOffset dateTimeOffset = new(2021, 10, 01, 0, 0, 0, TimeSpan.Zero);
            var opportunity = PhoenixOpportunity.From(json, dateTimeOffset);

            opportunity.Value.LastUpdatedDateTime.ShouldBe(dateTimeOffset);
        }

        [Fact]
        public async Task Phoenix_FromJsonWithNullProducts_Succeeds()
        {
            var json = await Resources.GetResourceJson<JsonOpportunity>(Resources.OpportunityNullProducts);

            DateTimeOffset dateTimeOffset = new(2021, 10, 01, 0, 0, 0, TimeSpan.Zero);
            var opportunity = PhoenixOpportunity.From(json, dateTimeOffset);

            opportunity.Value.LastUpdatedDateTime.ShouldBe(dateTimeOffset);
        }
    }
}
