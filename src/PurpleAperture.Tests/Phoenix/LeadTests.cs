﻿using System.Net.Http;
using System.Net.Http.Json;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using PurpleAperture.Common.Configuration;
using Xunit;

namespace PurpleAperture.Tests.Phoenix
{
    [Collection(nameof(HttpFixtureCollection))]
    public class LeadTests
    {
        private readonly HttpFixture _fixture;

        public LeadTests(HttpFixture fixture)
        {
            _fixture = fixture;

            var identity = TestIdentity.Create("Phoenix");
            _fixture.WithMockUser(identity);
        }

        [Fact]
        public async Task SendPhoenixLead_StoredInBlobWithNameAsSalesforceId()
        {
            // Arrange
            const string expectedBlobName = "0065P0000843b3a.json";
            const string resourceName = "PurpleAperture.Tests.test_data.lead-sample.json";
            var testData = await Resources.GetResourceString(resourceName);

            // Act
            var response = await _fixture.Client.PostAsync(
                "phoenix/lead",
                new StringContent(testData, Encoding.Default, MediaTypeNames.Application.Json));

            // Assert
            Assert.Equal(StatusCodes.Status204NoContent, (int)response.StatusCode);

            var blobContainerClient = _fixture.Services.GetRequiredService<BlobServiceClient>();
            var leadRequestsContainerName = _fixture.Services.GetRequiredService<IOptions<StorageContainers>>().Value.Leads;
            var containerClient = blobContainerClient.GetBlobContainerClient(leadRequestsContainerName);
            var blobClient = containerClient.GetBlobClient(expectedBlobName);
            var blobExists = await blobClient.ExistsAsync();
            Assert.True(blobExists.Value);
        }

        [Fact]
        public async Task SendPhoenixLeadWithoutLeadId_ReturnsBadRequest()
        {
            // Arrange
            var jsonPayload = "{\"SomeRandomProperty\":true}";

            // Act
            var response = await _fixture.Client.PostAsync(
                "phoenix/lead",
                new StringContent(jsonPayload, Encoding.Default, MediaTypeNames.Application.Json));

            // Assert
            Assert.Equal(StatusCodes.Status400BadRequest, (int)response.StatusCode);

            var problemDetails = await response.Content.ReadFromJsonAsync<ProblemDetails>();
            Assert.NotNull(problemDetails);
        }

        [Fact]
        public async Task SendPhoenixLeadWithInvalidJson_ReturnsBadRequest()
        {
            // Arrange
            var invalidJsonPayload = "{\"SomeRandom}";

            // Act
            var response = await _fixture.Client.PostAsync(
                "phoenix/lead",
                new StringContent(invalidJsonPayload, Encoding.Default, MediaTypeNames.Application.Json));

            // Assert
            Assert.Equal(StatusCodes.Status400BadRequest, (int)response.StatusCode);

            var problemDetails = await response.Content.ReadFromJsonAsync<ProblemDetails>();
            Assert.NotNull(problemDetails);
        }

        [Fact]
        public async Task SendPhoenixLeadWithNonStringLeadId_ReturnsBadRequest()
        {
            // Arrange
            var jsonPayload = "{\"LeadId\":1898989}";

            // Act
            var response = await _fixture.Client.PostAsync(
                "phoenix/lead",
                new StringContent(jsonPayload, Encoding.Default, MediaTypeNames.Application.Json));

            // Assert
            Assert.Equal(StatusCodes.Status400BadRequest, (int)response.StatusCode);

            var problemDetails = await response.Content.ReadFromJsonAsync<ProblemDetails>();
            Assert.NotNull(problemDetails);
        }
    }
}
