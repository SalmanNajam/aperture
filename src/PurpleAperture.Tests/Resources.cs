﻿using System;
using System.IO;
using System.Reflection;
using System.Text.Json;
using System.Threading.Tasks;

namespace PurpleAperture.Tests
{
    public static class Resources
    {
        public const string OpportunityManyProductsSampleJson = "PurpleAperture.Tests.test_data.opportunities.many-products-sample.json";
        public const string OpportunityIncorrectTypesSampleJson = "PurpleAperture.Tests.test_data.opportunities.incorrect-types-sample.json";
        public const string OpportunityScientificDecimalStringSampleJson = "PurpleAperture.Tests.test_data.opportunities.scientific-decimal-values.json";
        public const string OpportunityVariablePropertyCasingSampleJson = "PurpleAperture.Tests.test_data.opportunities.variable-property-casing-sample.json";
        public const string OpportunityNullProducts = "PurpleAperture.Tests.test_data.opportunities.null-products.json";

        private static readonly JsonSerializerOptions SerializerOptions = new() { PropertyNameCaseInsensitive = true };

        public static Stream GetResourceStream(string resourceName)
        {
            var assembly = Assembly.GetExecutingAssembly();

            return assembly.GetManifestResourceStream(resourceName) ?? throw new InvalidOperationException();
        }

        public static async Task<TModel> GetResourceJson<TModel>(string resourceName)
        {
            await using var jsonStream = GetResourceStream(resourceName);
            var jsonModel = await JsonSerializer.DeserializeAsync<TModel>(jsonStream, SerializerOptions);
            return jsonModel ?? throw new InvalidOperationException();
        }

        public static async Task<string> GetResourceString(string resourceName)
        {
            var assembly = Assembly.GetExecutingAssembly();

            await using var stream = assembly.GetManifestResourceStream(resourceName);
            using var reader = new StreamReader(stream ?? throw new InvalidOperationException());

            string jsonTestData = await reader.ReadToEndAsync();
            return jsonTestData;
        }
    }
}
