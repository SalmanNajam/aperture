﻿using System.Net.Http.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Xunit;

namespace PurpleAperture.Tests
{
    [Collection(nameof(HttpFixtureCollection))]
    public class AuthenticationTests
    {
        private static readonly string _requestPath = "phoenix/auth-test";
        private readonly HttpFixture _fixture;

        public AuthenticationTests(HttpFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public async Task WhenNoUserThenResponseIsUnauthorized()
        {
            // Arrange
            _fixture.WithNoMockUser();

            // Act
            var response = await _fixture.Client.PostAsJsonAsync(
                _requestPath,
                new { TestCall = true });

            // Assert
            Assert.Equal(StatusCodes.Status401Unauthorized, (int)response.StatusCode);
        }

        [Theory]
        [InlineData("null")]
        [InlineData("some-random-role")]
        public async Task WhenUserWithIncorrectRoleThenResponseIsForbidden(string role)
        {
            // Arrange
            var identity = TestIdentity.Create(role);
            _fixture.WithMockUser(identity);

            // Act
            var response = await _fixture.Client.PostAsJsonAsync(
                _requestPath,
                new { TestCall = true });

            // Assert
            Assert.Equal(StatusCodes.Status403Forbidden, (int)response.StatusCode);
        }

        [Fact]
        public async Task WhenUserWithCorrectRoleThenResponseIsOk()
        {
            // Arrange
            var identity = TestIdentity.Create("Phoenix");
            _fixture.WithMockUser(identity);

            // Act
            var response = await _fixture.Client.PostAsJsonAsync(
                _requestPath,
                new { TestCall = true });

            // Assert
            Assert.Equal(StatusCodes.Status204NoContent, (int)response.StatusCode);
        }
    }
}
