using Azure.Storage.Blobs;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using PurpleAperture.BackgroundJobs.Phoenix.Handlers;
using PurpleAperture.Common.Configuration;
using PurpleAperture.Common.Infrastructure.DataContext;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace PurpleAperture.Tests.ConvertToBlob
{
    [Collection(nameof(WebJobFixtureCollection))]
    public class LeadBlobToDbTests
    {
        private readonly WebJobFixture _fixture;

        public LeadBlobToDbTests(WebJobFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public async Task LeadBlob_AfterRunTask_ResultsInOneRecord()
        {
            const string resourceName = "PurpleAperture.Tests.test_data.lead-sample-2.json";

            // Arrange
            var serviceClient = _fixture.Services.GetRequiredService<BlobServiceClient>();
            var storageContainers = _fixture.Services.GetRequiredService<IOptions<StorageContainers>>();
            var containerClient = serviceClient.GetBlobContainerClient(storageContainers.Value.Leads);
            await containerClient.UploadBlobAsync("lead-sample.json", Resources.GetResourceStream(resourceName));
            var leadProcessor = _fixture.Services.GetRequiredService<ProcessPhoenixLead>();

            // Act
            await leadProcessor.ProcessPayloadAsync("lead-sample.json");

            // Assert
            var dbContext = _fixture.Services.GetRequiredService<PhoenixContext>();
            Assert.Equal(1, dbContext.PhoenixLeads.Count());
        }
    }
}
