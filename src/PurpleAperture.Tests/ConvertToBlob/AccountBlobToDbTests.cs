using Azure.Storage.Blobs;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using PurpleAperture.BackgroundJobs.Phoenix.Handlers;
using PurpleAperture.Common.Configuration;
using PurpleAperture.Common.Infrastructure.DataContext;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace PurpleAperture.Tests.ConvertToBlob
{
    [Collection(nameof(WebJobFixtureCollection))]
    public class AccountBlobToDbTests
    {
        private readonly WebJobFixture _fixture;

        public AccountBlobToDbTests(WebJobFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public async Task AccountBlob_AfterRunTask_ResultsInOneRecord()
        {
            const string resourceName = "PurpleAperture.Tests.test_data.account-sample.json";

            // Arrange
            var serviceClient = _fixture.Services.GetRequiredService<BlobServiceClient>();
            var storageContainers = _fixture.Services.GetRequiredService<IOptions<StorageContainers>>();
            var containerClient = serviceClient.GetBlobContainerClient(storageContainers.Value.Accounts);
            await containerClient.UploadBlobAsync("account.json", Resources.GetResourceStream(resourceName));
            var accountProcessor = _fixture.Services.GetRequiredService<ProcessPhoenixAccount>();

            // Act
            await accountProcessor.ProcessPayloadAsync("account.json");

            // Assert
            var dbContext = _fixture.Services.GetRequiredService<PhoenixContext>();
            Assert.Equal(1, dbContext.PhoenixAccounts.Count());
        }
    }
}
