using Azure.Storage.Blobs;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using PurpleAperture.BackgroundJobs.Phoenix.Handlers;
using PurpleAperture.Common.Configuration;
using PurpleAperture.Common.Infrastructure.DataContext;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;

namespace PurpleAperture.Tests.ConvertToBlob
{
    [Collection(nameof(WebJobFixtureCollection))]
    public class OpportunityBlobToDbTests
    {
        private readonly WebJobFixture _fixture;
        private readonly BlobContainerClient _containerClient;
        private readonly PhoenixContext _dbContext;

        public OpportunityBlobToDbTests(WebJobFixture fixture)
        {
            _fixture = fixture;

            var serviceClient = _fixture.Services.GetRequiredService<BlobServiceClient>();
            var storageContainers = _fixture.Services.GetRequiredService<IOptions<StorageContainers>>();
            _containerClient = serviceClient.GetBlobContainerClient(storageContainers.Value.Opportunities);

            _dbContext = _fixture.Services.GetRequiredService<PhoenixContext>();
        }

        [Fact]
        public async Task OpportunityBlob_AfterRunTask_ResultsInOneRecord()
        {
            await _fixture.ResetDbCheckpoint();
            await _containerClient.DeleteBlobIfExistsAsync("opportunity-many-products-sample.json");

            // Arrange
            await _containerClient.UploadBlobAsync("opportunity-many-products-sample.json", Resources.GetResourceStream(Resources.OpportunityManyProductsSampleJson));
            var opportunityProcessor = _fixture.Services.GetRequiredService<ProcessPhoenixOpportunity>();

            // Act
            await opportunityProcessor.ProcessPayloadAsync("opportunity-many-products-sample.json");

            // Assert
            var result = _dbContext.PhoenixOpportunities.Count(o => o.OpportunityId == "A-00475773");
            Assert.Equal(1, result);
        }

        [Fact]
        public async Task IncorrectOpportunityBlob_AfterRunTask_ResultsInNoRecord()
        {
            await _fixture.ResetDbCheckpoint();

            // Arrange
            await _containerClient.UploadBlobAsync("opportunity-incorrect-types-sample.json", Resources.GetResourceStream(Resources.OpportunityIncorrectTypesSampleJson));
            var opportunityProcessor = _fixture.Services.GetRequiredService<ProcessPhoenixOpportunity>();

            // Act
            var ex = await Assert.ThrowsAsync<JsonException>(async () => await opportunityProcessor.ProcessPayloadAsync("opportunity-incorrect-types-sample.json"));

            // Assert
            Assert.Equal("The JSON value could not be converted to System.String. Path: $.Products[0].ProductQty | LineNumber: 30 | BytePositionInLine: 21.", ex.Message);
            var result = _dbContext.PhoenixOpportunities.Count(o => o.OpportunityId == "A-00504544");
            Assert.Equal(0, result);
        }

        [Fact]
        public async Task ScientificDecimalOpportunityBlob_AfterRunTask_ResultsInOneRecord()
        {
            await _fixture.ResetDbCheckpoint();

            // Arrange
            await _containerClient.UploadBlobAsync("opportunity-scientific-decimal-strings-sample.json", Resources.GetResourceStream(Resources.OpportunityScientificDecimalStringSampleJson));
            var opportunityProcessor = _fixture.Services.GetRequiredService<ProcessPhoenixOpportunity>();

            // Act
            await opportunityProcessor.ProcessPayloadAsync("opportunity-scientific-decimal-strings-sample.json");

            // Assert
            var result = _dbContext.PhoenixOpportunities.Count(o => o.OpportunityId == "A-00504546");
            Assert.Equal(1, result);
        }
    }
}
