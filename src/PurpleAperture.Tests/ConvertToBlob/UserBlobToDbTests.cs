using Azure.Storage.Blobs;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using PurpleAperture.BackgroundJobs.Phoenix.Handlers;
using PurpleAperture.Common.Configuration;
using PurpleAperture.Common.Infrastructure.DataContext;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace PurpleAperture.Tests.ConvertToBlob
{
    [Collection(nameof(WebJobFixtureCollection))]
    public class UserBlobToDbTests
    {
        private readonly WebJobFixture _fixture;
        private readonly BlobContainerClient _containerClient;
        private readonly PhoenixContext _dbContext;
        private readonly string _usersContainerName;

        public UserBlobToDbTests(WebJobFixture fixture)
        {
            _fixture = fixture;
            var serviceClient = _fixture.Services.GetRequiredService<BlobServiceClient>();
            var storageContainers = _fixture.Services.GetRequiredService<IOptions<StorageContainers>>();
            _usersContainerName = storageContainers.Value.Users ?? throw new InvalidOperationException();
            _containerClient = serviceClient.GetBlobContainerClient(_usersContainerName);

            _dbContext = _fixture.Services.GetRequiredService<PhoenixContext>();
        }

        [Fact]
        public async Task UserBlob_AfterRunTask_ResultsInOneRecord()
        {
            const string resourceName = "PurpleAperture.Tests.test_data.user-sample.json";

            // Arrange
            await _containerClient.UploadBlobAsync("user-sample.json", Resources.GetResourceStream(resourceName));
            var userProcessor = _fixture.Services.GetRequiredService<ProcessPhoenixUser>();


            // Act
            await _fixture.ClearLastUpdate(_usersContainerName);
            await userProcessor.ProcessPayloadAsync("user-sample.json");

            // Assert
            Assert.Equal(1, _dbContext.PhoenixUsers.Count(x => x.UserId == "00E5P000000Ja57UAC"));
        }

        [Fact]
        public async Task UserBlob_WithNullEmail_AfterRunTask_ResultsInNoRecord()
        {
            const string resourceName = "PurpleAperture.Tests.test_data.user-sample-null-email.json";

            // Arrange
            await _containerClient.UploadBlobAsync("user-sample-null-email.json",
                Resources.GetResourceStream(resourceName));
            var userProcessor = _fixture.Services.GetRequiredService<ProcessPhoenixUser>();


            // Act
            await _fixture.ClearLastUpdate(_usersContainerName);
            await userProcessor.ProcessPayloadAsync("user-sample-null-email.json");

            // Assert
            Assert.Equal(0, _dbContext.PhoenixUsers.Count(x => x.UserId == "00E5P000000Ja57UAD"));
        }
    }
}
