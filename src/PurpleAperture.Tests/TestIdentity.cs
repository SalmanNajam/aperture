﻿using System.Collections.Generic;
using System.Security.Claims;
using PurpleAperture.Web.Security;

namespace PurpleAperture.Tests
{
    public static class TestIdentity
    {
        public static ClaimsIdentity Create(string role)
        {
            var claims = new List<Claim>
            {
                new("name", "Mocked user")
            };

            if (!string.IsNullOrEmpty(role))
            {
                claims.Add(new("role", role));
            }

            var identity = new ClaimsIdentity(
                claims,
                SchemeNames.Jwt,
                "name",
                "role");

            return identity;
        }
    }
}
