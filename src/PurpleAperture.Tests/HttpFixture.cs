using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PurpleAperture.Tests.Services;
using PurpleAperture.Web;
using PurpleAperture.Web.Security;
using PurpleAperture.Web.Services;
using System;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace PurpleAperture.Tests
{
    [CollectionDefinition(nameof(HttpFixtureCollection))]
    public class HttpFixtureCollection : ICollectionFixture<HttpFixture>
    {
    }

    public class HttpFixture : IAsyncLifetime
    {
        private IWebHost _webHost = default!;
        private ClaimsPrincipal? _mockedUser;
        private HttpClient _httpClient = default!;
        private IServiceProvider? _services;

        public async Task InitializeAsync()
        {
            _webHost = new WebHostBuilder()
                .UseStartup<Startup>()
                .UseKestrel()
                .UseUrls("http://127.0.0.1:0")
                .ConfigureAppConfiguration(builder =>
                {
                    builder.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                    builder.AddJsonFile("appsettings.Tests.json", optional: false);
                    builder.AddEnvironmentVariables();
                })
                .ConfigureTestServices(services =>
                {
                    services
                        .AddOptions<JwtBearerOptions>(SchemeNames.Jwt)
                        .PostConfigure(options =>
                        {
                            options.Events = new JwtBearerEvents
                            {
                                OnMessageReceived = context =>
                                {
                                    if (_mockedUser != null)
                                    {
                                        context.Principal = _mockedUser;
                                        context.Success();
                                    }

                                    return Task.CompletedTask;
                                }
                            };
                        });

                    services.AddScoped<BlobContainerDeleter>();
                })
                .Build();

            await _webHost.StartAsync();

            _services = _webHost.Services;

            using (var scope = _webHost.Services.CreateScope())
            {
                var blobContainerCreator = scope.ServiceProvider.GetRequiredService<BlobContainerCreator>();
                await blobContainerCreator.CreateContainers();

                var queueContainerCreator = scope.ServiceProvider.GetRequiredService<QueueContainerCreator>();
                await queueContainerCreator.CreateQueues();
            }

            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(_webHost.ServerFeatures.Get<IServerAddressesFeature>().Addresses.First())
            };
        }

        public async Task DisposeAsync()
        {
            using (var scope = _webHost.Services.CreateScope())
            {
                var blobContainerDeleter = scope.ServiceProvider.GetRequiredService<BlobContainerDeleter>();
                await blobContainerDeleter.DeleteContainers();

                var queueContainerCreator = scope.ServiceProvider.GetRequiredService<QueueContainerCreator>();
                await queueContainerCreator.DeleteQueues();
            }

            await _webHost.StopAsync();
            _webHost.Dispose();
        }

        public HttpClient Client => _httpClient;
        public IServiceProvider Services => _services ?? throw new InvalidOperationException();
        public void WithNoMockUser() => _mockedUser = null;
        public void WithMockUser(ClaimsIdentity identity) => _mockedUser = new ClaimsPrincipal(identity);
    }
}
