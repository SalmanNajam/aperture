﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PurpleAperture.Common.Infrastructure.DataContext;
using Xunit;

namespace PurpleAperture.Tests
{
    /// <summary>
    /// This test class is here to ensure that the dynamic data masking SQL script applies nicely on top of our database schema.
    /// See <repo-root>/docs/sql-dynamic-data-masking.md for more information.
    /// </summary>
    [Collection(nameof(WebJobFixtureCollection))]
    public class DynamicDataMaskingTests
    {
        private readonly WebJobFixture _fixture;

        public DynamicDataMaskingTests(WebJobFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public async Task WhenApplyingTheDynamicDataMaskingScriptThenAllGood()
        {
            var sqlScriptContents = await File.ReadAllTextAsync("enable-dynamic-data-masking.sql");

            using var scope = _fixture.Services.CreateScope();
            var scopedProvider = scope.ServiceProvider;

            var context = scopedProvider.GetRequiredService<PhoenixContext>();
            await context.Database.ExecuteSqlRawAsync(sqlScriptContents);
        }
    }
}
