﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PurpleAperture.Common.Configuration;

namespace PurpleAperture.Tests.Services
{
    public class BlobContainerDeleter
    {
        private readonly BlobServiceClient _blobServiceClient;
        private readonly StorageContainers _storageContainers;
        private readonly ILogger _logger;

        public BlobContainerDeleter(
            BlobServiceClient blobServiceClient,
            IOptions<StorageContainers> storageContainersAccessor,
            ILogger<BlobContainerDeleter> logger)
        {
            _blobServiceClient = blobServiceClient;
            _storageContainers = storageContainersAccessor.Value;
            _logger = logger;
        }

        public async Task DeleteContainers()
        {
            try
            {
                var tasks = _storageContainers
                    .Select(async containerName =>
                    {
                        var containerClient = _blobServiceClient.GetBlobContainerClient(containerName);
                        await containerClient.DeleteIfExistsAsync();
                    })
                    .ToList();

                await Task.WhenAll(tasks);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "An error occurred while deleting the blob containers");
            }
        }
    }
}
