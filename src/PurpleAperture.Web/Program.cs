using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using PurpleAperture.Web.Services;
using System.Threading.Tasks;

namespace PurpleAperture.Web
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            using (var scope = host.Services.CreateScope())
            {
                var blobContainerCreator = scope.ServiceProvider.GetRequiredService<BlobContainerCreator>();
                await blobContainerCreator.CreateContainers();

                var queueContainerCreator = scope.ServiceProvider.GetRequiredService<QueueContainerCreator>();
                await queueContainerCreator.CreateQueues();
            }

            await host.RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureLogging(builder => builder.AddAzureWebAppDiagnostics())
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
