﻿using System;

namespace PurpleAperture.Web
{
    public class ValidationException : Exception
    {
        public ValidationException(string? message)
            : base(message)
        {
        }
    }
}
