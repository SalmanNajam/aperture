﻿namespace PurpleAperture.Web.Configuration
{
    public class AzureAdOptions
    {
        public string? Authority { get; set; }
        public string? Audience { get; set; }
    }
}
