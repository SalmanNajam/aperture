using Azure.Storage.Blobs;
using Azure.Storage.Queues;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PurpleAperture.Common.Configuration;
using System.Security.Claims;
using System.Text.Json;

namespace PurpleAperture.Web.Features.Phoenix
{
    public static class Account
    {
        public record Command(JsonDocument JsonDocument) : PhoenixBase.ICommand;

        public record BlobKey(string Cidn) : PhoenixBase.IBlobKey
        {
            public string GetKey() => Cidn;
        }

        public class Handler : PhoenixBase.Handler<Command, BlobKey>
        {
            private readonly StorageContainers _storageContainers;
            private readonly StorageQueues _storageQueues;

            protected override string ContainerName => _storageContainers.Accounts;
            protected override string QueueName => _storageQueues.Accounts;

            public Handler(
                BlobServiceClient blobServiceClient,
                QueueServiceClient queueServiceClient,
                ClaimsPrincipal user,
                IOptions<StorageContainers> storageContainers,
                IOptions<StorageQueues> storageQueues,
                ILogger<Handler> logger)
                : base(blobServiceClient, queueServiceClient, user, logger)
            {
                _storageContainers = storageContainers.Value;
                _storageQueues = storageQueues.Value;
            }
        }
    }
}
