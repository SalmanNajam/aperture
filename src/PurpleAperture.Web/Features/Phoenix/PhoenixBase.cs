using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Azure.Storage.Queues;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mime;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace PurpleAperture.Web.Features.Phoenix
{
    public static class PhoenixBase
    {
        public interface ICommand : IRequest
        {
            JsonDocument JsonDocument { get; }
        }

        public interface IBlobKey
        {
            string GetKey();
        }

        public abstract class Handler<TCommand, TBlobKey> : AsyncRequestHandler<TCommand>
            where TCommand : ICommand
            where TBlobKey : IBlobKey
        {
            private readonly BlobServiceClient _blobServiceClient;
            private readonly QueueServiceClient _queueServiceClient;

            private readonly ClaimsPrincipal _user;
            private readonly ILogger _logger;

            private readonly JsonSerializerOptions _jsonSerializerOptions = new() { PropertyNameCaseInsensitive = true };
            private readonly JsonWriterOptions _jsonWriterOptions = new() { Indented = true };

            protected abstract string ContainerName { get; }
            protected abstract string QueueName { get; }

            protected Handler(BlobServiceClient blobServiceClient, QueueServiceClient queueServiceClient, ClaimsPrincipal user, ILogger logger)
            {
                _blobServiceClient = blobServiceClient;
                _queueServiceClient = queueServiceClient;
                _user = user;
                _logger = logger;
            }

            protected override async Task Handle(TCommand command, CancellationToken cancellationToken)
            {
                if (command.JsonDocument is null)
                {
                    throw new ValidationException("Unable to deserialise the JSON input");
                }

                await using var jsonStream = await DeserialiseJsonDocument(command);
                jsonStream.Seek(0, SeekOrigin.Begin);

                string? blobKey = default;
                try
                {
                    blobKey = await GetBlobKey(jsonStream, cancellationToken);
                    jsonStream.Seek(0, SeekOrigin.Begin);
                }
                catch (Exception exception)
                {
                    var blobKeyTypeName = GetBlobKeyTypeName();
                    var blobKeyPropertyName = GetBlobKeyPropertyName();

                    _logger.LogError(
                        exception,
                        "Error while deserializing the JSON payload to an instance of '{BlobKeyTypeName}' with a property named '{BlobKeyPropertyName}' of type string",
                        blobKeyTypeName,
                        blobKeyPropertyName);

                    throw new ValidationException($"Unable to find expected property '{blobKeyPropertyName}' of type string in the JSON input");
                }

                if (string.IsNullOrWhiteSpace(blobKey))
                {
                    throw new ValidationException($"Unable to find expected property '{GetBlobKeyPropertyName()}' of type string in the JSON input");
                }

                var blobNameJson = $"{blobKey.Trim()}.json";

                await UploadBlob(blobNameJson, jsonStream, cancellationToken);
                await WriteQueueMessageAsync(blobNameJson);
            }

            private async Task<string> GetBlobKey(Stream memoryStream, CancellationToken cancellationToken)
            {
                var blobKey = await JsonSerializer.DeserializeAsync<TBlobKey>(
                                  memoryStream,
                                  _jsonSerializerOptions,
                                  cancellationToken)
                              ?? throw new InvalidOperationException();
                return blobKey.GetKey();
            }

            private async Task<Stream> DeserialiseJsonDocument(TCommand command)
            {
                MemoryStream memoryStream = new();
                await using (Utf8JsonWriter jsonWriter = new(memoryStream, _jsonWriterOptions))
                {
                    command.JsonDocument.WriteTo(jsonWriter);
                }

                memoryStream.Seek(0, SeekOrigin.Begin);
                return memoryStream;
            }

            private async Task UploadBlob(string blobNameJson, Stream memoryStream, CancellationToken cancellationToken)
            {
                var containerClient = _blobServiceClient.GetBlobContainerClient(ContainerName);
                var blobClient = containerClient.GetBlobClient(blobNameJson);

                await blobClient.UploadAsync(
                    memoryStream,
                    new BlobUploadOptions
                    {
                        HttpHeaders = new BlobHttpHeaders
                        {
                            ContentType = MediaTypeNames.Application.Json
                        },
                        Metadata = new Dictionary<string, string>
                        {
                            ["aadClientId"] = _user.FindFirstValue("azp")
                        }
                    },
                    cancellationToken);
            }

            private async Task WriteQueueMessageAsync(string blobNameJson)
            {
                var queueClient = _queueServiceClient.GetQueueClient(QueueName);
                // Queue triggers require base64 encoded messages
                // https://docs.microsoft.com/en-us/azure/azure-functions/functions-bindings-storage-queue-trigger?tabs=csharp#encoding
                var message = Convert.ToBase64String(Encoding.UTF8.GetBytes(blobNameJson));
                await queueClient.SendMessageAsync(message);
            }

            private static string GetBlobKeyPropertyName() => typeof(TBlobKey).GetProperties()[0].Name;

            private static string GetBlobKeyTypeName()
            {
                //
                // Input: PurpleAperture.Web.Features.Phoenix.Lead+BlobKey
                // Output: Lead.BlobKey
                //
                var fullyQualifiedName = typeof(TBlobKey).FullName!;
                var lastDotIndex = fullyQualifiedName.LastIndexOf('.');

                return fullyQualifiedName
                    .Substring(lastDotIndex + 1)
                    .Replace('+', '.');
            }
        }
    }
}
