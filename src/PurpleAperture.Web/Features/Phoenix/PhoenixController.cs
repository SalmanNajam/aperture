using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PurpleAperture.Web.Features.Shared;
using PurpleAperture.Web.Security;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace PurpleAperture.Web.Features.Phoenix
{
    [Route("phoenix")]
    [Authorize(Policy = PolicyNames.Phoenix)]
    public class PhoenixController : ControllerBase
    {
        private readonly IMediator _mediator;

        public PhoenixController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("auth-test")]
        public async Task<IActionResult> AuthTest([FromBody] JsonDocument jsonDocument, CancellationToken cancellationToken)
        {
            Response.RegisterForDispose(jsonDocument);

            await _mediator.Send(new AuthTest.Command(jsonDocument), cancellationToken);
            return NoContent();
        }

        [HttpPost("opportunity")]
        public async Task<IActionResult> Opportunity([FromBody] JsonDocument jsonDocument, CancellationToken cancellationToken)
        {
            Response.RegisterForDispose(jsonDocument);

            await _mediator.Send(new Opportunity.Command(jsonDocument), cancellationToken);
            return NoContent();
        }

        [HttpPost("account")]
        public async Task<IActionResult> Account([FromBody] JsonDocument jsonDocument, CancellationToken cancellationToken)
        {
            Response.RegisterForDispose(jsonDocument);

            await _mediator.Send(new Account.Command(jsonDocument), cancellationToken);
            return NoContent();
        }

        [HttpPost("lead")]
        public async Task<IActionResult> Lead([FromBody] JsonDocument jsonDocument, CancellationToken cancellationToken)
        {
            Response.RegisterForDispose(jsonDocument);

            await _mediator.Send(new Lead.Command(jsonDocument), cancellationToken);
            return NoContent();
        }

        [HttpPost("user")]
        public async Task<IActionResult> User([FromBody] JsonDocument jsonDocument, CancellationToken cancellationToken)
        {
            Response.RegisterForDispose(jsonDocument);

            await _mediator.Send(new User.Command(jsonDocument), cancellationToken);
            return NoContent();
        }
    }
}
