using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PurpleAperture.Web.Features.Shared;
using PurpleAperture.Web.Security;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace PurpleAperture.Web.Features.WorkDay
{
    [Route("workday")]
    [Authorize(Policy = PolicyNames.WorkDay)]
    public class WorkdayController : ControllerBase
    {
        private readonly IMediator _mediator;

        public WorkdayController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("auth-test")]
        public async Task<IActionResult> AuthTest([FromBody] JsonDocument jsonDocument, CancellationToken cancellationToken)
        {
            Response.RegisterForDispose(jsonDocument);

            await _mediator.Send(new AuthTest.Command(jsonDocument), cancellationToken);
            return NoContent();
        }

        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody] JsonDocument jsonDocument, CancellationToken cancellationToken)
        {
            Response.RegisterForDispose(jsonDocument);

            await _mediator.Send(new Update.Command(jsonDocument), cancellationToken);
            return NoContent();
        }
    }
}
