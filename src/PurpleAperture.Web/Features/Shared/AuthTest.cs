using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using MediatR;
using Microsoft.Extensions.Options;
using PurpleAperture.Common.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mime;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace PurpleAperture.Web.Features.Shared
{
    public static class AuthTest
    {
        public record Command(JsonDocument JsonDocument) : IRequest;

        public class Handler : AsyncRequestHandler<Command>
        {
            private readonly BlobServiceClient _blobServiceClient;
            private readonly ClaimsPrincipal _user;
            private readonly StorageContainers _storageContainers;

            public Handler(
                BlobServiceClient blobServiceClient,
                ClaimsPrincipal user,
                IOptions<StorageContainers> storageContainers)
            {
                _blobServiceClient = blobServiceClient;
                _user = user;
                _storageContainers = storageContainers.Value;
            }

            protected override async Task Handle(Command command, CancellationToken cancellationToken)
            {
                var containerClient = _blobServiceClient.GetBlobContainerClient(_storageContainers.AuthTests);

                var blobName = $"{Guid.NewGuid()}.json";
                var blobClient = containerClient.GetBlobClient(blobName);

                var jsonBytes = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(command.JsonDocument, new JsonSerializerOptions { WriteIndented = true }));
                using var memoryStream = new MemoryStream(jsonBytes);
                await blobClient.UploadAsync(
                    memoryStream,
                    new BlobUploadOptions
                    {
                        HttpHeaders = new BlobHttpHeaders
                        {
                            ContentType = MediaTypeNames.Application.Json
                        },
                        Metadata = new Dictionary<string, string>
                        {
                            ["aadClientId"] = _user.FindFirstValue("azp")
                        }
                    },
                    cancellationToken);
            }
        }
    }
}
