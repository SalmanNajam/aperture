using Azure.Storage.Queues;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PurpleAperture.Common.Configuration;
using System;
using System.Threading.Tasks;

namespace PurpleAperture.Web.Services
{
    public class QueueContainerCreator
    {
        private readonly QueueServiceClient _queueServiceClient;
        private readonly StorageQueues _storageQueues;
        private readonly ILogger<QueueContainerCreator> _logger;

        public QueueContainerCreator(
            QueueServiceClient queueServiceClient,
            IOptions<StorageQueues> options,
            ILogger<QueueContainerCreator> logger)
        {
            _queueServiceClient = queueServiceClient;
            _storageQueues = options.Value;
            _logger = logger;
        }

        public async Task CreateQueues()
        {
            try
            {
                foreach (var queueName in _storageQueues)
                {
                    var queueClient = _queueServiceClient.GetQueueClient(queueName);
                    await queueClient.CreateIfNotExistsAsync();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "An error occurred while creating the storage queues");
            }
        }

        public async Task DeleteQueues()
        {
            try
            {
                foreach (var queueName in _storageQueues)
                {
                    var queueClient = _queueServiceClient.GetQueueClient(queueName);
                    await queueClient.DeleteIfExistsAsync();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "An error occurred while deleting the storage queues");
            }
        }
    }
}
