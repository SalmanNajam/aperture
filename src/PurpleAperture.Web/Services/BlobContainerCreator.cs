﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PurpleAperture.Common.Configuration;

namespace PurpleAperture.Web.Services
{
    public class BlobContainerCreator
    {
        private readonly BlobServiceClient _blobServiceClient;
        private readonly StorageContainers _storageContainers;
        private readonly ILogger _logger;

        public BlobContainerCreator(
            BlobServiceClient blobServiceClient,
            IOptions<StorageContainers> storageContainersAccessor,
            ILogger<BlobContainerCreator> logger)
        {
            _blobServiceClient = blobServiceClient;
            _storageContainers = storageContainersAccessor.Value;
            _logger = logger;
        }

        public async Task CreateContainers()
        {
            try
            {
                var tasks = _storageContainers
                    .Select(async containerName =>
                    {
                        var containerClient = _blobServiceClient.GetBlobContainerClient(containerName);
                        if (!await containerClient.ExistsAsync())
                        {
                            await containerClient.CreateIfNotExistsAsync();
                        }
                    })
                    .ToList();

                await Task.WhenAll(tasks);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "An error occurred while creating the blob containers");
            }
        }
    }
}
