namespace PurpleAperture.Web.Security
{
    public static class PolicyNames
    {
        public const string Phoenix = nameof(Phoenix);
        public const string WorkDay = nameof(WorkDay);
    }
}
