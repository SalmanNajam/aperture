﻿namespace PurpleAperture.Web.Security
{
    public static class SchemeNames
    {
        public const string Jwt = nameof(Jwt);
    }
}
