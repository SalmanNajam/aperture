using Azure.Identity;
using Hellang.Middleware.ProblemDetails;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PurpleAperture.Common.Configuration;
using PurpleAperture.Web.Configuration;
using PurpleAperture.Web.Security;
using PurpleAperture.Web.Services;
using System.Threading.Tasks;

namespace PurpleAperture.Web
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddMediatR(typeof(Startup));
            services.AddAzureClients(builder =>
            {
                builder.UseCredential(new ManagedIdentityCredential());
                builder.AddBlobServiceClient(_configuration.GetSection("AzureBlobStorageOptions"));
                builder.AddQueueServiceClient(_configuration.GetSection("AzureQueueStorageOptions"));
            });

            services.AddHttpContextAccessor();
            services.AddScoped(provider =>
            {
                var httpContextAccessor = provider.GetRequiredService<IHttpContextAccessor>();
                var httpContext = httpContextAccessor.HttpContext;

                return httpContext!.User;
            });

            var azureAdOptions = _configuration.GetSection(nameof(AzureAdOptions)).Get<AzureAdOptions>();
            services
                .AddAuthentication()
                .AddJwtBearer(SchemeNames.Jwt, options =>
                {
                    options.Authority = azureAdOptions.Authority;
                    options.Audience = azureAdOptions.Audience;
                });

            services.AddAuthorization(options =>
            {
                options.DefaultPolicy = new AuthorizationPolicyBuilder(SchemeNames.Jwt)
                    .RequireAuthenticatedUser()
                    .Build();

                options.AddPolicy(PolicyNames.Phoenix, builder =>
                {
                    builder
                        .AddAuthenticationSchemes(SchemeNames.Jwt)
                        .RequireRole(AadRoleNames.Phoenix);
                });

                options.AddPolicy(PolicyNames.WorkDay, builder =>
                {
                    builder
                        .AddAuthenticationSchemes(SchemeNames.Jwt)
                        .RequireRole(AadRoleNames.WorkDay);
                });
            });

            services.AddApplicationInsightsTelemetry();

            services.Configure<StorageContainers>(_configuration.GetSection(nameof(StorageContainers)));
            services.Configure<StorageQueues>(_configuration.GetSection(nameof(StorageQueues)));

            services.AddScoped<BlobContainerCreator>();
            services.AddScoped<QueueContainerCreator>();

            services.AddProblemDetails(options =>
            {
                //
                // By default exception details will be included when the ASP.NET Core environment is Development
                // See https://github.com/khellang/Middleware/blob/master/src/ProblemDetails/ProblemDetailsOptionsSetup.cs
                //
                options.ShouldLogUnhandledException = (context, exception, problemDetails) => true;
                options.Map<ValidationException>(x =>
                {
                    var problemDetails = StatusCodeProblemDetails.Create(StatusCodes.Status400BadRequest);
                    problemDetails.Extensions[ProblemDetailsOptions.DefaultExceptionDetailsPropertyName] = new[]
                    {
                        x.Message
                    };

                    return problemDetails;
                });
            });
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseProblemDetails();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints
                    .MapControllers()
                    .RequireAuthorization(new AuthorizeAttribute());

                endpoints
                    .MapGet("", context =>
                    {
                        context.Response.StatusCode = StatusCodes.Status204NoContent;
                        return Task.CompletedTask;
                    })
                    .AllowAnonymous();
            });
        }
    }
}
