#Requires -Version 7

<#
.SYNOPSIS
    Creates a certificate signing request (CSR) for a client certificate
.EXAMPLE
    PS C:\> .\create-client-certificate-csr.ps1 -Target DevSecOps -OutputDirectory C:\Users\me\Downloads

    Creates a CSR for the DevSecOps team in the C:\Users\me\Downloads directory
.EXAMPLE
    PS C:\> .\create-client-certificate-csr.ps1 -Target MuleSoft -OutputDirectory C:\Users\me\Downloads -ForProduction

    Creates a CSR for MuleSoft team in the C:\Users\me\Downloads directory for production use
#>

[CmdletBinding()]
param(
    # The target of the certificate signing request (CSR)
    [Parameter(Mandatory = $true)]
    [ValidateSet(
        'MuleSoft',
        'PCF',
        'DevSecOps',
        'WorkDay'
    )]
    [String]
    $Target,

    # The directory in which the resulting files will be written
    [Parameter(Mandatory = $true)]
    [System.IO.DirectoryInfo]
    $OutputDirectory,

    # Indicates whether the resulting certificate is for production use or not
    [Parameter(Mandatory = $false)]
    [Switch]
    $ForProduction,

    # (Optional) The path to the OpenSSL executable
    [Parameter(Mandatory = $false)]
    [System.IO.FileInfo]
    $OpenSslPath = $null
)

Set-StrictMode -Version Latest

if ($PSBoundParameters.ContainsKey('OpenSslPath')) {
    if (-not $OpenSslPath.Exists) {
        throw ('Unable to find OpenSSL at the provided path ''{0}''' -f $OpenSslPath.FullName)
    }
} else {
    $openSslCommand = Get-Command -Name 'openssl' -ErrorAction 'SilentlyContinue'
    if ($null -ne $openSslCommand) {
        $OpenSslPath = $openSslCommand.Source
    } else {
        #
        # On Windows, if you have Git installed, there's a high chance it ships with OpenSSL
        #
        if ([System.Runtime.InteropServices.RuntimeInformation]::IsOSPlatform([System.Runtime.InteropServices.OSPlatform]::Windows)) {
            $gitCommand = Get-Command -Name 'git' -ErrorAction 'SilentlyContinue'
            if ($null -ne $gitCommand) {
                #
                # Git: <git-install-folder>\cmd\git.exe
                # OpenSSL: <git-install-folder>\usr\bin\openssl.exe
                #
                $gitExePath = $gitCommand.Source
                $openSslExePath = Join-Path `
                    -Path (Split-Path -Path $gitExePath -Parent) `
                    -ChildPath '..' `
                    -AdditionalChildPath 'usr', 'bin', 'openssl.exe'

                if (Test-Path -Path $openSslExePath) {
                    $OpenSslPath = $openSslExePath
                }
            }
        }
    }

    if ($null -eq $OpenSslPath) {
        throw 'Unable to automatically find OpenSSL, please use the -OpenSslPath parameter'
    }
}

if (-not $OutputDirectory.Exists) {
    New-Item -Path $OutputDirectory.FullName -ItemType Directory -Force
}

$subjects = @{
    MuleSoft  = @{
        Country    = 'AU'
        Company    = 'Telstra'
        CommonName = 'Phoenix MuleSoft'
    }
    PCF       = @{
        Country    = 'AU'
        Company    = 'Telstra'
        CommonName = 'Phoenix PCF'
    }
    DevSecOps = @{
        Country    = 'AU'
        Company    = 'Telstra Purple'
        CommonName = 'DevSecOps'
    }
    WorkDay = @{
        Country    = 'AU'
        Company    = 'Telstra'
        CommonName = 'WorkDay Integration'
    }
}

$subject = $subjects[$Target]

$openSslPassphraseArgs = @(
    'rand',
    '-base64', '30'
)

$passphrase = & $OpenSslPath $openSslPassphraseArgs

#
# From https://www.namecheap.com/support/knowledgebase/article.aspx/10161/14/generating-a-csr-on-windows-using-openssl/
#
$openSslCsrArgs = @(
    'req',
    '-new',
    '-newkey', 'rsa:2048',
    '-keyout', (Join-Path -Path $OutputDirectory.FullName -ChildPath 'aperture-private-key.key'),
    '-out', (Join-Path -Path $OutputDirectory.FullName -ChildPath 'aperture-csr.txt'),
    '-passout', ('pass:{0}' -f $passphrase),
    '-subj', ('"/C={0}/O={1}/CN={2} ({3})"' -f $subject.Country, $subject.Company, $subject.CommonName, $(if ($ForProduction) { 'Production' } else { 'Non-Production' }))
)

#
# 🤞
#
& $OpenSslPath $openSslCsrArgs

$passphrase | Out-File -FilePath (Join-Path -Path $OutputDirectory.FullName -ChildPath 'aperture-private-key-passphrase.txt') -Force
