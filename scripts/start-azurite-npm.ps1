[CmdletBinding()]
param(
)

$repositoryRootFolder = Split-Path -Path $PSScriptRoot -Parent
$azuriteFolder = Join-Path -Path $repositoryRootFolder -ChildPath '.azurite'

if (-not (Test-Path -Path $azuriteFolder)) {
    $null = New-Item -Path $azuriteFolder -ItemType Directory
}

azurite --location $azuriteFolder
