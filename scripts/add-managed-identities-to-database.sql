--
--Replace <AzureDevOpsPrincipal> with one of the below values
--Dev, UAT, Staging: TelstraPurple-Engine-Aperture-23491ae2-5d32-4ecc-a951-ff689c173ff6
--Prod: TelstraPurple-Engine-Aperture-b17d83ed-0ec1-4127-86d6-d623f4c0fdaa
--
IF NOT EXISTS (SELECT [name] FROM [sys].[database_principals] WHERE  [name] = N'<AzureDevOpsPrincipal>')
BEGIN
    CREATE USER [<AzureDevOpsPrincipal>] FROM  EXTERNAL PROVIDER  WITH DEFAULT_SCHEMA=[dbo]
END
GO

GRANT ALTER ANY MASK TO [<AzureDevOpsPrincipal>]
GRANT ALTER ANY SENSITIVITY CLASSIFICATION TO [<AzureDevOpsPrincipal>]
alter role db_ddlAdmin add member [<AzureDevOpsPrincipal>]
alter role db_datawriter add member [<AzureDevOpsPrincipal>]
alter role db_datareader add member [<AzureDevOpsPrincipal>]

--
--Replace <WebAppIdentity> with one of the below values
--Dev: purple-dev-aperture-webapp
--UAT: purple-uat-aperture-webapp
--Staging: purple-staging-aperture-webapp
--Prod: purple-prod-aperture-webapp
--
IF NOT EXISTS (SELECT [name] FROM [sys].[database_principals] WHERE  [name] = N'<WebAppIdentity>')
BEGIN
    CREATE USER [<WebAppIdentity>] FROM  EXTERNAL PROVIDER  WITH DEFAULT_SCHEMA=[dbo]
END
GO

alter role db_datawriter add member [<WebAppIdentity>]
alter role db_datareader add member [<WebAppIdentity>]
grant unmask to [<WebAppIdentity>]
