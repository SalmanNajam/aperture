#! /bin/bash

##
#
# This script is used to create a "cannot delete" lock on the storage account
# to avoid accidental data loss.
#
##

#
# To stop on errors
#
set -e

#
# Id of the target Azure subscription
#
# Telstra Integration - Dev: 23491ae2-5d32-4ecc-a951-ff689c173ff6
# Telstra Integration - Prod: b17d83ed-0ec1-4127-86d6-d623f4c0fdaa
#
SUBSCRIPTION_ID=""

#
# Resource group name
#
# Dev: purple-dev-aperture
# UAT: purple-uat-aperture
# Staging: purple-staging-aperture
# Production: purple-prod-aperture
#
RESOURCE_GROUP_NAME=""

#
# Storage account name
#
# Dev: purpledevaperturestore
# UAT: purpleuataperturestore
# Staging: purplestgaperturestore
# Production: purpleprodaperturestore
#
STORAGE_ACCOUNT_NAME=""

#
# Select Azure subscription
#
az account set --subscription $SUBSCRIPTION_ID --output none

#
# Look for the resource group
#
RESOURCE_GROUP=$(az group list --query "[?name == '$RESOURCE_GROUP_NAME']" --output tsv)
if [ -z "$RESOURCE_GROUP" ]
then
    echo "Unable to find resource group '$RESOURCE_GROUP_NAME'"
    exit 1
fi

#
# Select default resource group
#
az configure --defaults group=$RESOURCE_GROUP_NAME

#
# Look for storage account
#
STORAGE_ACCOUNT_ID=$(az storage account list --query "[?name == '$STORAGE_ACCOUNT_NAME'].id" --output tsv)
if [ -z "$STORAGE_ACCOUNT_ID" ]
then
    echo "Unable to find storage account '$STORAGE_ACCOUNT_NAME' in resource group '$RESOURCE_GROUP_NAME'"
    exit 1
fi

#
# Look for existing lock
#
LOCK_NAME="Prevent deletion"
EXISTING_LOCK=$(az lock list --resource-group $RESOURCE_GROUP_NAME --resource-name $STORAGE_ACCOUNT_NAME --resource-type Microsoft.Storage/storageAccounts --query "[?name == '$LOCK_NAME']" --output tsv)
if [ -n "$EXISTING_LOCK" ]
then
    echo "A lock already exists"
    exit 0
fi

#
# Create lock
#
az lock create \
    --name "Prevent deletion" \
    --lock-type CanNotDelete \
    --resource-group $RESOURCE_GROUP_NAME \
    --resource $STORAGE_ACCOUNT_NAME \
    --resource-type Microsoft.Storage/storageAccounts \
    --output none
