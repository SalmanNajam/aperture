﻿# run the tests with docker-compose
docker-compose -f ./scripts/docker-compose.yml -f ./scripts/docker-compose.integration-tests.yml up --abort-on-container-exit --exit-code-from test

# store the result of the test run
$testRunExitCode = $LASTEXITCODE

docker-compose -f ./scripts/docker-compose.yml -f ./scripts/docker-compose.integration-tests.yml down --remove-orphans

# store the result of the tear down operation
$dockerComposeDownExitCode = $LASTEXITCODE

# report back the success or failure
if ($testRunExitCode -ne 0) {
    Write-Host "docker-compose up returned non-zero exit code of $testRunExitCode"
    exit $testRunExitCode
}
elseif ($dockerComposeDownExitCode -ne 0) {
    Write-Host "docker-compose down returned non-zero exit code of $dockerComposeDownExitCode"
    exit $dockerComposeDownExitCode
}
