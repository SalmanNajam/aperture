#! /bin/bash

##
#
# This script is used to grant appropriate Azure roles to the
# App Service managed identity so it can access the storage account.
#
##

#
# To stop on errors
#
set -e

#
# Id of the target Azure subscription
#
# Telstra Integration - Dev: 23491ae2-5d32-4ecc-a951-ff689c173ff6
# Telstra Integration - Prod: b17d83ed-0ec1-4127-86d6-d623f4c0fdaa
#
SUBSCRIPTION_ID=""

#
# Resource group name
#
# Dev: purple-dev-aperture
# UAT: purple-uat-aperture
# Staging: purple-staging-aperture
# Production: purple-prod-aperture
#
RESOURCE_GROUP_NAME=""

#
# App Service name
#
# Dev: purple-dev-aperture-webapp
# UAT: purple-uat-aperture-webapp
# Staging: purple-staging-aperture-webapp
# Production: purple-prod-aperture-webapp
#
APP_SERVICE_NAME=""

#
# Storage account name
#
# Dev: purpledevaperturestore
# UAT: purpleuataperturestore
# Staging: purplestgaperturestore
# Production: purpleprodaperturestore
#
STORAGE_ACCOUNT_NAME=""

#
# Select Azure subscription
#
az account set --subscription $SUBSCRIPTION_ID --output none

#
# Look for the resource group
#
RESOURCE_GROUP=$(az group list --query "[?name == '$RESOURCE_GROUP_NAME']" --output tsv)
if [ -z "$RESOURCE_GROUP" ]
then
    echo "Unable to find resource group '$RESOURCE_GROUP_NAME'"
    exit 1
fi

#
# Select default resource group
#
az configure --defaults group=$RESOURCE_GROUP_NAME

#
# Look for the App Service
#
APP_SERVICE=$(az webapp list --query "[?name == '$APP_SERVICE_NAME']" --output tsv)
if [ -z "$APP_SERVICE" ]
then
    echo "Unable to find App Service '$APP_SERVICE_NAME' in resource group '$RESOURCE_GROUP_NAME'"
    exit 1
fi

#
# Get managed identity
#
MANAGED_IDENTITY_ID=$(az webapp show --name $APP_SERVICE_NAME --query "identity.principalId" --output tsv)
if [ -z "$MANAGED_IDENTITY_ID" ]
then
    echo "Unable to find a managed identity for App Service '$APP_SERVICE_NAME' in resource group '$RESOURCE_GROUP_NAME'"
    exit 1
fi

#
# Look for storage account
#
STORAGE_ACCOUNT_ID=$(az storage account list --query "[?name == '$STORAGE_ACCOUNT_NAME'].id" --output tsv)
if [ -z "$STORAGE_ACCOUNT_ID" ]
then
    echo "Unable to find storage account '$STORAGE_ACCOUNT_NAME' in resource group '$RESOURCE_GROUP_NAME'"
    exit 1
fi

#
# Assign roles
#
az role assignment create \
    --role "Storage Blob Data Owner" \
    --assignee-object-id $MANAGED_IDENTITY_ID \
    --scope $STORAGE_ACCOUNT_ID \
    --output none

az role assignment create \
    --role "Storage Queue Data Contributor" \
    --assignee-object-id $MANAGED_IDENTITY_ID \
    --scope $STORAGE_ACCOUNT_ID \
    --output none
