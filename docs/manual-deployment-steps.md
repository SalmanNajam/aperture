# Manual deployment steps

The app requires once-off manual deployment steps to be taken.

The reason for that is that the service principal used by the Azure DevOps service connection doesn't have the necessary permissions assigned to it.
The operations below require the "User Access Administrator" role, which we're not granting to service principals at this stage.

## Create Managed Identities in SQL database for AzureDevOps and WebApp Identity

In order for AzureDevOps to be able to access the database, AzureDevOps principal used by AzureDevOps pipeline should be added with ddlAdmin, datareader and datawriter roles.
Also this identity should be given the permission to alter `MASK` and `SENSITIVITY CLASSIFICATION` to the database

In order for the WebApp to communicate with database over managed identity, The WebApp Idenity needs to be created and given datareader and datawriter roles.
Also `unmask` should be granted to the WebApp Identity.

NOTE: The script below can be run by SSMS using your purple identity. Your user should be added to Active Directory Group that is set as the database server Active Directory Admin.
This has been scripted in the [`scripts/add-managed-identities-to-database.sql` file](../scripts/add-managed-identities-to-database.sql).

## Allow the App Service managed identity to interact with the Storage account blob services

The app leverages Azure managed identity where possible, as it reduces the number of sensitive credentials it needs to protect.

In order for the App Service to be able to read from and write to Blob storage, it needs to be assigned a specific role.

This has been scripted in the [`scripts/grant-blob-storage-access-to-app-service-managed-identity.sh` file](../scripts/grant-blob-storage-access-to-app-service-managed-identity.sh).

## Add a "cannot delete" lock to the Storage account

The Blob services have been configured with soft-delete and automatic versioning to mitigate the possibility of data loss.

However, this does not protect against the deletion of the Storage account.
As a result, a "cannot delete" lock is created against the Storage account to prevent it from being accidentally deleted.

This has been scripted in the [`scripts/grant-blob-storage-access-to-app-service-managed-identity.sh` file](../scripts/add-cannot-delete-lock-on-storage-account.sh).
