# Client certificates

During the Telstra Security assessment, it was mandated that Aperture needs to use mutual TLS (or mTLS).
This document outlines how this is enforced and how to create new client certificates.

## Setup

We're leveraging Cloudflare's capabilities to enforce mTLS for Aperture.
See the [official documentation on the matter](https://developers.cloudflare.com/ssl/client-certificates/enable-mtls).

To ensure that consumers can't go around the mTLS requirement, we redirect requests to the `azurewebsites.net` domain to the appropriate `purple.tech` one.

At the time of writing (March 2021), Cloudflare doesn't support per-host client certificates.
In other words, a client certificate created for the `purple.tech` zone will be accepted by Cloudflare for all hosts that have mTLS enabled.

As a result, we currently have 3 client certificates:

1. One for the Phoenix MuleSoft platform, with a 1-year expiry.
1. One for the Phoenix PCF platform, with a 1-year expiry.
1. One for the DecSecOps team, with a 10-year expiry.

Client certificates are stored in LastPass.

## Create client certificates

### Create the CSR

Cloudflare can manage the private key and Certificate Signing Request (or CSR) creation, but when doing so, the certificate subject is not configurable and is always "CN=Cloudflare, C=US", which prevents us from distinguishing the different certificates.

As a result, we create our own private key and CSR with OpenSSL.
To make this easier, feel free to use the [`create-client-certificate-csr.ps1` script](../scripts/create-client-certificate-csr.ps1).

### Create the certificate

Navigate to [the Cloudflare dashboard](https://dash.cloudflare.com/) > the `purple.tech` zone > SSL/TLS > Client certificates > Create certificate.

Use the following settings:

- Select "Use my private key and CSR" and paste the content of the `aperture-csr.txt` file that was created in the previous step.
- Pick the certificate validity accordingly (1 year for Telstra, 10 years for Purple).
- Create the certificate.
- Get the PEM certificate contents and save it in a file locally with the `.crt` extension.

### Store in LastPass

Make sure that you put the following in LastPass:

- The private key file (`.key` file).
- The private key passphrase.
- The certificate (`.crt` file).
