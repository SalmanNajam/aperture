# SQL dynamic data masking

During their assessment, Telstra Security flagged that we store Personally Identifiable Information (PII) in our database.
We decided to use SQL Server dynamic data masking to remediate this.
Official documentation is avaiable here: <https://docs.microsoft.com/en-us/azure/azure-sql/database/dynamic-data-masking-overview>.

We used the following guidance on Unily to determine which data we need to obfuscate: <https://telstra.unily.com/sites/plr/SitePage/146531/do-you-need-to-log-your-plr-data>.

## Options evaluated

Since we're using EF Core for our schema migrations, it was the natural choice.
Unfortunately, at the time of writing it doesn't support dynamic data masking, see <https://github.com/dotnet/efcore/issues/20592>.

We then found a third-party NuGet package that adds this capability, but when evaluated, it didn't support EF Core 5.
We opened an issue with the maintainer at <https://github.com/nikitasavinov/EntityFrameworkCore.Extensions/issues/153>.

As a result, we decided to manage this ourselves manually.

## How it works

The first implementation used an EF Core migration with a hand-written SQL script, see <https://dev.azure.com/TelstraPurple-Engine/Aperture/_git/Aperture/pullrequest/73?_a=files>.
The issue with this approach is that subsequent migrations could potentially disable data masking on the columns it was enabled on.

To counter this, we decided to write a SQL script outside of EF Core, that we would run on the target database after running the EF Core migrations.
We also added an automated test to ensure that the SQL script runs successfully to catch potential issues, in case of a column rename, for example.
See <https://dev.azure.com/TelstraPurple-Engine/Aperture/_git/Aperture/pullrequest/74>.

The script is in the `infrastructure` folder: [`enable-dynamic-data-masking.sql`](../infrastructure/enable-dynamic-data-masking.sql).

The permissions around which users can access masked data are managed in the [ReadifyAzureSQL repository](https://dev.azure.com/readify/Technology/_git/ReadifyAzureSQL?path=%2Fdevelopment-australiaeast.json).
