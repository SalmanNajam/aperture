# Development environment setup

## Integration tests

The `PurpleAperture.Tests` project contains integration tests. The tests rely on Azurite running, instructions for
Azurite are below. Once Azurite is running, the tests can be run via `dotnet test` or via your IDE's test runner.

The pipeline uses `docker-compose` to run the tests, and the same can be achieved locally by using the following command.

`docker-compose -f .\scripts\docker-compose.yml -f .\scripts\docker-compose.integration-tests.yml up`

And the corresponding `docker-compose down` is here:

`docker-compose -f .\scripts\docker-compose.yml -f .\scripts\docker-compose.integration-tests.yml down`

## Azurite

Azurite is a cross-platform Azure Storage emulator.
More information can be found at <https://docs.microsoft.com/en-us/azure/storage/common/storage-use-azurite>.

### Run with npm

```powershell
npm install --global azurite
./scripts/start-azurite-npm.ps1
```

### Run with Docker

We've created a [`docker-compose.yml`](./scripts/docker-compose.yml) file in the `scripts` directory.

```powershell
cd ./scripts
docker-compose up
```

### Migration
Using EF Core Migrations:
* Ensure EF Core tooling is installed (refer to `.config/dotnet-tools.json` in the project)
```
> dotnet tool restore
```

* Adding a new migration
```
> dotnet ef migrations add <migration name> --project PurpleAperture.Common/PurpleAperture.Common.csproj
```

* Update database
```
> dotnet ef database update --project PurpleAperture.Common/PurpleAperture.Common.csproj
```

## Call the API

The `postman` directory contains both a collection and an environment that you can import.

You'll need to update the `purpleaperture-client-secret` variable of the environment, which you can get from the "Purple - Aperture Dev" LastPass folder.
Please open a request over at <https://it.purple.tech> to request access.
