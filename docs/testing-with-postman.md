# Testing with Postman

## Collection and environment

### DevSecOps team members

If you're part of the DevSecOps team, you can get access to the Postman team.
Request access at [https://myaccess.microsoft.com/@purple.onmicrosoft.com#/access-packages](Access packages) > Search for "Postman" > Request that access package.

You'll then be able to log in to Postman with SSO using your Purple account and have access to the shared workspace.
During SSO, put telstrapurple for team domain.
Request access to the "PurpleAperture" collection if you don't have access to it straight away.

You'll also need to update the appropriate environments(s) `purpleaperture-client-id` and `purpleaperture-client-secret` variables with the corresponding values that you'll find in LastPass.

### Not a member of DevSecOps

No trouble, we regularly export the Postman collection in the repository at `<repo-root>/postman`.
There's also environment files for local dev and the development environment.

If you find the collection in the repository is out of date, please ask someone from DevSecOps to refresh it from the DevSecOps Postman team.

## Client certificates

Telstra Security mandated that we need to use mutual TLS (or mTLS), also known as "using client certificates".
This is set up at the Cloudflare level, hence not applicable if you test the API on your local machine.
More information about this in the [client certificates docs page](./client-certificates-setup.md).

To use client certificates in Postman, follow the steps in the [official documentation](https://learning.postman.com/docs/sending-requests/certificates/).
You will find the certificate, the private key, and the passphrase in LastPass.
